<?php

use App\Http\Controllers\backend\adminController;
use App\Http\Controllers\backend\AnggotaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\backend\HalamanUtamaControler;
use App\Http\Controllers\backenD\LoginController;
use App\Http\Requests\AnggotaRequest;
use PhpParser\Node\Stmt\HaltCompiler;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//-----------------------------------HalamanUtama-----------------------------------//
Route::get('/',[HalamanUtamaControler::class,'index']);
route::get('berita',[HalamanUtamaControler::class,'berita'])->name('berita');
route::get('agenda',[HalamanUtamaControler::class,'agenda'])->name('agenda');
route::get('download',[HalamanUtamaControler::class,'download'])->name('download');
route::get('keanggotaan',[HalamanUtamaControler::class,'keanggotaan'])->name('keanggotaan');
route::get('pengurus',[HalamanUtamaControler::class,'pengurus'])->name('pengurus');
route::get('profil',[HalamanUtamaControler::class,'profil'])->name('profil');
route::get('/register',[LoginController::class,'register'])->name('register');
route::post('/proses_register',[LoginController::class,'proses_register'])->name('proses_register');
route::get('login',[LoginController::class,'login'])->name('login')->middleware('alreadyLogged');
route::post('proses_login',[LoginController::class,'proses_login'])->name('proses_login');
route::get('detail_berita',[HalamanUtamaControler::class,'detaik_berita'])->name('detail_berita');
route::post('logout',[LoginController::class,'logout'])->name('logout');

//-------------------------------------admin---------------------------------------------//
Route::group(['middleware' => ['isLoggedIn']], function () {
route::get('admin',[adminController::class,'admin'])->name('admin');
route::get('admin/agama',[adminController::class,'agama'])->name('admin_agama');
route::post('admin/tambah_agama',[adminController::class,'tambah_agama'])->name('tambah_agama');
route::post('admin/update_agama/{id}',[adminController::class,'update_agama'])->name('update_agama');
route::post('admin/delete_agama/{id}',[adminController::class,'delete_agama'])->name('delete_agama');
route::get('admin/rekening_pembayaran',[adminController::class,'rekening_pembayaran'])->name('rekening_pembayaran');
route::post('admin/tambah_rekening_pembayaran',[adminController::class,'tambah_rekening_pembayaran'])->name('tambah_rekening_pembayaran');
route::post('admin/update_rekening_pembayaran/{id}',[adminController::class,'update_rekening_pembayaran'])->name('update_rekening_pembayaran');
route::post('admin/delete_rekening_pembayaran/{id}',[adminController::class,'delete_rekening_pembayaran'])->name('delete_rekening_pembayaran');
route::get('admin/jabatan',[adminController::class,'jabatan'])->name('admin_jabatan');
route::post('admin/tambah_jabatan',[adminController::class,'tambah_jabatan'])->name('tambah_jabatan');
route::post('admin/update_jabatan/{id}',[adminController::class,'update_jabatan'])->name('update_jabatan');
route::post('admin/delete_jabatan/{id}',[adminController::class,'delete_jabatan'])->name('delete_jabatan');
route::get('admin/pengurus',[adminController::class,'pengurus'])->name('admin_pengurus');
route::post('admin/tambah_pengurus',[adminController::class,'tambah_pengurus'])->name('tambah_pengurus');
route::post('admin/update_pengurus/{id}',[adminController::class,'update_pengurus'])->name('update_pengurus');
route::post('admin/delete_pengurus/{id}',[adminController::class,'delete_pengurus'])->name('delete_pengurus');
route::get('admin/foto_pengurus',[adminController::class,'foto_pengurus'])->name('admin_foto_pengurus');
route::post('admin/update_foto_pengurus/{id}',[adminController::class,'update_foto_pengurus'])->name('update_foto_pengurus');
route::get('admin/struktur_organisasi',[adminController::class,'struktur_organisasi'])->name('struktur_organisasi');
route::post('admin/update_struktur_organisasi',[adminController::class,'update_Struktur_organisasi'])->name('update_struktur_organisasi');
route::get('admin/slide',[adminController::class,'slide'])->name('admin_slide');
route::post('admin/tambah_slide',[adminController::class,'tambah_slide'])->name('tambah_slide');
route::post('admin/update_slide/{id}',[adminController::class,'update_slide'])->name('update_slide');
route::post('admin/delete_slide/{id}',[adminController::class,'delete_slide'])->name('delete_slide');
route::get('admin/profil',[adminController::class,'profil'])->name('admin_profil');
route::post('admin/update_profil',[adminController::class,'update_profil'])->name('update_profil');
route::get('admin/berita',[adminController::class,'berita'])->name('admin_berita');
route::post('admin/tambah_berita',[adminController::class,'tambah_berita'])->name('tambah_berita');
route::post('admin/update_berita/{id}',[adminController::class,'update_berita'])->name('update_berita');
route::post('admin/delete_berita/{id}',[adminController::class,'delete_berita'])->name('delete_berita');
route::get('admin/agenda',[adminController::class,'agenda'])->name('admin_agenda');
route::post('admin/tambah_agenda',[adminController::class,'tambah_agenda'])->name('tambah_agenda');
route::post('admin/update_agenda/{id}',[adminController::class,'update_agenda'])->name('update_agenda');
route::post('admin/delete_agenda/{id}',[adminController::class,'delete_agenda'])->name('delete_agenda');
route::get('admin/kategori_download',[adminController::class,'kategori_download'])->name('admin_kategori_download');
route::post('admin/tambah_kategori_download',[adminController::class,'tambah_kategori_download'])->name('tambah_kategori_download');
route::post('admin/update_kategori_download',[adminController::class,'update_kategori_download'])->name('update_kategori_download');
route::post('admin/delete_kategori_download/{id}',[adminController::class,'delete_kategori_download'])->name('delete_kategori_download');
route::get('admin/download',[adminController::class,'download'])->name('admin_download');
route::post('admin/tambah_download',[adminController::class,'tambah_download'])->name('tambah_download');
route::post('admin/update_download/{id}',[adminController::class,'update_download'])->name('update_download');
route::post('admin/delete_download/{id}',[adminController::class,'delete_download'])->name('delete_download');
route::get('admin/provinsi',[adminController::class,'provinsi'])->name('admin_provinsi');
route::post('admin/tambah_provinsi',[adminController::class,'tambah_provinsi'])->name('tambah_provinsi');
route::post('admin/update_provinsi/{id}',[adminController::class,'update_provinsi'])->name('update_provinsi');
route::post('admin/delete_provinsi/{id}',[adminController::class,'delete_provinsi'])->name('delete_provinsi');
route::get('admin/kota_kabupaten',[adminController::class,'kota_kabupaten'])->name('admin_kota_kabupaten');
route::post('admin/tambah_kota_kabupaten',[adminController::class,'tambah_kota_kabupaten'])->name('tambah_kota_kabupaten');
route::post('admin/update_kota_kabupaten/{id}',[adminController::class,'update_kota_kabupaten'])->name('update_kota_kabupaten');
route::post('admin/delete_kota_kabupaten/{id}',[adminController::class,'delete_kota_kabupaten'])->name('delete_kota_kabupaten');
route::get('admin/klasifikasi_sbu_konstruksi',[adminController::class,'klasifikasi_sbu_konstruksi'])->name('klasifikasi_sbu_konstruksi');
route::post('admin/tambah_klasifikasi_sbu_konstruksi',[adminController::class,'tambah_klasifikasi_sbu_konstruksi'])->name('tambah_klasifikasi_sbu_konstruksi');
route::post('admin/update_klasifikasi_sbu_konstruksi/{id}',[adminController::class,'update_klasifikasi_sbu_konstruksi'])->name('update_klasifikasi_sbu_konstruksi');
route::post('admin/delete_klasifikasi_sbu_konstruksi/{id}',[adminController::class,'delete_klasifikasi_sbu_konstruksi'])->name('delete_klasifikasi_sbu_konstruksi');
route::get('admin/klasifikasi_sbu_non_konstruksi',[adminController::class,'klasifikasi_sbu_non_konstruksi'])->name('klasifikasi_sbu_non_konstruksi');
route::post('admin/tambah_klasifikasi_sbu_non_konstruksi',[adminController::class,'tambah_klasifikasi_sbu_non_konstruksi'])->name('tambah_klasifikasi_sbu_non_konstruksi');
route::post('admin/update_klasifikasi_sbu_non_konstruksi/{id}',[adminController::class,'update_klasifikasi_sbu_non_konstruksi'])->name('update_klasifikasi_sbu_non_konstruksi');
route::post('admin/delete_klasifikasi_sbu_non_konstruksi/{id}',[adminController::class,'delete_klasifikasi_sbu_non_konstruksi'])->name('delete_klasifikasi_sbu_non_konstruksi');
route::get('admin/sub_klasifikasi_sbu_konstruksi',[adminController::class,'sub_klasifikasi_sbu_konstruksi'])->name('sub_klasifikasi_sbu_konstruksi');
route::post('admin/tambah_sub_klasifikasi_sbu_konstruksi',[adminController::class,'tambah_sub_klasifikasi_sbu_konstruksi'])->name('tambah_sub_klasifikasi_sbu_konstruksi');
route::post('admin/update_sub_klasifikasi_sbu_konstruksi/{id}',[adminController::class,'update_sub_klasifikasi_sbu_konstruksi'])->name('update_sub_klasifikasi_sbu_konstruksi');
route::post('admin/delete_sub_klasifikasi_sbu_konstruksi/{id}',[adminController::class,'delete_sub_klasifikasi_sbu_konstruksi'])->name('delete_sub_klasifikasi_sbu_konstruksi');
route::get('admin/sub_klasifikasi_sbu_konstruksi',[adminController::class,'sub_klasifikasi_sbu_konstruksi'])->name('sub_klasifikasi_sbu_non_konstruksi');
route::post('admin/tambah_sub_klasifikasi_sbu_non_konstruksi',[adminController::class,'tambah_sub_klasifikasi_sbu_non_konstruksi'])->name('tambah_sub_klasifikasi_sbu_non_konstruksi');
route::post('admin/update_sub_klasifikasi_sbu_non_konstruksi/{id}',[adminController::class,'update_sub_klasifikasi_sbu_non_konstruksi'])->name('update_sub_klasifikasi_sbu_non_konstruksi');
route::post('admin/delete_sub_klasifikasi_sbu_non_konstruksi/{id}',[adminController::class,'delete_sub_klasifikasi_sbu_non_konstruksi'])->name('delete_sub_klasifikasi_sbu__non_konstruksi');
route::get('admin/data_perusahaan',[adminController::class,'data_perusahaan'])->name('admin_data_perusahaan');
route::post('admin/tambah_data_perusahaan',[adminController::class,'tambah_data_perusahaan'])->name('tambah_data_perusahaan');
route::post('admin/update_data_perusahaan/{id}',[adminController::class,'update_data_perusahaan'])->name('update_data_perusahaan');
route::post('admin/delete_data_perusahaan/{id}',[adminController::class,'delete_data_perusahaan'])->name('delete_data_perusahaan');
route::get('admin/sbu_konstruksi',[adminController::class,'data_perusahaan'])->name('admin_sbu_konstruksi');
route::post('admin/tambah_sbu_konstruksi',[adminController::class,'tambah_data_perusahaan'])->name('tambah_sbu_konstruksi');
route::post('admin/update_sbu_konstruksi/{id}',[adminController::class,'update_data_perusahaan'])->name('update_sbu_konstruksi');
route::post('admin/delete_sbu_konstruksi/{id}',[adminController::class,'delete_data_perusahaan'])->name('delete_sbu_konstruksi');
route::get('admin/sbu_non_konstruksi',[adminController::class,'data_perusahaan'])->name('admin_sbu_non_konstruksi');
route::post('admin/tambah_sbu_non_konstruksi',[adminController::class,'tambah_data_perusahaan'])->name('tambah_sbu_non_konstruksi');
route::post('admin/update_sbu_non_konstruksi/{id}',[adminController::class,'update_data_perusahaan'])->name('update_sbu_non_konstruksi');
route::post('admin/delete_sbu_non_konstruksi/{id}',[adminController::class,'delete_data_perusahaan'])->name('delete_sbu_non_konstruksi');
route::get('admin/pembayaran_kta',[adminController::class,'pembayaran_kta'])->name('pembayaran_kta');
route::get('admin/pembayaran_sbu_konstruksi',[adminController::class,'pembayaran_sbu_non_konstruksi'])->name('pembayaran_sbu_konstruksi');
route::get('admin/pembayaran_sbu_non_konstruksi',[adminController::class,'pembayaran_sbu_non_konstruksi'])->name('pembayaran_sbu_non_konstruksi');
route::get('admin/pembayaran_registrasi_sbu_konstruksi',[adminController::class,'pembayaran_registrasi_sbu_konstruksi'])->name('pembayaran_registrasi_sbu_konstruksi');
route::post('admin/pembayaran_registrasi_sbu_konstruksi/{id}',[adminController::class,'update_registrasi_sbu_konstruksi'])->name('update_registrasi_sbu_konstruksi');
route::get('admin/h_pembayaran_kta',[adminController::class,'h_pembayaran_kta'])->name('h_pembayaran_kta');
route::get('admin/h_pembayaran_sbu_konstruksi',[adminController::class,'h_pembayaran_sbu_konstruksi'])->name('h_pembayaran_sbu_konstruksi');
route::get('admin/h_pembayaran_sbu_non_konstruksi',[adminController::class,'h_pembayaran_sbu_non_konstruksi'])->name('h_pembayaran_sbu_non_konstruksi');
route::get('admin/h_pembayaran_registrasi_sbu_konstruksi',[adminController::class,'h_pembayaran_registrasi_sbu_konstruksi'])->name('h_pembayaran_registrasi_sbu_konstruksi');
route::get('admin/tentang_kami',[adminController::class,'tentang_kami'])->name('tentang_kami');
route::get('admin/update_tentang_kami/{id}',[adminController::class,'update_tentang_kami'])->name('update_tentang_kami');
route::get('admin/dokumen_download/{file}',[adminController::class,'download_dokumen'])->name('download_dokumen');
});
//---------------------------------------------------------anggota---------------------------------------------
route::get('anggota',[AnggotaController::class,'anggota'])->name('anggota');
route::get('anggota/h_p_kta',[AnggotaController::class,'h_p_kta'])->name('anggota_h_p_kta');
route::get('anggota/h_p_registrasi_sbu_konstruksi',[AnggotaController::class,'h_p_registrasi_sbu_konstruksi'])->name('anggota_h_p_registrasi_sbu_konstruksi');
route::get('anggota/h_p_sbu_konstruksi',[AnggotaController::class,'h_p_sbu_konstruksi'])->name('anggota_h_p_sbu_konstruksi');
route::get('anggota/h_p_sbu_non_konstruksi',[AnggotaController::class,'h_p_sbu_non_konstruksi'])->name('anggota_h_p_sbu_non_konstruksi');
route::get('anggota/kta_sbu_kta',[AnggotaController::class,'anggota_kta_sbu_kta'])->name('anggota_kta_sbu_kta');
route::get('anggota/sbu_konstruksi',[AnggotaController::class,'anggota_sbu_konstruksi'])->name('anggota_sbu_konstruksi');
route::get('anggota/sbu_non_konstruksi',[AnggotaController::class,'anggota_sbu_non_konstruksi'])->name('anggota_sbu_non_konstruksi');
route::get('anggota/p_kta_sbu',[AnggotaController::class,'p_kta_sbu'])->name('anggota_p_kta_sbu');
route::post('anggota/tambah_p_kta_sbu_kta',[AnggotaController::class,'tambah_p_kta_sbu_kta'])->name('tambah_p_kta_sbu_kta');
route::post('anggota/tambah_p_kta_sbu_sbu_konstruksi',[AnggotaController::class,'tambah_p_kta_sbu_sbu_konstruksi'])->name('tambah_p_kta_sbu_sbu_konstruksi');
route::post('anggota/tambah_p_kta_sbu_sbu_non_konstruksi',[AnggotaController::class,'tambah_p_kta_sbu_sbu_non_konstruksi'])->name('tambah_p_kta_sbu_sbu_non_konstruksi');
route::get('anggota/registrasi_2_sbu_konstruksi',[AnggotaController::class,'p_registrasi_2_sbu_konstruksi'])->name('anggota_registrasi_2_sbu_konstruksi');
route::post('anggota/tambah_registrasi_2_sbu_konstruksi',[AnggotaController::class,'tambah_r2_registrasi_sbu_konstruksi'])->name('tambah_r2_sbu_konstruksi');
route::post('anggota/tambah_registrasi_3_sbu_konstruksi',[AnggotaController::class,'tambah_r3_registrasi_sbu_konstruksi'])->name('tambah_r3_sbu_konstruksi');
route::get('anggota/registrasi_3_sbu_konstruksi',[AnggotaController::class,'p_registrasi_3_sbu_konstruksi'])->name('anggota_registrasi_3_sbu_konstruksi');
route::get('anggota/ubah_password',[AnggotaController::class,'ubah_user_pengguna'])->name('anggota_ubah_user_pengguna');
route::post('anggota/update_ubah_password/{id}',[AnggotaController::class,'update_ubah_user_pengguna'])->name('update_ubah_user_pengguna');
