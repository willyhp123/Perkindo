function irban_1_5() {
  let arr = new Array()
  for (let i = 8; i <= 12; i++) {
    arr.push(i)
  }
  return arr
}

function ck_full(id) {
  CKEDITOR.replace(id);
}

function ck(id) {
	CKEDITOR.replace( id, {
	// Define the toolbar groups as it is a more accessible solution.
	toolbarGroups: [{
	    "name": "basicstyles",
	    "groups": ["basicstyles"]
	},
	{
	    "name": "links",
	    "groups": ["links"]
	},
	{
	    "name": "paragraph",
	    "groups": ["list", "blocks"]
	},
	{
	    "name": "document",
	    "groups": ["mode"]
	},
	{
	    "name": "insert",
	    "groups": ["insert"]
	},
	{
	    "name": "styles",
	    "groups": ["styles"]
	}
	],
	// Remove the redundant buttons from toolbar groups defined above.
	removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
	} );
}

function ck_simple(id) {
  CKEDITOR.replace( id, {
  // Define the toolbar groups as it is a more accessible solution.
  toolbarGroups: [{
      "name": "basicstyles",
      "groups": ["basicstyles"]
  },
  {
      "name": "paragraph",
      "groups": ["list"]
  },
  {
      "name": "insert",
      "groups": ["insert"]
  },
  {
      "name": "styles",
      "groups": ["styles"]
  }
  ],
  // Remove the redundant buttons from toolbar groups defined above.
  removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,SpecialChar,Flash,Smiley,PageBreak,Iframe,HorizontalRule'
  } );
}

function ck_simple_rp(id) {
  CKEDITOR.replace( id, {
  // Define the toolbar groups as it is a more accessible solution.
  toolbarGroups: [{
      "name": "basicstyles",
      "groups": ["basicstyles"]
  },
  {
      "name": "paragraph",
      "groups": ["list"]
  },
  {
      "name": "insert",
      "groups": ["insert"]
  },
  {
      "name": "styles",
      "groups": ["styles"]
  }
  ],
  // Remove the redundant buttons from toolbar groups defined above.
  removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Flash,Smiley,PageBreak,Iframe,HorizontalRule'
  } );
}

function SpecialCharactersRP( editor ) {
    editor.plugins.get( 'SpecialCharacters' ).addItems( 'Rupiah', [
        { title: 'Rp', character: 'Rp ' }
    ] );
}

function number_to_romawi(numb) {
  switch (numb) {
    case 1:
      numb = "I";
      break;
    case 2:
      numb = "II";
      break;
    case 3:
      numb = "III";
      break;
    case 4:
      numb = "IV";
      break;
    case 5:
      numb = "V";
      break;
    case 6:
      numb = "VI";
      break;
    case 7:
      numb = "VII";
      break;
    case 8:
      numb = "VIII";
      break;
    case 9:
      numb = "IX";
      break;
    case 10:
      numb = "X";
  }
  return numb;
}

function number_to_roman(numb) {
	switch (numb) {
	  case 1:
	    numb = "A";
	    break;
	  case 2:
	    numb = "B";
	    break;
	  case 3:
	    numb = "C";
	    break;
	  case 4:
	    numb = "D";
	    break;
	  case 5:
	    numb = "E";
	    break;
	  case 6:
	    numb = "F";
	    break;
	  case 7:
	    numb = "G";
	    break;
	  case 8:
	    numb = "H";
	    break;
	  case 9:
	    numb = "I";
	    break;
	  case 10:
	    numb = "J";
	}
	return numb;
}

function roman_to_number(roman) {
  switch (roman) {
    case 'A':
      roman = 1;
      break;
    case 'B':
      roman = 2;
      break;
    case 'C':
      roman = 3;
      break;
    case 'D':
      roman = 4;
      break;
    case 'E':
      roman = 5;
      break;
    case 'F':
      roman = 6;
      break;
    case 'G':
      roman = 7;
      break;
    case 'H':
      roman = 8;
      break;
    case 'I':
      roman = 9;
      break;
    case 'J':
      roman = 10;
  }
  return roman;
}

function tgl_raw_to_tgl_id(tgl) {
  var tgl_array = tgl.split('-')
  switch (tgl_array[1]) {
    case '01':
      tgl_array[1] = "Januari";
      break;
    case '02':
      tgl_array[1] = "Februari";
      break;
    case '03':
      tgl_array[1] = "Maret";
      break;
    case '04':
      tgl_array[1] = "April";
      break;
    case '05':
      tgl_array[1] = "Mei";
      break;
    case '06':
      tgl_array[1] = "Juni";
      break;
    case '07':
      tgl_array[1] = "Juli";
      break;
    case '08':
      tgl_array[1] = "Agustus";
      break;
    case '09':
      tgl_array[1] = "September";
      break;
    case '10':
      tgl_array[1] = "Oktober";
      break;
    case '11':
      tgl_array[1] = "November";
      break;
    case '12':
      tgl_array[1] = "Desember";
  }

  tgl = tgl_array.join(' ');
  return tgl;
}

function tgl_dd_MM_yyyy(id) {
	$(id).datepicker({
	    language: 'id',
	    autoclose: true,
	    todayHighlight: true,
	    format: "dd MM yyyy"
	});
}

function date_to_diff(tgl) {
  var tgl_array = tgl.split(' ')
  switch (tgl_array[1]) {
    case 'Januari':
      tgl_array[1] = "1";
      break;
    case 'Februari':
      tgl_array[1] = "2";
      break;
    case 'Maret':
      tgl_array[1] = "3";
      break;
    case 'April':
      tgl_array[1] = "4";
      break;
    case 'Mei':
      tgl_array[1] = "5";
      break;
    case 'Juni':
      tgl_array[1] = "6";
      break;
    case 'Juli':
      tgl_array[1] = "7";
      break;
    case 'Agustus':
      tgl_array[1] = "8";
      break;
    case 'September':
      tgl_array[1] = "9";
      break;
    case 'Oktober':
      tgl_array[1] = "10";
      break;
    case 'November':
      tgl_array[1] = "1";
      break;
    case 'Desember':
      tgl_array[1] = "1";
  }

  tgl = tgl_array.join('/');
  return tgl;
}

function mm_id_to_mm_eng(tgl) {
    var tgl_array = tgl.split(' ')
    switch (tgl_array[1]) {
      case 'Januari':
        tgl_array[1] = "Jan";
        break;
      case 'Februari':
        tgl_array[1] = "Feb";
        break;
      case 'Maret':
        tgl_array[1] = "Mar";
        break;
      case 'April':
        tgl_array[1] = "Apr";
        break;
      case 'Mei':
        tgl_array[1] = "May";
        break;
      case 'Juni':
        tgl_array[1] = "Jun";
        break;
      case 'Juli':
        tgl_array[1] = "Jul";
        break;
      case 'Agustus':
        tgl_array[1] = "Ags";
        break;
      case 'September':
        tgl_array[1] = "Sep";
        break;
      case 'Oktober':
        tgl_array[1] = "Oct";
        break;
      case 'November':
        tgl_array[1] = "Nov";
        break;
      case 'Desember':
        tgl_array[1] = "Dec";
    }

    tgl = tgl_array.join('-');
    return tgl;
}

function timepicker_default(id) {
	$(id).timepicker({
	    showMeridian: false,
	    icons: {
	        up: 'mdi mdi-chevron-up',
	        down: 'mdi mdi-chevron-down'
	    }
	});
}

function toast_custom(tipe, text, title) {
    Command: toastr[tipe](text, title)

    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": true,
      "progressBar": true,
      "positionClass": "toast-bottom-left",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
}