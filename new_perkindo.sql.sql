-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Apr 2022 pada 08.08
-- Versi server: 10.4.20-MariaDB
-- Versi PHP: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_perkindo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agamas`
--

CREATE TABLE `agamas` (
  `id_agama` bigint(20) UNSIGNED NOT NULL,
  `agama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `agamas`
--

INSERT INTO `agamas` (`id_agama`, `agama`, `created_at`, `updated_at`) VALUES
(8, 'Islam', '2022-04-08 23:26:51', '2022-04-08 23:26:51'),
(9, 'Kristen', '2022-04-08 23:26:59', '2022-04-08 23:26:59'),
(10, 'Khatolik', '2022-04-08 23:27:05', '2022-04-08 23:27:05'),
(11, 'Hindu', '2022-04-08 23:27:12', '2022-04-08 23:27:12'),
(12, 'Budha', '2022-04-08 23:27:17', '2022-04-08 23:27:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `agendas`
--

CREATE TABLE `agendas` (
  `id_agenda` bigint(20) UNSIGNED NOT NULL,
  `tanggal_mulai` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_selesai` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_agenda` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggotas`
--

CREATE TABLE `anggotas` (
  `id_anggota` bigint(20) UNSIGNED NOT NULL,
  `nomor_keanggotaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_perusahaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_penanggung_jawab` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_perusahaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi_id` int(11) DEFAULT NULL,
  `kota_kabupaten_id` int(11) DEFAULT NULL,
  `telepon_telex_fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kta_sampai` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_kta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_penanggung_jawab` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `anggotas`
--

INSERT INTO `anggotas` (`id_anggota`, `nomor_keanggotaan`, `nama_perusahaan`, `nama_penanggung_jawab`, `alamat_perusahaan`, `provinsi_id`, `kota_kabupaten_id`, `telepon_telex_fax`, `no_hp_1`, `no_hp_2`, `kta_sampai`, `foto_kta`, `password`, `email`, `foto_penanggung_jawab`, `level`, `created_at`, `updated_at`) VALUES
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$rE8bbbirDUGxfOVfNvZ74uZfSBAvL3aKi0GtPGz4Oaccp3TcydNR6', 'admin@gmail.com', NULL, 'admin', '2022-04-08 23:24:11', '2022-04-08 23:24:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `beritas`
--

CREATE TABLE `beritas` (
  `id_berita` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_sbu_konstruksis`
--

CREATE TABLE `detail_sbu_konstruksis` (
  `id_detail_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `sub_klasifikasi_sbu_konstruksi_id` int(11) NOT NULL,
  `sbu_konstruksi_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `detail_sbu_konstruksis`
--

INSERT INTO `detail_sbu_konstruksis` (`id_detail_sbu_konstruksi`, `sub_klasifikasi_sbu_konstruksi_id`, `sbu_konstruksi_id`, `created_at`, `updated_at`) VALUES
(4, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_sbu_non_konstruksis`
--

CREATE TABLE `detail_sbu_non_konstruksis` (
  `id_detail_sbu_non_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `sbu_non_konstruksi_id` bigint(20) DEFAULT NULL,
  `klasifikasi_sbu_non_konstruksi_id` bigint(20) DEFAULT NULL,
  `s_k_sbu_non_konstruksi_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `downloads`
--

CREATE TABLE `downloads` (
  `id_download` bigint(20) UNSIGNED NOT NULL,
  `kategori_download_id` int(11) NOT NULL,
  `dokumen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto_penguruses`
--

CREATE TABLE `foto_penguruses` (
  `id_pengurus` bigint(20) UNSIGNED NOT NULL,
  `periode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `foto_penguruses`
--

INSERT INTO `foto_penguruses` (`id_pengurus`, `periode`, `foto`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatans`
--

CREATE TABLE `jabatans` (
  `id_jabatan` bigint(20) UNSIGNED NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `jabatans`
--

INSERT INTO `jabatans` (`id_jabatan`, `jabatan`, `created_at`, `updated_at`) VALUES
(4, 'Ketua', '2022-04-08 23:28:41', '2022-04-08 23:28:41'),
(5, 'Wakil Ketua I', '2022-04-08 23:28:53', '2022-04-08 23:28:53'),
(6, 'Wakil Ketua II', '2022-04-08 23:29:03', '2022-04-08 23:29:03'),
(7, 'Wakil Ketua III', '2022-04-08 23:29:23', '2022-04-08 23:29:23'),
(8, 'Wakil Ketua IV', '2022-04-08 23:29:35', '2022-04-08 23:29:35'),
(9, 'Wakil Ketua V', '2022-04-08 23:29:50', '2022-04-08 23:29:50'),
(10, 'Wakil Ketua Vl', '2022-04-08 23:30:05', '2022-04-08 23:30:05'),
(11, 'Wakil Ketua VII', '2022-04-08 23:30:37', '2022-04-08 23:30:37'),
(12, 'Wakil Ketua VIII', '2022-04-08 23:30:52', '2022-04-08 23:30:52'),
(13, 'Wakil Ketua IX', '2022-04-08 23:31:35', '2022-04-08 23:31:35'),
(14, 'Wakil Ketua X', '2022-04-08 23:31:57', '2022-04-08 23:31:57'),
(15, 'Sekretaris', '2022-04-08 23:32:25', '2022-04-08 23:32:25'),
(16, 'Wakil Sekretaris I', '2022-04-08 23:32:40', '2022-04-08 23:32:40'),
(17, 'Wakil Sekretaris II', '2022-04-08 23:32:56', '2022-04-08 23:32:56'),
(18, 'Bendahara', '2022-04-08 23:33:07', '2022-04-08 23:33:07'),
(19, 'Wakil Bendahara I', '2022-04-08 23:33:18', '2022-04-08 23:33:18'),
(20, 'Wakil Bendahara II', '2022-04-08 23:33:29', '2022-04-08 23:33:29'),
(21, 'Badan Verifikasi Validasi awal', '2022-04-08 23:33:52', '2022-04-08 23:33:52'),
(22, 'Badan Pelaksana Daerah', '2022-04-08 23:34:07', '2022-04-08 23:34:07'),
(23, 'Wakil Ketua XI', '2022-04-08 23:34:32', '2022-04-08 23:34:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_downloads`
--

CREATE TABLE `kategori_downloads` (
  `id_kategori_download` bigint(20) UNSIGNED NOT NULL,
  `kategori_download` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `klasifikasi_sbu_konstruksis`
--

CREATE TABLE `klasifikasi_sbu_konstruksis` (
  `id_klasifikasi_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `klasifikasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `klasifikasi_sbu_non_konstruksis`
--

CREATE TABLE `klasifikasi_sbu_non_konstruksis` (
  `id_k_sbu_non_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `klasifikasi_non_konstruksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kota_kabupatens`
--

CREATE TABLE `kota_kabupatens` (
  `id_kota_kabupaten` bigint(20) UNSIGNED NOT NULL,
  `provinsi_id` int(11) NOT NULL,
  `kota_kabupaten` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ktas`
--

CREATE TABLE `ktas` (
  `id_kta` bigint(20) UNSIGNED NOT NULL,
  `anggota_id` bigint(20) DEFAULT NULL,
  `kta_berlaku_sampai` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_kta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aktif` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_02_08_043020_create_agamas_table', 1),
(6, '2022_02_08_043053_create_agendas_table', 1),
(7, '2022_02_08_043114_create_anggotas_table', 1),
(8, '2022_02_08_043144_create_beritas_table', 1),
(9, '2022_02_08_043214_create_detail_sbu_konstruksis_table', 1),
(10, '2022_02_08_043240_create_detail_sbu_non_konstruksis_table', 1),
(11, '2022_02_08_043319_create_downloads_table', 1),
(12, '2022_02_08_043421_create_foto_penguruses_table', 1),
(13, '2022_02_08_043439_create_jabatans_table', 1),
(14, '2022_02_08_043653_create_kategori_downloads_table', 1),
(15, '2022_02_08_043734_create_klasifikasi_sbu_konstruksis_table', 1),
(16, '2022_02_08_043809_create_klasifikasi_sbu_non_konstruksis_table', 1),
(17, '2022_02_08_043841_create_kota_kabupatens_table', 1),
(18, '2022_02_08_043944_create_pembayaran_ktas_table', 1),
(19, '2022_02_08_044018_create_pembayaran_registrasi_sbu_konstruksis_table', 1),
(20, '2022_02_08_044103_create_pembayaran_sbu_non_konstruksis_table', 1),
(21, '2022_02_08_044131_create_penguruses_table', 1),
(22, '2022_02_08_044156_create_profils_table', 1),
(23, '2022_02_08_044219_create_provinsis_table', 1),
(24, '2022_02_08_044259_create_registrasi_sbu_konstruksis_table', 1),
(25, '2022_02_08_044325_create_rekening_pembayarans_table', 1),
(26, '2022_02_08_044407_create_sbu_konstruksis_table', 1),
(27, '2022_02_08_044431_create_sbu_non_konstruksis_table', 1),
(28, '2022_02_08_044445_create_slides_table', 1),
(29, '2022_02_08_044630_create_struktur_organisasis_table', 1),
(30, '2022_02_08_044700_create_sub_klasifikasi_sbu_konstruksis_table', 1),
(31, '2022_02_08_044720_create_sub_klasifikasi_sbu_non_konstruksis_table', 1),
(32, '2022_02_08_044749_create_tentang_kamis_table', 1),
(33, '2022_02_08_044801_create_user_penggunas_table', 1),
(34, '2022_02_08_053159_create_ktas_table', 1),
(35, '2022_02_08_054213_create_pembayaran_sbu_konstruksis_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_ktas`
--

CREATE TABLE `pembayaran_ktas` (
  `id_pembayaran_kta` bigint(20) UNSIGNED NOT NULL,
  `rekening_pembayaran_id` int(11) NOT NULL,
  `anggota_id` int(11) NOT NULL,
  `no_rekening` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atas_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sudah_lihat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sudah_diproses` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_registrasi_sbu_konstruksis`
--

CREATE TABLE `pembayaran_registrasi_sbu_konstruksis` (
  `id_pembayaran_registrasi_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `rekening_pembayaran_id` int(11) NOT NULL,
  `anggota_id` int(11) NOT NULL,
  `no_rekening` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atas_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registrasi_tahun_ke` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_registrasi_tahun_ke` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sudah_lihat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sudah_diproses` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_sbu_konstruksis`
--

CREATE TABLE `pembayaran_sbu_konstruksis` (
  `id_pembayaran_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `rekening_pembayaran_id` int(11) NOT NULL,
  `anggota_id` int(11) NOT NULL,
  `no_rekening` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atas_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sudah_dilihat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sudah_diproses` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran_sbu_non_konstruksis`
--

CREATE TABLE `pembayaran_sbu_non_konstruksis` (
  `id_pembayaran_sbu_non_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `anggota_id` int(11) NOT NULL,
  `rekening_pembayaran_id` int(11) NOT NULL,
  `no_rekening` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `atas_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sudah_lihat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sudah_diproses` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penguruses`
--

CREATE TABLE `penguruses` (
  `id_pengurus` bigint(20) UNSIGNED NOT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_ktp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_hp_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agama_id` int(11) NOT NULL,
  `jabatan_id` int(255) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `profils`
--

CREATE TABLE `profils` (
  `id_profil` bigint(20) UNSIGNED NOT NULL,
  `profil` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kontak_perkindo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_lokasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `profils`
--

INSERT INTO `profils` (`id_profil`, `profil`, `kontak_perkindo`, `foto_lokasi`, `created_at`, `updated_at`) VALUES
(1, '<p>Hanya aku</p>', '<p>Hanya kamu</p>', 'Formula diamond crystal and trendy bling.PNG', NULL, '2022-02-27 22:14:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsis`
--

CREATE TABLE `provinsis` (
  `id_provinsi` bigint(20) UNSIGNED NOT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `provinsis`
--

INSERT INTO `provinsis` (`id_provinsi`, `provinsi`, `created_at`, `updated_at`) VALUES
(2, 'Kalimantan Barat', '2022-02-13 20:28:36', '2022-02-13 20:28:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `registrasi_sbu_konstruksis`
--

CREATE TABLE `registrasi_sbu_konstruksis` (
  `id_registrasi_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `anggota_id` bigint(20) DEFAULT NULL,
  `r_pembayaran_id` bigint(20) DEFAULT NULL,
  `atas_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bukti_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registrasi_tahun_ke` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_registrasi_tahun_ke` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekening_pembayarans`
--

CREATE TABLE `rekening_pembayarans` (
  `id_rekening_pembayaran` bigint(20) UNSIGNED NOT NULL,
  `nama_bank` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_rek` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atas_nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `rekening_pembayarans`
--

INSERT INTO `rekening_pembayarans` (`id_rekening_pembayaran`, `nama_bank`, `no_rek`, `atas_nama`, `created_at`, `updated_at`) VALUES
(9, 'Mandiri', '900-00-36132489', 'an.Mardiansyah', '2022-04-08 23:27:56', '2022-04-08 23:27:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sbu_konstruksis`
--

CREATE TABLE `sbu_konstruksis` (
  `id_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `anggota_id` int(11) NOT NULL,
  `no_seri_formulir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_masuk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `berlaku_sampai` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registrasi_tahun_ke_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registrasi_tahun_ke_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tenaga_ahli` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sbu_non_konstruksis`
--

CREATE TABLE `sbu_non_konstruksis` (
  `id_sbu_non_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `no_seri_formulir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anggota_id` int(11) DEFAULT NULL,
  `tanggal_dikeluarkan_sbu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pj_operasional` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `slides`
--

CREATE TABLE `slides` (
  `id_slide` bigint(20) UNSIGNED NOT NULL,
  `gambar_slide` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aktif` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `struktur_organisasis`
--

CREATE TABLE `struktur_organisasis` (
  `id_struktur_organisasi` bigint(20) UNSIGNED NOT NULL,
  `periode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `struktur_organisasis`
--

INSERT INTO `struktur_organisasis` (`id_struktur_organisasi`, `periode`, `gambar`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_klasifikasi_sbu_konstruksis`
--

CREATE TABLE `sub_klasifikasi_sbu_konstruksis` (
  `id_sub_klasifikasi_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL,
  `k_sbu_konstruksi_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_klasifikasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lingkup_pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_klasifikasi_sbu_non_konstruksis`
--

CREATE TABLE `sub_klasifikasi_sbu_non_konstruksis` (
  `id_sub_k_sbu_non_k` bigint(20) UNSIGNED NOT NULL,
  `k_sbu_non_konstruksi_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_klasifikasi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lingkup_pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tentang_kamis`
--

CREATE TABLE `tentang_kamis` (
  `id_tentang_kami` bigint(20) UNSIGNED NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `misi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sejarah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_hp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tentang_kamis`
--

INSERT INTO `tentang_kamis` (`id_tentang_kami`, `deskripsi`, `visi`, `misi`, `sejarah`, `nomor_hp`, `email1`, `email2`, `alamat`, `map`, `instagram`, `facebook`, `created_at`, `updated_at`) VALUES
(1, 'Ada kami<br>', '<p>ada kami<br></p>', '<p>ada kami<br></p>', '<p>ada kami<br></p>', '087734635840', 'admin@gmail.com', 'admin2@gmail.com', '<p>ada kami<br></p>', 'Formula diamond crystal and trendy bling.PNG', 'www.instagram.com', 'www.instagram.com', NULL, '2022-02-19 05:12:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_penggunas`
--

CREATE TABLE `user_penggunas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `agamas`
--
ALTER TABLE `agamas`
  ADD PRIMARY KEY (`id_agama`);

--
-- Indeks untuk tabel `agendas`
--
ALTER TABLE `agendas`
  ADD PRIMARY KEY (`id_agenda`);

--
-- Indeks untuk tabel `anggotas`
--
ALTER TABLE `anggotas`
  ADD PRIMARY KEY (`id_anggota`),
  ADD UNIQUE KEY `anggotas_nomor_keanggotaan_unique` (`nomor_keanggotaan`);

--
-- Indeks untuk tabel `beritas`
--
ALTER TABLE `beritas`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indeks untuk tabel `detail_sbu_konstruksis`
--
ALTER TABLE `detail_sbu_konstruksis`
  ADD PRIMARY KEY (`id_detail_sbu_konstruksi`),
  ADD UNIQUE KEY `sbu_konstruksi_id` (`sbu_konstruksi_id`);

--
-- Indeks untuk tabel `detail_sbu_non_konstruksis`
--
ALTER TABLE `detail_sbu_non_konstruksis`
  ADD PRIMARY KEY (`id_detail_sbu_non_konstruksi`),
  ADD UNIQUE KEY `detail_sbu_non_konstruksis_sbu_non_konstruksi_id_unique` (`sbu_non_konstruksi_id`);

--
-- Indeks untuk tabel `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id_download`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `foto_penguruses`
--
ALTER TABLE `foto_penguruses`
  ADD PRIMARY KEY (`id_pengurus`);

--
-- Indeks untuk tabel `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indeks untuk tabel `kategori_downloads`
--
ALTER TABLE `kategori_downloads`
  ADD PRIMARY KEY (`id_kategori_download`);

--
-- Indeks untuk tabel `klasifikasi_sbu_konstruksis`
--
ALTER TABLE `klasifikasi_sbu_konstruksis`
  ADD PRIMARY KEY (`id_klasifikasi_sbu_konstruksi`);

--
-- Indeks untuk tabel `klasifikasi_sbu_non_konstruksis`
--
ALTER TABLE `klasifikasi_sbu_non_konstruksis`
  ADD PRIMARY KEY (`id_k_sbu_non_konstruksi`);

--
-- Indeks untuk tabel `kota_kabupatens`
--
ALTER TABLE `kota_kabupatens`
  ADD PRIMARY KEY (`id_kota_kabupaten`);

--
-- Indeks untuk tabel `ktas`
--
ALTER TABLE `ktas`
  ADD PRIMARY KEY (`id_kta`),
  ADD UNIQUE KEY `ktas_anggota_id_unique` (`anggota_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pembayaran_ktas`
--
ALTER TABLE `pembayaran_ktas`
  ADD PRIMARY KEY (`id_pembayaran_kta`);

--
-- Indeks untuk tabel `pembayaran_registrasi_sbu_konstruksis`
--
ALTER TABLE `pembayaran_registrasi_sbu_konstruksis`
  ADD PRIMARY KEY (`id_pembayaran_registrasi_sbu_konstruksi`);

--
-- Indeks untuk tabel `pembayaran_sbu_konstruksis`
--
ALTER TABLE `pembayaran_sbu_konstruksis`
  ADD PRIMARY KEY (`id_pembayaran_sbu_konstruksi`);

--
-- Indeks untuk tabel `pembayaran_sbu_non_konstruksis`
--
ALTER TABLE `pembayaran_sbu_non_konstruksis`
  ADD PRIMARY KEY (`id_pembayaran_sbu_non_konstruksi`);

--
-- Indeks untuk tabel `penguruses`
--
ALTER TABLE `penguruses`
  ADD PRIMARY KEY (`id_pengurus`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `profils`
--
ALTER TABLE `profils`
  ADD PRIMARY KEY (`id_profil`);

--
-- Indeks untuk tabel `provinsis`
--
ALTER TABLE `provinsis`
  ADD PRIMARY KEY (`id_provinsi`);

--
-- Indeks untuk tabel `registrasi_sbu_konstruksis`
--
ALTER TABLE `registrasi_sbu_konstruksis`
  ADD PRIMARY KEY (`id_registrasi_sbu_konstruksi`),
  ADD UNIQUE KEY `registrasi_sbu_konstruksis_anggota_id_unique` (`anggota_id`),
  ADD UNIQUE KEY `registrasi_sbu_konstruksis_r_pembayaran_id_unique` (`r_pembayaran_id`);

--
-- Indeks untuk tabel `rekening_pembayarans`
--
ALTER TABLE `rekening_pembayarans`
  ADD PRIMARY KEY (`id_rekening_pembayaran`);

--
-- Indeks untuk tabel `sbu_konstruksis`
--
ALTER TABLE `sbu_konstruksis`
  ADD PRIMARY KEY (`id_sbu_konstruksi`);

--
-- Indeks untuk tabel `sbu_non_konstruksis`
--
ALTER TABLE `sbu_non_konstruksis`
  ADD PRIMARY KEY (`id_sbu_non_konstruksi`);

--
-- Indeks untuk tabel `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id_slide`);

--
-- Indeks untuk tabel `struktur_organisasis`
--
ALTER TABLE `struktur_organisasis`
  ADD PRIMARY KEY (`id_struktur_organisasi`);

--
-- Indeks untuk tabel `sub_klasifikasi_sbu_konstruksis`
--
ALTER TABLE `sub_klasifikasi_sbu_konstruksis`
  ADD PRIMARY KEY (`id_sub_klasifikasi_sbu_konstruksi`);

--
-- Indeks untuk tabel `sub_klasifikasi_sbu_non_konstruksis`
--
ALTER TABLE `sub_klasifikasi_sbu_non_konstruksis`
  ADD PRIMARY KEY (`id_sub_k_sbu_non_k`);

--
-- Indeks untuk tabel `tentang_kamis`
--
ALTER TABLE `tentang_kamis`
  ADD PRIMARY KEY (`id_tentang_kami`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `user_penggunas`
--
ALTER TABLE `user_penggunas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `agamas`
--
ALTER TABLE `agamas`
  MODIFY `id_agama` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `agendas`
--
ALTER TABLE `agendas`
  MODIFY `id_agenda` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `anggotas`
--
ALTER TABLE `anggotas`
  MODIFY `id_anggota` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `beritas`
--
ALTER TABLE `beritas`
  MODIFY `id_berita` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `detail_sbu_konstruksis`
--
ALTER TABLE `detail_sbu_konstruksis`
  MODIFY `id_detail_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `detail_sbu_non_konstruksis`
--
ALTER TABLE `detail_sbu_non_konstruksis`
  MODIFY `id_detail_sbu_non_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id_download` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `foto_penguruses`
--
ALTER TABLE `foto_penguruses`
  MODIFY `id_pengurus` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id_jabatan` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `kategori_downloads`
--
ALTER TABLE `kategori_downloads`
  MODIFY `id_kategori_download` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `klasifikasi_sbu_konstruksis`
--
ALTER TABLE `klasifikasi_sbu_konstruksis`
  MODIFY `id_klasifikasi_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `klasifikasi_sbu_non_konstruksis`
--
ALTER TABLE `klasifikasi_sbu_non_konstruksis`
  MODIFY `id_k_sbu_non_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `kota_kabupatens`
--
ALTER TABLE `kota_kabupatens`
  MODIFY `id_kota_kabupaten` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `ktas`
--
ALTER TABLE `ktas`
  MODIFY `id_kta` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `pembayaran_ktas`
--
ALTER TABLE `pembayaran_ktas`
  MODIFY `id_pembayaran_kta` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `pembayaran_registrasi_sbu_konstruksis`
--
ALTER TABLE `pembayaran_registrasi_sbu_konstruksis`
  MODIFY `id_pembayaran_registrasi_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `pembayaran_sbu_konstruksis`
--
ALTER TABLE `pembayaran_sbu_konstruksis`
  MODIFY `id_pembayaran_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `pembayaran_sbu_non_konstruksis`
--
ALTER TABLE `pembayaran_sbu_non_konstruksis`
  MODIFY `id_pembayaran_sbu_non_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `penguruses`
--
ALTER TABLE `penguruses`
  MODIFY `id_pengurus` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `profils`
--
ALTER TABLE `profils`
  MODIFY `id_profil` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `provinsis`
--
ALTER TABLE `provinsis`
  MODIFY `id_provinsi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `registrasi_sbu_konstruksis`
--
ALTER TABLE `registrasi_sbu_konstruksis`
  MODIFY `id_registrasi_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `rekening_pembayarans`
--
ALTER TABLE `rekening_pembayarans`
  MODIFY `id_rekening_pembayaran` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `sbu_konstruksis`
--
ALTER TABLE `sbu_konstruksis`
  MODIFY `id_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `sbu_non_konstruksis`
--
ALTER TABLE `sbu_non_konstruksis`
  MODIFY `id_sbu_non_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `slides`
--
ALTER TABLE `slides`
  MODIFY `id_slide` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `struktur_organisasis`
--
ALTER TABLE `struktur_organisasis`
  MODIFY `id_struktur_organisasi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `sub_klasifikasi_sbu_konstruksis`
--
ALTER TABLE `sub_klasifikasi_sbu_konstruksis`
  MODIFY `id_sub_klasifikasi_sbu_konstruksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `sub_klasifikasi_sbu_non_konstruksis`
--
ALTER TABLE `sub_klasifikasi_sbu_non_konstruksis`
  MODIFY `id_sub_k_sbu_non_k` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tentang_kamis`
--
ALTER TABLE `tentang_kamis`
  MODIFY `id_tentang_kami` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user_penggunas`
--
ALTER TABLE `user_penggunas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
