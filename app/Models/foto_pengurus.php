<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class foto_pengurus extends Model
{
    protected $table = 'foto_penguruses';
    protected $primaryKey = 'id_pengurus';
    protected $fillable = [
        'periode','foto'
    ];
}
