<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detail_sbu_non_konstruksi extends Model
{
   protected $table = 'detail_sbu_non_konstruksis';
   protected $primaryKey = 'id_detail_sbu_non_konstruksi';
   protected $fillable = [
     'sbu_non_konstruksi_id','klasifikasi_sbu_non_konstruksi_id','sub_klasifikasi_sbu_non_konstruksi_id'
   ];
   public function detail_sbu_non_konstruksi(){
       return $this->belongsTo(detail_sbu_non_konstruksi::class,'id_sbu_non_konstruksi',
           'id_detail_sbu_non_konstruksi');
   }
}
