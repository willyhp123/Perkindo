<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class downloads extends Model
{
    protected $table = 'downloads';
    protected $primaryKey = 'id_download';
    protected $fillable= [
        'kategori_download_id','dokumen','judul'
    ];
}
