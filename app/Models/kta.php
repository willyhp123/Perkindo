<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kta extends Model
{
    protected $table = 'ktas';
    protected $primaryKey = 'id_kta';
    protected $fillable = [
        'anggota_id','kta_berlaku_sampai','file_kta','aktif'
    ];
}
