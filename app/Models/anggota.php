<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class anggota extends Model
{
    protected $primaryKey = 'id_anggota';
    protected $table = 'anggotas';
    protected $fillable = [
        'nomor_keanggotaan','nama_perusahaan','nama_penanggung_jawab','alamat_perusahaan',
        'provinsi_id','kota_kabupaten_id','telepon_telex_fax','no_hp_1','no_hp_2','password','email','foto_penanggung_jawab'
        ,'level','kta_sampai','foto_kta'
    ];
}
