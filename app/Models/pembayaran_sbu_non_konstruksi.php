<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pembayaran_sbu_non_konstruksi extends Model
{
    protected $table = 'pembayaran_sbu_non_konstruksis';
    protected $primaryKey = 'id_pembayaran_sbu_non_konstruksi';
    protected $fillable = [
      'anggota_id','rekening_pembayaran_id','atas_nama','no_rekening','keterangan','bukti_pembayaran',
       'sudah_lihat','sudah_diproses'
    ];
}
