<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profils extends Model
{
    protected $table = 'profils';
    protected $primaryKey = 'id_profil';
    protected $fillable = [
      'profil','kontak_perindo','foto_lokasi'
    ];
}
