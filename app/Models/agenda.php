<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class agenda extends Model
{
    protected $table ='agendas';
    protected $primaryKey = 'id_agenda';
    protected $fillable = [
        'tanggal_mulai','tanggal_selesai','nama_agenda',''
    ];
}
