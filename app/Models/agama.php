<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class agama extends Model
{
    protected $table = 'agamas';
    protected $primaryKey = 'id_agama';
    protected $fillable = [
        'agama'
    ];

    public function pengurus_agama(){
        return $this->hasOne(pengurus::class,'agama_id','id_agama');
    }


}

