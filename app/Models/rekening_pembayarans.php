<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rekening_pembayarans extends Model
{
    protected $table = 'rekening_pembayarans';
    protected $primaryKey = 'id_rekening_pembayaran';
    protected $fillable = [
        'nama_bank','no_rek','atas_nama'
    ];
}
