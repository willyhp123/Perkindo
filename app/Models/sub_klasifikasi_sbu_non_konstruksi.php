<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sub_klasifikasi_sbu_non_konstruksi extends Model
{
    protected $table = 'sub_klasifikasi_sbu_non_konstruksis';
    protected $primaryKey = 'id_sub_k_sbu_non_k';
    protected $fillable = [
        'k_sbu_non_konstruksi_id','kode','sub_klasifikasi','lingkup_pekerjaan','keterangan'
    ];
}
