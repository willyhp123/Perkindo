<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pengurus extends Model
{
    protected $table = 'penguruses' ;
    protected $primaryKey = 'id_pengurus';
    protected $fillable = [
        'nik','nama','no_ktp','tempat_lahir','tanggal_lahir','jenis_kelamin','agama_id','alamat','no_hp_1','no_hp_2',
        'jabatan_id','email','foto_pengurus','password'
    ];
    public function pengurus_agama(){
        return $this->belongsTo(agama::class,'agama_id','id_agama');
    }
    public function pengurus_jabatan(){
        return $this->belongsTo(jabatan::class,'jabatan_id','id_jabatan');
    }
}
