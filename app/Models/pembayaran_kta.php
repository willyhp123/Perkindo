<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pembayaran_kta extends Model
{
    protected $table = 'pembayaran_ktas';
    protected $primaryKey = 'id_pembayaran_kta';
    protected $fillable = [
        'anggota_id','rekening_pembayaran_id','no_rekening','atas_nama','keterangan','bukti_pembayaran',
        'sudah_lihat','sudah_diproses'
    ];

}
