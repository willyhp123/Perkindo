<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class provinsi extends Model
{
    protected $table = 'provinsis';
    protected $primaryKey ='id_provinsi';
    protected $fillable = [
      'provinsi'
    ];
}
