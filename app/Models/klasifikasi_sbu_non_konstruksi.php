<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class klasifikasi_sbu_non_konstruksi extends Model
{
    protected $table = 'klasifikasi_sbu_non_konstruksis';
    protected $primaryKey = 'id_k_sbu_non_konstruksi';
    protected $fillable =[
        'klasifikasi_non_konstruksi'
    ];
}
