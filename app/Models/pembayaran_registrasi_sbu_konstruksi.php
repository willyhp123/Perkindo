<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pembayaran_registrasi_sbu_konstruksi extends Model
{
    protected $table ='pembayaran_registrasi_sbu_konstruksis';
    protected $primaryKey = 'id_pembayaran_registrasi_sbu_konstruksi';
    protected $fillable = [
        'anggota_id','rekening_pembayaran_id','no_rekening','atas_nama','keterangan','bukti_pembayaran'
        ,'registrasi_tahun_ke','tanggal_registrasi_tahun_ke','sudah_lihat','sudah_diproses'
    ];
}
