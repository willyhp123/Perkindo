<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kota_kabupaten extends Model
{
    protected $table = 'kota_kabupatens';
    protected $primaryKey = 'id_kota_kabupaten';
    protected $fillable = [
        'provinsi_id','kota_kabupaten',
    ];
}
