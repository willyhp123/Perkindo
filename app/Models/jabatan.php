<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jabatan extends Model
{
    protected $table = 'jabatans';
    protected $primaryKey  = 'id_jabatan';
    protected $fillable = [
        'jabatan'
    ];
    public function pengurus_jabatan(){
        $this->hasOne(jabatan::class,'jabatan_id','id_jabatan');
    }
}
