<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class struktur_organisasi extends Model
{
    protected $table = 'struktur_organisasis';
    protected $primaryKey = 'id_struktur_organisasi';
    protected $fillable = [
        'periode','gambar',''
    ];
}
