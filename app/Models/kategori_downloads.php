<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kategori_downloads extends Model
{
     protected $table = 'kategori_downloads';
     protected $primaryKey = 'id_kategori_download';
     protected $fillable = [
         'kategori_download'
     ];
}
