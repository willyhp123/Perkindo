<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class registrasi_sbu_konstruksi extends Model
{
    protected $table = 'registrasi_sbu_konstruksis';
    protected $primaryKey = 'id_registrasi_sbu_konstruksi';
    protected $fillable = [
        'anggota_id','rekening_pembayaran_id','atas_nama','keterangan','bukti_pembayaran','registrasi_tahun_ke',
        'tanggal_registrasi_tahun_ke'
    ];
}
