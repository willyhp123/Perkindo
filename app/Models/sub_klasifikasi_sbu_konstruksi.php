<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sub_klasifikasi_sbu_konstruksi extends Model
{
    protected $table = 'sub_klasifikasi_sbu_konstruksis';
    protected $primaryKey = 'id_sub_klasifikasi_sbu_konstruksi';
    protected $fillable = [
        'k_sbu_konstruksi_id','kode','sub_klasifikasi','lingkup_pekerjaan','keterangan'
    ];

}
