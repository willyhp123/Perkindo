<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class klasifikasi_sbu_konstruksi extends Model
{
    protected $table = 'klasifikasi_sbu_konstruksis';
    protected $primaryKey = 'id_klasifikasi_sbu_konstruksi';
    protected $fillable = [
      'klasifikasi'
    ];
}
