<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class slide extends Model
{
    protected $table = 'slides';
    protected $primaryKey = 'id_slide';
    protected $fillable = [
        'gambar_slide','judul','url','aktif'
    ];
}
