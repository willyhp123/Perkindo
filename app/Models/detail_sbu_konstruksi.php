<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detail_sbu_konstruksi extends Model
{
    protected $table = 'detail_sbu_konstruksis';
    protected $primaryKey = 'id_detail_sbu_konstruksi';
    protected $fillable = [
        'sbu_konstruksi_id','klasifikasi_sbu_konstruksi_id','sub_klasifikasi_sbu_kosntruksi_id'
    ];
    public function sbu_konstruksi(){
        return $this->belongsTo(sbu_konstruksi::class,'id_sbu_konstruksi',
            'id_detail_sbu_konstruksi');
    }
}
