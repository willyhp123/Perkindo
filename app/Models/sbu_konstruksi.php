<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sbu_konstruksi extends Model
{
    protected $table = 'sbu_konstruksis';
    protected $primaryKey ='id_sbu_konstruksi';
    protected $fillable = [
        'no_seri_formulir','anggota_id','tanggal_masuk','berlaku_sampai','registrasi_tahun_ke_2',
        'registrasi_tahun_ke_3','tenaga_ahli','foto_pengurus'
    ];
    public function detail_sbu_konstruksi(){
        return $this->hasMany(detail_sbu_konstruksi::class,
            'sbu_konstruksi_id','id_sbu_konstruksi');
    }
}
