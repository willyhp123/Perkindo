<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class berita extends Model
{
   protected $table = 'beritas';
    protected $primaryKey = 'id_berita';
   protected $fillable = [
       'judul','isi','url','foto_pengurus','publish'
   ];

}
