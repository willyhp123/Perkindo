<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tentang_kami extends Model
{
    protected $table = 'tentang_kamis';
    protected $primaryKey = 'id_tentang_kami';
    protected $fillable = [
        'deskripsi','visi','misi','sejarah','nomor_hp','email1','email2','alamat','map',
        'instagram','facebook'
    ];
}
