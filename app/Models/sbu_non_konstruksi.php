<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sbu_non_konstruksi extends Model
{
    protected $table = 'sbu_non_konstruksis';
    protected $primaryKey ='id_sbu_non_konstruksi';
    protected $fillable = [
        'anggota_id','rekening_pembayaran_id','no_rekening','atas_nama','keterangan','bukti_pembayaran'
        ,'sudah_lihat','sudah_diproses'
    ];
    public function detail_sbu_non_konstruksi(){
        return $this->hasMany(detail_sbu_non_konstruksi::class,
            'sbu_non_konstruksi_id','id_sbu_non_konstruksi');
    }
}
