<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class kota_kabupatenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provinsi_id'=>'required',
            'kota_kabupaten' =>'required'
        ];
    }
    public function messages()
    {
        return[
          'provinsi_id.required'=>'Provinsi Tidak Boleh Kosong',
          'kota_kabupaten_id.required'=>'Kota Kabupaten Tidak Boleh Kosong'
        ];
    }
}
