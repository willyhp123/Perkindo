<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class rekening_pembayaranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_bank'=>'required',
            'atas_nama'=>'required',
            'no_rek'=>'required'
        ];
    }
    public function messages()
    {
        return [
          'nama_bank.required'=>'Nama Bank Tidak Boleh Kosong',
          'atas_nama.required'=>'Atas Nama Tidak Boleh Kosong',
          'no_rek.required' =>'No Rekening Tidak Boleh Kosong',
        ];
    }
}
