<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class slideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gambar_slide'=>'required',
            'judul' =>'required',
            'url'=> 'required',
        ];
    }
    public function messages()
    {
        return [
            'gambar_slide.required'=>'Gambar Slide Tidak Boleh Kosong',
            'judul.required'=>'Judul Tidak Boleh Kosong',
            'url.required'=>'Url Tidak Boleh Kosong'
        ];
    }
}
