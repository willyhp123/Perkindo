<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class downloadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kategori_download_id'=>'required',
            'dokumen'=>'required',
            'judul'=>'required',
        ];
    }
    public function messages()
    {
        return [
          'kategori_download_id.required'=>'Kategori_download Tidak Boleh Kosong',
          'dokumen.required'=>'Dokumen Tidak Boleh Kosong',
          'judul.required'=>'judul Tidak Boleh Kosong',
        ];
    }
}
