<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgendaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal_mulai'=>'required',
            'tanggal_selesai'=>'required',
            'nama_agenda'=>'required',
        ];
    }
    public function messages()
    {
        return [
          'tanggal_mulai.required'=>'Tanggal Mulai Tidak Boleh Kosong',
          'tanggal_selesai.required'=>'Tanggal Selesai Tidak Boleh Kosong',
          'nama_agenda.required'=>'Nama Agenda Tidak Boleh Kosong'
        ];
    }
}
