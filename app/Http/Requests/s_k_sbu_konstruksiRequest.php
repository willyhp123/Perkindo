<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class s_k_sbu_konstruksiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'k_sbu_konstruksi_id'=>'required',
            'kode'=>'required',
            'sub_klasifikasi'=>'required',
            'lingkup_pekerjaan'=>'required',
            'keterangan'=>'required',
        ];
    }
    public function messages()
    {
        return[
            'k_sbu_konstruksi_id.required'=>'Klasifikasi Sbu Konstruksi Tidak Boleh Kosong',
            'kode.required'=>'Kode Tidak Boleh Kosong',
            'sub_klasifikasi.required'=>'Sub Klasifikasi Tidak Boleh Kosong',
            'lingkup_pekerjaan.required'=>'Lingkup Pekerjaan Tidak Boleh Kosong',
            'keterangan.required'=>'Keterangan Tidak Boleh Kosong'
        ];
    }
}
