<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class s_k_sbu_non_konstruksiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'k_sbu_non_konstruksi_id'=>'required',
            'kode'=>'required',
            'sub_klasifikasi'=>'required',
            'lingkup_pekerjaan'=>'required',
            'keterangan'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'k_sbu_non_konstruksi_id.required'=>'Klasifikasi Sbu Non Konstruksi Tidak Boleh Kosong',
            'kode.required'=>'Kode Tidak Boleh Kosong',
            'sub_klasifikasi.required'=>'sub_klasifikasi Tidak Boleh Kosong',
            'lingkup_pekerjaan.required'=>'Lingkup_pekerjaan Tidak boleh Kosong',
            'keterangan.required' =>'Keterangan Tidak Boleh Kosong',
        ];
    }
}
