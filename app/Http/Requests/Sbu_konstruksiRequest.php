<?php

namespace App\Http\Requests;

use App\Rules\tanggalRule;
use Illuminate\Foundation\Http\FormRequest;

class Sbu_konstruksiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'anggota_id'=>'required',
            'no_seri_formulir'=>'required',
            'tanggal_masuk'=>'required', new tanggalRule(),
            'berlaku_sampai'=>'required', new tanggalRule(),
            'tenaga_ahli'=>'required',
            'foto'=>'required'
        ];
    }
    public function messages()
    {
        return [
          'anggota_id.required'=>'Anggota Tidak Boleh Kosong',
          'no_seri_formulir.required'=>'No Seri Formulir Tidak Boleh Kosong',

          'tenaga_ahli.required'=>'Tenaga Ahli tidak Boleh Kosong',
          'foto.required'=>'Foto Tidak boleh Kosong'
        ];
    }
}
