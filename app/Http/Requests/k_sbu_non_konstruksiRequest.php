<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class k_sbu_non_konstruksiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'klasifikasi_non_konstruksi'=>'required'
        ];
    }
    public function messages()
    {
        return[
          'klasifikasi_non_konstruksi.required'=>'Klasifikasi Tidak Boleh Kosong'
        ];
    }
}
