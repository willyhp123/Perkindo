<?php

namespace App\Http\Requests;

use App\Rules\agama\agama;
use Illuminate\Foundation\Http\FormRequest;

class AgamaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agama'=> ['required'],
        ];

    }
    public function messages()
    {
        return [
            'agama.required'=>'Agama Tidak Boleh Kosong',
            'agama_update.required'=>'Agama Tidak Boleh Kosong'
        ];
    }
}
