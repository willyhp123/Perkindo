<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BeritaController extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'judul'=>'required',
            'isi'=>'required',
            'url'=>'required',
            'foto'=>'required',
        ];
    }
    public function messages()
    {
        return [
          'judul.required'=>'Judul Tidak Boleh Kosong',
          'isi.required'=>'Isi Tidak Boleh Kosong',
          'url.required'=>'Url Tidak Boleh Kosong',
          'foto.required'=>'Foto Tidak Boleh Kosong',
        ];
    }
}
