<?php

namespace App\Http\Requests\anggota;

use Illuminate\Foundation\Http\FormRequest;

class r_2_sbu_konstrruksireques extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rekening_pembayaran_id'=>'required',
            'no_rekening'=>'required',
            'atas_nama'=>'required',
            'keterangan'=>'required',
            'bukti_pembayaran'=>'required'
        ];
    }
    public function messages()
    {
        return [
          'rekening_pembayaran_id.required'=>'Rekening pembayaran tidak boleh kosong',
           'no_rekening.required'=>'no rekening tidak boleh kosong',
           'atas_nama.required'=>'Atas Nama tidak boleh kosong',
            'keterangan.required'=>'Keterangan Tidak Boleh Kosong',
            'bukti_pembayaran.required'=>'Bukti pembayaran wajib kirim file'
        ];
    }
}
