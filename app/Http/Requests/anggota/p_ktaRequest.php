<?php

namespace App\Http\Requests\anggota;

use Illuminate\Foundation\Http\FormRequest;

class p_ktaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rekening_pembayaran_id'=> 'required',
            'anggota_id'=>'required',
            'no_rekening'=>'required',
            'atas_nama' => 'required',
            'keterangan' =>'required',
            'bukti_pembayaran'=>'required',

        ];
    }
    public function messages()
    {
        return [
            'rekening_pembayaran_id.required'=>'Rekening pembayaran tidak boleh kosong',
            'anggota_id.required' =>'Anggota Tidak Boleh Kosong',
            'no_rekening.required' =>'No Rekening tidak Boleh Kosong',
            'atas_nama.required'=>'Atas Nama Tidak boleh kosong',
            'keterangan.required'=>'Keterangan Tidak Boleh Kosong',
            'bukti_pembayaran.required'=>'Bukti Pembayaran tidak boleh Kosong',

        ];
    }
}
