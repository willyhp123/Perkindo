<?php

namespace App\Http\Requests\anggota;

use Illuminate\Foundation\Http\FormRequest;

class r_3_sbu_konstruksiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_rekening'=>'required',
            'keterangan' =>'required',
            'bukti_pembayaran'=>'required',
            'rekening_pembayaran_id'=>'required',
            'atas_nama'=>'required',
        ];
    }
    public function messages()
    {
        return [
          'no_rekening.required'=>'no_rekening Tidak Boleh Kosong',
          'keterangan.required'=>'keterangan Tidak Boleh Kosong',
          'bukti_pembayaran.required'=>'Bukti Pembayaran tidak Boleh Kosong',
          'rekening_pembayaran_id.required'=>'rekening_tujuan tidak Boleh Kosong',
          'atas_nama.required'=>'Atas Nama Tidak boleh kosong'
        ];
    }
}
