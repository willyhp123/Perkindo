<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnggotaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nomor_keanggotaan'=>'required',
            'nama_perusahaan'=>'required',
            'nama_penanggung_jawab'=>'required',
            'alamat_perusahaan'=>'required',
            'provinsi_id'=>'required',
            'kota_kabupanten_id'=>'required',
            'telepon_telex_fax'=>'required',
            'no_hp_1'=>'required',
            'no_hp_2'=>'required',
            'kta_sampai'=>'required',
            'email'=>'required',

        ];
    }
    public function messages()
    {
        return [
           'nomor_keanggotaan.required'=>'Nomor Anggota Tidak Boleh Kosong',
           'nama_perusahaan.required'=>'Nama Perusahaan Tidak Boleh Kosong',
           'nama_penanggung_jawab.required'=>'Nama Penanggung Jawab Tidak Boleh Kosong',
           'alamat_perusahaan.required'=>'Alamat Perusaahaan Tidak Boleh Kosong',
           'provinsi_id.required'=>'Provinsi Tidak Boleh kosong',
           'kota_kabupaten_id.required'=>'Kota Kabupaten Tidak Boleh Kosong',
           'telepon_telex_fax.required'=>'Telepon/Telex/Fax Tidak Boleh Kosong',
           'no_hp_1.required'=>'No Hp 1 Tidak Boleh Kosong',
           'no_hp_2.required'=>'No Hp 2 Tidak Boleh Kosong',
           'kta_sampai.required'=>'Kta Sampai Tidak Boleh Kosong',
           'email.required'=>'Email Tidak Boleh Kosong',
        ];
    }
}
