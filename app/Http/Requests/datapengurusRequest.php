<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class datapengurusRequest extends FormRequest
{
    /**
     * @var mixed
     */
    private $tanggal_lahir;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_ktp'=>'required',
            'nik'=>'required',
            'nama'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'jenis_kelamin'=>'required',
            'alamat'=>'required',
            'no_hp_1'=>'required',
            'no_hp_2'=>'required',
            'email'=>'required',
            'foto'=>'required',
            'password'=>'required',
            'agama_id'=>'required',
            'jabatan_id'=>'required',
        ];
    }
    public function messages()
    {
        return[
            'no_ktp.required'=>'No Ktp Tidak Boleh Kosong',
            'nik.required'=>'Nik tidak Boleh Kosong',
            'nama.required' => 'Nama Tidak Boleh Kosong',
            'tempat_lahir.required'=>'Tempat Lahir Tidak Boleh Kosong',
            'tanggal_lahir.required'=>'Tanggal Lahir Tidak boleh Kosong',
            'jenis_kelamin.required'=>'Jenis Kelamin tidak boleh Kosong',
            'alamat.required'=>'Alamat Tidak Boleh Kosong',
            'no_hp_1.required'=>'No Hp 1 Tidak Boleh Kosong',
            'no_hp_2.required'=>'No Hp 2 Tidak Boleh Kosong',
            'email.required'=>'email Tidak Boleh Kosong',
            'password.required'=> 'password Tidak Boleh kosong',
            'agama_id.required'=>'Agama Tidak Boleh Kosong',
            'jabatan_id.required'=>'Jabatan Tidak Boleh Kosong'
        ];
    }
}
