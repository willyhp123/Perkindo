<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class sbu_non_konstruksiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_seri_formulir'=>'required',
            'anggota_id'=>'required',
            'tanggal_dikeluarkan_sbu'=>'required',
            'pj_operasional'=>'required',
            'file'=>'required',
        ];
    }
    public function messages()
    {
        return[
          'no_seri_formulir.required'=>'No Seri Formulir Tidak Boleh Kosong',
          'anggota_id.required'=>'Anggota Tidak Boleh Kosong' ,
          'tanggal_dikeluarkan_sbu'=>'Tanggal Dikeluarkan SBU tidak Boleh kosong',
          'pj_operasional'=>'Pj_operasional Tidak Boleh Kosong',
          'file' =>'File Tidak Boleh Kosong'
        ];
    }
}
