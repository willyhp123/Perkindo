<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\anggota\p_ktaRequest;
use App\Http\Requests\anggota\r_2_sbu_konstrruksireques;
use App\Http\Requests\anggota\r_3_sbu_konstruksiRequest;
use App\Models\anggota;
use App\Models\registrasi_sbu_konstruksi;
use Illuminate\Http\Request;
use App\Models\pembayaran_kta;
use App\Models\pembayaran_sbu_konstruksi;
use App\Models\pembayaran_sbu_non_konstruksi;
use App\Models\pembayaran_registrasi_sbu_konstruksi;
use App\Models\rekening_pembayarans;
use App\Models\sbu_konstruksi;
use App\Models\sbu_non_konstruksi;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class AnggotaController extends Controller
{
    public function anggota()
    {
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.index', $data);
    }

    public function anggota_kta_sbu_kta()
    {
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.kta_sbu.kta', $data);
    }

    public function anggota_sbu_konstruksi()
    {
        $anggota_sbu_konstruksi = sbu_konstruksi::all();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.kta_sbu.sbu_konstruksi', $data,['anggota_sbu_konstruksi'=> $anggota_sbu_konstruksi]);
    }

    public function anggota_sbu_non_konstruksi()
    {
        $anggota_sbu_non_konstruksi =sbu_non_konstruksi::all();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.kta_sbu.sbu_non_konstruksi', $data,['anggota_sbu_non_konstruksi'=> $anggota_sbu_non_konstruksi]);
    }

    public function p_kta_sbu()
    {
        $rekening = rekening_pembayarans::all();
        $pembayaran_kta = pembayaran_kta::all();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.pembayaran.kta_sbu', $data, ['pembayaran_kta' => $pembayaran_kta
            , 'rekening' => $rekening
        ]);
    }
    public function tambah_p_kta_sbu_kta(p_ktaRequest $request){
        $pembayaran_kta = new pembayaran_kta();
        $pembayaran_kta->no_rekening = $request->no_rekening;
        $pembayaran_kta->rekening_pembayaran_id = $request->rekening_pembayaran_id;
        $pembayaran_kta->keterangan = $request->keterangan;
        $pembayaran_kta->anggota_id = $request->anggota_id;
        $pembayaran_kta->atas_nama = $request->atas_nama;
        $pembayaran_kta->sudah_diproses = "belum diproses";
        if ($request->hasFile('bukti_pembayaran')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('bukti_pembayaran')->move('foto_bukti_bayar_kta/', $request->file('bukti_pembayaran')->getClientOriginalName());
            $pembayaran_kta->bukti_pembayaran = $request->file('bukti_pembayaran')->getClientOriginalName();
            $pembayaran_kta->save();
            return back()->with('success', 'Anda Berhasil Mengirim Data Anda,
                Tunggu admin melakukan proses selanjutnya');
        }
    }
    public function tambah_p_kta_sbu_sbu_konstruksi(p_ktaRequest $request){
        $pembayaran_sbu_konstruksi = new pembayaran_sbu_konstruksi();
        $pembayaran_sbu_konstruksi->no_rekening = $request->no_rekening;
        $pembayaran_sbu_konstruksi->keterangan = $request->keterangan;
        $pembayaran_sbu_konstruksi->rekening_pembayaran_id = $request->rekening_pembayaran_id;
        $pembayaran_sbu_konstruksi->anggota_id = $request->anggota_id;
        $pembayaran_sbu_konstruksi->atas_nama = $request->atas_nama;
        $pembayaran_sbu_konstruksi->sudah_diproses = "belum diproses";
        if ($request->hasFile('bukti_pembayaran')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('bukti_pembayaran')->move('foto_bukti_bayar_sbu_konstruksi/', $request->file('bukti_pembayaran')->getClientOriginalName());
            $pembayaran_sbu_konstruksi->bukti_pembayaran = $request->file('bukti_pembayaran')->getClientOriginalName();
            $pembayaran_sbu_konstruksi->save();
            return back()->with('success', 'Anda Berhasil Mengirim Data Anda,
                Tunggu admin melakukan proses selanjutnya');
        }
    }
    public function tambah_p_kta_sbu_sbu_non_konstruksi(p_ktaRequest $request){
        $pembayaran_sbu_non_konstruksi = new pembayaran_sbu_non_konstruksi();
        $pembayaran_sbu_non_konstruksi->no_rekening = $request->no_rekening;
        $pembayaran_sbu_non_konstruksi->keterangan = $request->keterangan;
        $pembayaran_sbu_non_konstruksi->rekening_pembayaran_id = $request->rekening_pembayaran_id;
        $pembayaran_sbu_non_konstruksi->anggota_id = $request->anggota_id;
        $pembayaran_sbu_non_konstruksi->atas_nama = $request->atas_nama;
        $pembayaran_sbu_non_konstruksi->sudah_diproses = "belum diproses";
        if ($request->hasFile('bukti_pembayaran')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('bukti_pembayaran')->move('foto_bukti_sbu_non_konstruksi/', $request->file('bukti_pembayaran')->getClientOriginalName());
            $pembayaran_sbu_non_konstruksi->bukti_pembayaran = $request->file('bukti_pembayaran')->getClientOriginalName();
            $pembayaran_sbu_non_konstruksi->save();
            return back()->with('success', 'Anda Berhasil Mengirim Data Anda,
                Tunggu admin melakukan proses selanjutnya');
        }
    }
    public function p_registrasi_2_sbu_konstruksi()
    {
        $rekening = rekening_pembayarans::all();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.pembayaran.registrasi_2_sbu_konstruksi', $data, ['rekening' => $rekening]);
    }

    public function p_registrasi_3_sbu_konstruksi()
    {
        $rekening = rekening_pembayarans::all();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.pembayaran.registrasi_3_sbu_konstruksi', $data, ['rekening' => $rekening]);
    }

    public function h_p_kta()
    {
        $h_p_kta = pembayaran_kta::all();
        $rekening = rekening_pembayarans::all();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.history_pembayaran.kta', $data,
            ['h_p_kta' => $h_p_kta, 'rekening' => $rekening]
        );
    }

    public function h_p_sbu_konstruksi()
    {
        $h_p_sbu_konstruksi = pembayaran_sbu_konstruksi::all();
        $rekening = rekening_pembayarans::all();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.history_pembayaran.sbu_konstruksi', $data,
            ['h_p_sbu_konstruksi' => $h_p_sbu_konstruksi, 'rekening' => $rekening]
        );
    }

    public function h_p_sbu_non_konstruksi()
    {
        $h_p_sbu_non_konstruksi = pembayaran_sbu_non_konstruksi::all();
        $rekening = rekening_pembayarans::all();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.history_pembayaran.sbu_non_konstruksi', $data
            , ['h_p_sbu_non_konstruksi' => $h_p_sbu_non_konstruksi, 'rekening' => $rekening]
        );
    }

    public function h_p_registrasi_sbu_konstruksi()
    {
        $h_p_registrasi_sbu_konstruksi = pembayaran_registrasi_sbu_konstruksi::all();
        $rekening = rekening_pembayarans::all();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.history_pembayaran.registrasi_sbu_konstruksi', $data,
            ['h_p_registrasi_sbu_konstruksi' => $h_p_registrasi_sbu_konstruksi, 'rekening' => $rekening]
        );
    }

    public function ubah_user_pengguna()
    {
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.anggota.user_pengguna.index', $data);
    }
    public function update_ubah_user_pengguna(Request $request,$id){
        $user = anggota::find($id);
        if(Hash::check($request->password_lama,$user->password)){
            $user->password = $user->password_baru;
            $user->update();
            return back()->with('success','Anda Berhasil Ubah Passoword Akun Anda');
        }
        else{
            return back()->with('fail','Password lama anda salah');
        }

    }

    public function tambah_r2_registrasi_sbu_konstruksi(r_2_sbu_konstrruksireques $request)
    {
        $registrasi_2_sbu_konstruksi = new pembayaran_registrasi_sbu_konstruksi();
        $registrasi_2_sbu_konstruksi->rekening_pembayaran_id = $request->rekening_pembayaran_id;
        $registrasi_2_sbu_konstruksi->anggota_id = $request->anggota_id;
        $registrasi_2_sbu_konstruksi->no_rekening = $request->no_rekening;
        $registrasi_2_sbu_konstruksi->atas_nama = $request->atas_nama;
        $registrasi_2_sbu_konstruksi->keterangan =$request->keterangan;
        $registrasi_2_sbu_konstruksi->sudah_diproses = "belum diproses";
        $registrasi_2_sbu_konstruksi->registrasi_tahun_ke = "2";
        $registrasi_2_sbu_konstruksi->tanggal_registrasi_tahun_ke = date("d-m-Y");

        if ($request->hasFile('bukti_pembayaran')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('bukti_pembayaran')->move('foto_registrasi_sbu_konstruksi/', $request->file('bukti_pembayaran')->getClientOriginalName());
            $registrasi_2_sbu_konstruksi->bukti_pembayaran = $request->file('bukti_pembayaran')->getClientOriginalName();
            $registrasi_2_sbu_konstruksi->save();
            return back()->with('success', 'Anda Berhasil Mengirim Data Anda,
                Tunggu admin melakukan proses selanjutnya');
        }
    }
    public function tambah_r3_registrasi_sbu_konstruksi(r_3_sbu_konstruksiRequest $request){
        $registrasi_3_sbu_konstruksi = new pembayaran_registrasi_sbu_konstruksi();
        $registrasi_3_sbu_konstruksi->rekening_pembayaran_id = $request->rekening_pembayaran_id;
        $registrasi_3_sbu_konstruksi->anggota_id = $request->anggota_id;
        $registrasi_3_sbu_konstruksi->no_rekening = $request->no_rekening;
        $registrasi_3_sbu_konstruksi->atas_nama = $request->atas_nama;
        $registrasi_3_sbu_konstruksi->keterangan =$request->keterangan;
        $registrasi_3_sbu_konstruksi->sudah_diproses = "belum_diproses";
        $registrasi_3_sbu_konstruksi->registrasi_tahun_ke = "3";
        $registrasi_3_sbu_konstruksi->tanggal_registrasi_tahun_ke = date("d-m-Y");
        if ($request->hasFile('bukti_pembayaran')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('bukti_pembayaran')->move('foto_r_3_sbu_konstruksi/', $request->file('bukti_pembayaran')->getClientOriginalName());
            $registrasi_3_sbu_konstruksi->bukti_pembayaran = $request->file('bukti_pembayaran')->getClientOriginalName();
            $registrasi_3_sbu_konstruksi->save();
            return back()->with('success', 'Anda Berhasil Mengirim Data Anda,
                Tunggu admin melakukan proses selanjutnya');
        }

    }
     

}
