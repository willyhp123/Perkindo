<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\tentang_kami;
use Illuminate\Http\Request;
use App\Models\berita;
use App\Models\agenda;
use App\Models\downloads;
use App\Models\pengurus;
use App\Models\anggota;
use App\Models\provinsi;
use App\Models\kota_kabupaten;
use App\Models\jabatan;
use App\Models\agama;
use App\Models\kategori_downloads;
use App\Models\profils;


class HalamanUtamaControler extends Controller
{
    public function index(){
        $berita = berita::all();
        $data = ['datas' => tentang_kami::first()];
        return view('backend.halaman_utama.index',$data,['berita'=>$berita]);
    }
    public function berita(){
        $berita = berita::all();
        $data = ['datas' => tentang_kami::first()];
        return view('backend.halaman_utama.berita',$data,['berita'=>$berita]);
    }
    public function agenda(){
        $agenda = agenda::all();
        $data = ['datas' => tentang_kami::first()];
        return view('backend.halaman_utama.agenda',$data,['agenda'=>$agenda]);
    }
    public function download(){
        $kategori_download = kategori_downloads::all();
        $download = downloads::all();
        $data = ['datas' => tentang_kami::first()];
        return view('backend.halaman_utama.download',$data,['download'=>$download,
            'kategori_download'=>$kategori_download]);
    }
    public function pengurus(){
        $agama = agama::all();
        $jabatan = jabatan::all();
        $pengurus = pengurus::all();
        $data = ['datas' => tentang_kami::first()];
        return view('backend.halaman_utama.pengurus',$data,['pengurus'=>$pengurus,
            'agama'=>$agama,'jabatan'=>$jabatan
            ]);
    }
    public function keanggotaan(){
        $provinsi = provinsi::all();
        $kota_kabupaten = kota_kabupaten::all();
        $keanggotaan = anggota::where('level','=','anggota')->get();
        $data = ['datas' => tentang_kami::first()];
        return view('backend.halaman_utama.anggota',$data,['keanggotaan'=>$keanggotaan,
            'provinsi'=>$provinsi,'kota_kabupaten'=>$kota_kabupaten
            ]);
    }
    public function dokumen_download(Request $request, $file){
        return response()->download(public_path('dokumen/' . $file));
    }
    public function profil(){
        $data = ['datas' => profils::first()];
        return view('backend.halaman_utama.profil',$data);
    }
   
    public function detail_berita($id){
        $data = ['datas' => tentang_kami::first()];
        $berita = ['data'=> berita::find($id)->first()];
        return view('backend.halaman_utama.detail_berita',$data,$berita);

}

}
