<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\anggota;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\Concerns\Has;

class LoginController extends Controller
{
    public function login(){
        return view('backend.login.index');
    }
    public function proses_login(Request $request){
        request()->validate(
            [
                'email'=>'required',
                'password'=>'required',
            ]
        );
        $user = anggota::where('email','=',$request->email)->first();
        if($user){
            if(Hash::check($request->password,$user->password)){
                $request->session()->put('loginID',$user->id_anggota);
                if($user->level == 'admin'){
                    return redirect('admin');
                }
                elseif($user->level == 'anggota'){
                    return redirect('anggota');
                }
            }
            else{
                return back()->with('fail','Password not matches');
            }
        }else{
            return back()->with('fail','This email is not registered');
        }
    }
    public function register(){
        return view('backend.login.register');
    }
    public function proses_register(Request $request){
        $anggota = new anggota();
        $anggota->create(
            [
                'level' => $request->level,
                'email'=>$request->email,
                'password' =>Hash::make($request->password),

            ]
        )->save();
        return redirect('register');
    }
    public function logout(){
        if (session()->has('loginID')) {
            session()->pull('loginID');
            return redirect('login')->with('success', 'Anda Berhasil Logout');
        }
    }
    public function level(){
          $data =  anggota::where('id_anggota', '=', session('loginID'))->first();
          if($data){
               if($data->level == 'Admin'){
                    return redirect('admin');
                }
                elseif($data->level == 'anggota'){
                    return redirect('anggota');
                }
          }
    }
}
