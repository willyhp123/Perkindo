<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AgamaRequest;
use App\Http\Requests\AgendaRequest;
use App\Http\Requests\AnggotaRequest;
use App\Http\Requests\BeritaController;
use App\Http\Requests\downloadRequest;
use App\Http\Requests\JabatanRequest;
use App\Http\Requests\k_sbu_konstruksiRequest;
use App\Http\Requests\k_sbu_non_konstruksiRequest;
use App\Http\Requests\kategori_downloadRequest;
use App\Http\Requests\kota_kabupatenRequest;
use App\Http\Requests\ProvinsiRequest;
use App\Http\Requests\s_k_sbu_konstruksiRequest;
use App\Http\Requests\s_k_sbu_non_konstruksiRequest;
use App\Http\Requests\Sbu_konstruksiRequest;
use App\Http\Requests\sbu_non_konstruksiRequest;
use App\Http\Requests\slideRequest;
use App\Models\anggota;
use App\Models\kategori_downloads;
use App\Models\kta;
use App\Models\pembayaran_kta;
use App\Models\pembayaran_sbu_konstruksi;
use App\Models\pembayaran_sbu_non_konstruksi;
use App\Models\pengurus;
use App\Models\profils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\agama;
use App\Models\rekening_pembayarans;
use App\Models\jabatan;
use Illuminate\Support\Facades\File;
use App\Models\Slide;
use App\Models\berita;
use App\Models\agenda;
use App\Models\provinsi;
use App\Models\kota_kabupaten;
use App\Models\klasifikasi_sbu_konstruksi;
use App\Models\klasifikasi_sbu_non_konstruksi;
use App\Models\sub_klasifikasi_sbu_konstruksi;
use App\Models\sub_klasifikasi_sbu_non_konstruksi;
use App\Models\detail_sbu_konstruksi;
use App\Models\detail_sbu_non_konstruksi;
use App\Models\sbu_konstruksi;
use App\Models\sbu_non_konstruksi;
use App\Models\downloads;
use App\Models\foto_pengurus;
use App\Models\struktur_organisasi;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use App\Models\tentang_kami;
use App\Models\pembayaran_registrasi_sbu_konstruksi;
use App\Http\Requests\rekening_pembayaranRequest;
use App\Http\Requests\datapengurusRequest;
use Illuminate\Support\Facades\DB;
class adminController extends Controller
{
    public function admin()
    {
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.index', $data);
    }

    public function agama()
    {
        $agama = DB::table('agamas')
                ->select('agama','id_agama')
                ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.pengurus.agama', $data, ['agama' => $agama]);
    }

    public function tambah_agama(AgamaRequest $request)
    {
            $agamaRequest = DB::table('agamas')
                            ->select('agama')
                            ->where('agama','=',$request->agama)
                            ->first();
            if($agamaRequest){
                return back()->with('fail', 'Agama Sudah Di Input sebelumnnya');
            }
            else{
                $agama = new agama();
                $agama->create([
                    'agama'=> $request->agama
                ])->save();
                return back()->with('success', 'Anda Berhasil Tambah Data Agama');
            }
    }

    public function update_agama(Request $request, $id)
    {
        $agama = agama::find($id);
        $agama->agama = $request->agama;
        $agama->update();
        return back()->with('success', 'Anda Telah Berhasil Update data Agama');
    }

    public function delete_agama($id)
    {
        $agama = agama::find($id);
        $agama->delete();
        return back()->with('success', 'Anda Telah Berhasil Delete data Agama');
    }

    public function rekening_pembayaran()
    {
        $rekening_pembayaran = DB::table('rekening_pembayarans')
                                ->select('id_rekening_pembayaran','nama_bank','no_rek','atas_nama')
                                ->get();
        ;
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.pengurus.rekening_pembayaran'
            , $data, ['rekening_pembayaran' => $rekening_pembayaran]);
    }

    public function tambah_rekening_pembayaran(rekening_pembayaranRequest $request)
    {
        $rekening_pembayaranRequest = DB::table('rekening_pembayarans')
                                    ->select('no_rek')
                                    ->where('no_rek','=',$request->no_rek)
                                    ->first();
        if($rekening_pembayaranRequest){
            return back()->with('fail', 'No rekening Sudah Di inputkan Sebelumnya');
        }
        else{
            $rekening_pembayaran = new rekening_pembayarans();
            $rekening_pembayaran->create(
                [
                    'nama_bank'=> $request->nama_bank,
                    'no_rek' => $request->no_rek,
                    'atas_nama'=> $request->atas_nama
                ]
            )->save();
            return back()->with('success', 'Anda Berhasil Input Data Rekening Pembayaran');
        }
    }
    public function update_rekening_pembayaran(Request $request, $id)
    {
        $rekening_pembayaran = rekening_pembayarans::find($id);
        $rekening_pembayaran->update(
            [
                'nama_bank' => $request->nama_bank,
                'no_rek' => $request->no_rek,
                'atas_nama' => $request->atas_nama
            ]
        );
        return back()->with('success', 'Anda Berhasil Update Data Rekening Pembayaran');
    }

    public function delete_rekening_pembayaran($id)
    {
        $rekening_pembayaran = rekening_pembayarans::find($id);
        $rekening_pembayaran->delete();
        return back()->with('success', 'Anda Berhasil Delete Data Rekening Pembayaran');
    }

    //-----------------jabatan---------------------
    public function jabatan()
    {
        $jabatan = DB::table('jabatans')
                  ->select('id_jabatan','jabatan')
                  ->get();
        ;
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.pengurus.jabatan', $data, ['jabatan' => $jabatan]);

    }

    public function tambah_jabatan(JabatanRequest $request)
    {
        $jabatanRequest = jabatan::where('jabatan','=',$request->jabatan)->first();
        if($jabatanRequest){
            return back()->with('fail', 'Jabatan Sudah Di Inputkan Sebelumnnya');
        }
        else{
            $jabatan = new jabatan();
            $jabatan->create($request->all())->save();
            return back()->with('success', 'Anda Berhasil Tambah Data Jabatan');
        }
    }

    public function update_jabatan(Request $request, $id)
    {
        $jabatan = jabatan::find($id);
        $jabatan->update($request->all());
        return back()->with('success', 'Anda Berhasil Update Data Jabatan');
    }

    public function delete_jabatan($id)
    {
        $jabatan = jabatan::find($id);
        $jabatan->delete();
        return back()->with('success', 'Anda Berhasil Delete Data Jabatan');
    }

    //----------------------pengurus------------------
    public function pengurus()
    {
        $pengurus_agama = DB::table('penguruses')
                         ->select('nik','nama','no_ktp','tempat_lahir','tanggal_lahir'
                         ,'jenis_kelamin','alamat','no_hp_1','no_hp_2','agama_id','jabatan_id','email',
                        'foto','id_pengurus')
                        ->get();
        $pengurus_jabatan = pengurus::with('pengurus_jabatan')->get();
        $agama = DB::table('agamas')
                ->select('agama','id_agama')
                ->get();
        $jabatan = DB::table('jabatans')
                 ->select('id_jabatan','jabatan')
                 ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.pengurus.data_pengurus', $data, ['pengurus_agama' => $pengurus_agama,
            'pengurus_jabatan' => $pengurus_jabatan, 'agama' => $agama
            , 'jabatan' => $jabatan
        ]);

    }

    public function tambah_pengurus(datapengurusRequest $request)
    {
        $pengurusRequest = pengurus::where('no_ktp','=',$request->no_ktp)->orwhere('nik','=',$request->nik)->first();
        if($pengurusRequest){
            return back()->with('fail', 'No Ktp atau Nik Sudah di input Sebelumnya');
        }
        $pengurus = new pengurus();
        $pengurus->no_ktp = $request->no_ktp;
        $pengurus->nik = $request->nik;
        $pengurus->nama = $request->nama;
        $pengurus->tempat_lahir = $request->tempat_lahir;
        $pengurus->tanggal_lahir = $request->tanggal_lahir;
        $pengurus->jenis_kelamin = $request->jenis_kelamin;
        $pengurus->alamat = $request->alamat;
        $pengurus->agama_id = $request->agama_id;
        $pengurus->no_hp_1 = $request->no_hp_1;
        $pengurus->no_hp_2 = $request->no_hp_2;
        $pengurus->jabatan_id = $request->jabatan_id;
        $pengurus->email = $request->email;
        $pengurus->password = $request->password;
        if ($request->hasFile('file')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('file')->move('foto_pengurus/', $request->file('file')->getClientOriginalName());
            $pengurus->foto = $request->file('file')->getClientOriginalName();
          
        }
        $pengurus->save();
        return back()->with('success', 'Anda Berhasil Input Data Pengurus');
    }

    public function update_pengurus(Request $request, $id)
    {
        $pengurus = pengurus::find($id);
        $pengurus->no_ktp = $request->no_ktp;
        $pengurus->nik = $request->nik;
        $pengurus->nama = $request->nama;
        $pengurus->tempat_lahir = $request->tempat_lahir;
        $pengurus->tanggal_lahir = $request->tanggal_lahir;
        $pengurus->jenis_kelamin = $request->jenis_kelamin;
        $pengurus->alamat = $request->alamat;
        $pengurus->agama_id = $request->agama_id;
        $pengurus->no_hp_1 = $request->no_hp_1;
        $pengurus->no_hp_2 = $request->no_hp_2;
        $pengurus->jabatan_id = $request->jabatan_id;
        $pengurus->email = $request->email;
        $pengurus->password = $request->password;

        if ($request->hasFile('file')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $pengurus->foto;
            $file_path = public_path('foto_pengurus/' . $file_name);
            File::delete($file_path);
            $request->file('file')->move('foto_pengurus/', $request->file('file')->getClientOriginalName());
            $pengurus->foto = $request->file('file')->getClientOriginalName();
            $pengurus->save();
            return back()->with('success', 'Anda Berhasil Update  Data Pengurus');
        }
    }

    public function delete_pengurus($id)
    {
        $pengurus = pengurus::find($id);
        $pengurus->delete();
        $file_name = $pengurus->foto;
        $file_path = public_path('foto_pengurus/' . $file_name);
        File::delete($file_path);
        return back()->with('success', 'Anda Berhasil Update Data Pengurus');
    }

    public function slide()
    {
        $slide = DB::table('slides')
                ->select('gambar_slide','judul','url','aktif')
                ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.setting.slide', $data, ['slide' => $slide]);
    }

    public function tambah_slide(slideRequest $request)
    {
        $slide = new slide();
        $slide->judul = $request->judul;
        $slide->url = $request->url;
        $slide->aktif = "tidak aktif";
        if ($request->hasFile('gambar_slide')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('gambar_slide')->move('gambar_slide/', $request->file('gambar_slide')->getClientOriginalName());
            $slide->gambar_slide = $request->file('gambar_slide')->getClientOriginalName();
            $slide->save();
            return back()->with('success', 'Anda Berhasil Input Data Slide');
        }
    }

    public function update_slide(Request $request, $id)
    {
        $slide = slide::find($id);
        $slide->judul = $request->judul;
        $slide->url = $request->url;
        if ($request->hasFile('gambar_slide')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $slide->gambar_slide;
            $file_path = public_path('gambar_slide/' . $file_name);
            File::delete($file_path);
            $request->file('gambar_slide')->move('gambar_slide/', $request->file('gambar_slide')->getClientOriginalName());
            $slide->gambar_slide = $request->file('gambar_slide')->getClientOriginalName();
            $slide->save();
            return back()->with('success', 'Anda Berhasil Update Data Slide');
        }
    }

    public function delete_slide($id)
    {
        $slide = slide::find($id);
        $slide->delete();
        $file_name = $slide->gambar_slide;
        $file_path = public_path('gambar_slide/' . $file_name);
        File::delete($file_path);
        return back()->with('success', 'Anda Berhasil Delete Data Slide');
    }

    public function berita()
    {
        $berita = DB::table('beritas')
                ->select('judul','isi','url','foto','id_berita')
                ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.setting.berita', $data, ['berita' => $berita]);
    }

    public function tambah_berita(BeritaController $request)
    {
        $berita = new berita();
        $berita->judul = $request->judul;
        $berita->isi = $request->isi;
        $berita->url = $request->url;
        if ($request->hasFile('foto_berita')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('foto_berita')->move('foto_berita/', $request->file('foto_berita')->getClientOriginalName());
            $berita->foto = $request->file('foto_berita')->getClientOriginalName();
            $berita->save();
            return back()->with('success', 'Anda Berhasil Input Data Berita');
        }

    }

    public function update_berita(Request $request, $id)
    {
        $berita = berita::find($id);
        $berita->judul = $request->judul;
        $berita->isi = $request->isi;
        $berita->url = $request->url;
        if ($request->hasFile('foto_berita')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $berita->gambar_slide;
            $file_path = public_path('foto_berita/' . $file_name);
            File::delete($file_path);
            $request->file('foto_berita')->move('foto_berita/', $request->file('foto_berita')->getClientOriginalName());
            $berita->foto = $request->file('foto_berita')->getClientOriginalName();
            $berita->save();
            return back()->with('success', 'Anda Berhasil Update Data Berita');
        }
    }

    public function delete_berita($id)
    {
        $berita = berita::find($id);
        $berita->delete();
        $file_name = $berita->gambar_slide;
        $file_path = public_path('foto_berita/' . $file_name);
        File::delete($file_path);
        return back()->with('success', 'Anda Berhasil Delete Data Berita');
    }

    public function agenda()
    {
        $agenda = DB::table('agendas')
                ->select('tanggal_mulai','tanggal_selesai','nama_agenda')
                ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.setting.agenda', $data, ['agenda' => $agenda]);
    }

    public function tambah_agenda(AgendaRequest $request)
    {
        $agendaRequest = agenda::where('nama_agenda','=',$request->nama_agenda)->first();
        if($agendaRequest){
            return back()->with('fail', $request->nama_agenda.'sudah Di Input');
        }
        else{
            $agenda = new agenda();
            $agenda->create($request->all())->save();
            return back()->with('success', 'Anda Berhasil Tambah Data Agenda');
        }
    }

    public function update_agenda(Request $request, $id)
    {
        $agenda = agenda::find($id);
        $agenda->update($request->all());
        return back()->with('success', 'Anda Berhasil Update Data Agenda');
    }

    public function delete_agenda($id)
    {
        $agenda = agenda::find($id);
        $agenda->delete();
        return back()->with('success', 'Anda Berhasil Delete Data Agenda');
    }

    public function kategori_download()
    {

        $kategori_download = DB::table('kategori_downloads')
                            ->select('id_kategori_download','kategori_download')
                            ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.setting.kategori_download', $data, ['kategori_download' => $kategori_download]);
    }

    public function tambah_kategori_download(kategori_downloadRequest $request)
    {
        $kategori_downloadRequest = kategori_downloads::where('kategori_download','=',$request->kategori_download)->first();
        if($kategori_downloadRequest){
            return back()->with('fail', $request->kategori_download.'Sudah Di Input');
        }else{
            $kategori = new kategori_downloads();
            $kategori->kategori_download = $request->kategori_download;
            $kategori->save();
            return back()->with('success', 'Anda Berhasil Tambah Data Kategori Download');
        }
    }
    public function update_kategori_download(Request $request, $id)
    {
        $kategori = kategori_downloads::find($id);
        $kategori->kategori_download = $request->kategori_download;
        $kategori->update();
        return back()->with('success', 'Anda Berhasil Update Data Kategori Download');
    }

    public function delete_kategori_download($id)
    {
        $kategori = kategori_downloads::find($id);
        $kategori->delete();
        return back()->with('success', 'Anda Berhasil Delete Data Kategori Download');
    }

    public function provinsi()
    {
        $provinsi = DB::table('provinsis')
                    ->select('id_provinsi','provinsi')
                    ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.master.provinsi', $data, ['provinsi' => $provinsi]);
    }

    public function tambah_provinsi(ProvinsiRequest $request)
    {
        $provinsiRequest = provinsi::where('provinsi','=',$request->provinsi)->first();
        if($provinsiRequest){
            return back()->with('fail', $request->provinsi.'Sudah Di input');
        }
        $provinsi = new provinsi();
        $provinsi->provinsi = $request->provinsi;
        $provinsi->save();
        return back()->with('success', 'Anda Berhasil Tambah Data Provinsi');
    }

    public function update_provinsi(Request $request, $id)
    {
        $provinsi = provinsi::find($id);
        $provinsi->provinsi = $request->provinsi;
        $provinsi->update();
        return back()->with('success', 'Anda Berhasil Update data Provinsi');
    }

    public function delete_provinsi($id)
    {
        $provinsi = provinsi::find($id);
        $provinsi->delete();
        return back()->with('success', 'Anda Berhasil Delete data Provinsi');
    }

    public function kota_kabupaten()
    {
        $kota_kabupaten = DB::table('kota_kabupatens')
                        ->select('id_kota_kabupaten','provinsi_id','kota_kabupaten')
                        ->get();
        $provinsi = DB::table('provinsis')->select('id_provinsi','provinsi')->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.master.kota_kabupaten', $data, ['kota_kabupaten' => $kota_kabupaten,
            'provinsi' => $provinsi
        ]);
    }

    public function tambah_kota_kabupaten(kota_kabupatenRequest $request)
    {
        $kota_kabupatenRequest= kota_kabupaten::where('kota_kabupaten','=',$request->kota_kabupaten)->first();
        if($kota_kabupatenRequest){
            return back()->with('fail', $request->kota_kabupaten.'Sudah Di Input');
        }
        else{
            $kota_kabupatens = new kota_kabupaten();
            $kota_kabupatens->create([
                'provinsi_id' => $request->provinsi_id,
                'kota_kabupaten' => $request->kota_kabupaten
            ])->save();
            return back()->with('success', 'Anda Berhasil Tambah data Kota Kabupaten');
        }
    }

    public function update_kota_kabupaten(Request $request, $id)
    {
        $kota_kabupaten = kota_kabupaten::find($id);
        $kota_kabupaten->update($request->all());
        return back()->with('success', 'Anda Berhasil Update data Kota Kabupaten');
    }

    public function delete_kota_kabupaten($id)
    {
        $kota_kabupaten = kota_kabupaten::find($id);
        $kota_kabupaten->delete();
        return back()->with('success', 'Anda Berhasil Delete Data Kota Kabupaten');
    }

    public function klasifikasi_sbu_konstruksi()
    {
        $klasifikasi = DB::table('klasifikasi_sbu_konstruksis')
                    ->select('id_klasifikasi_sbu_konstruksi','klasifikasi')
                    ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.klasifikasi.k_sbu_konstruksi', $data
            , ['klasifikasi' => $klasifikasi]);
    }

    public function tambah_klasifikasi_sbu_konstruksi(k_sbu_konstruksiRequest $request)
    {
        $klasifikasiRequest= klasifikasi_sbu_konstruksi::where('klasikasi','=',$request->klasifikasi)->first();
        if($klasifikasiRequest){
            return back()->with('fail', $request->klasifikasi.'Sudah Di Input');
        }
        else{
            $klasifikasi = new klasifikasi_sbu_konstruksi();
            $klasifikasi->klasifikasi = $request->klasifikasi;
            $klasifikasi->save();
            return back()->with('success', 'Anda Berhasil Tambah Data Klasifikasi sbu Konstruksi');
        }

    }

    public function update_klasifikasi_sbu_konstruksi(Request $request, $id)
    {
        $klasifikasi = klasifikasi_sbu_konstruksi::find($id);
        $klasifikasi->klasifikasi = $request->klasifikasi;
        $klasifikasi->update();
        return back()->with('success', 'Anda Berhasil Update Data Klasifikasi sbu Konstruksi');

    }

    public function delete_klasifikasi_sbu_konstruksi($id)
    {
        $klasifikasi = klasifikasi_sbu_konstruksi::find($id);
        $klasifikasi->delete();
        return back()->with('success', 'Anda Berhasil Delete Data Klasifikasi sbu Konstruksi');
    }

    public function klasifikasi_sbu_non_konstruksi()
    {
        $klasifikasi = DB::table('klasifikasi_sbu_non_konstruksis')
                     ->select('id_k_sbu_non_konstruksi','klasifikasi_non_konstruksi')
                     ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.klasifikasi.k_sbu_non_konstruksi', $data,
            ['klasifikasi' => $klasifikasi]
        );
    }

    public function tambah_klasifikasi_sbu_non_konstruksi(k_sbu_non_konstruksiRequest $request)
    {
        $sbu_non_konstruksiRequest = sbu_non_konstruksi::where('no_seri_formulir','=',$request->no_seri_formulir);
        if($sbu_non_konstruksiRequest){
            return back()->with('fail', $request->no_seri_formulir.'Sudah Di Input');
        }
        else{
            $klasifikasi = new klasifikasi_sbu_non_konstruksi();
            $klasifikasi->create($request->all())->save();
            return back()->with('success', 'Anda Telah Berhasil Tambah data Klasifikasi sbu Non Konstruksi');
        }
    }

    public function update_klasifikasi_sbu_non_konstruksi(Request $request, $id)
    {
        $klasifikasi = klasifikasi_sbu_non_konstruksi::find($id);
        $klasifikasi->klasifikasi_non_konstruksi = $request->klasifikasi;
        $klasifikasi->update();
        return back()->with('success', 'Anda Telah Berhasil Update data Klasifikasi sbu Non Konstruksi');
    }

    public function delete_klasifikasi_sbu_non_konstruksi($id)
    {
        $klasifikasi = klasifikasi_sbu_non_konstruksi::find($id);
        $klasifikasi->delete();
        return back()->with('success', 'Anda Telah Berhasil Delete data Klasifikasi sbu Non Konstruksi');

    }

    public function sub_klasifikasi_sbu_konstruksi()
    {
        $sub_klasifikasi = DB::table('sub_klasifikasi_sbu_konstruksis')
                          ->select('id_sub_klasifikasi_sbu_konstruksi','k_sbu_konstruksi_id'
                          ,'kode','sub_klasifikasi','lingkup_pekerjaan','keterangan')
                          ->get();
        $klasifikasi = DB::table('klasifikasi_sbu_konstruksis')
                     ->select('id_klasifikasi_sbu_konstruksi','klasifikasi')
                     ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.Klasifikasi.s_k_sbu_konstruksi', $data,
            ['klasifikasi' => $klasifikasi, 'sub_klasifikasi' => $sub_klasifikasi]
        );
    }

    public function tambah_sub_klasifikasi_sbu_konstruksi(s_k_sbu_konstruksiRequest $request)
    {
        $klasifikasiRequest = sub_klasifikasi_sbu_konstruksi::where('sub_klasifikasi','=',$request->sub_klasifikasi)->first();
        if($klasifikasiRequest){
            return back()->with('fail', $request->sub_klasifikasi.'Sudah Di Input');
        }
        else{
            $klasifikasi = new sub_klasifikasi_sbu_konstruksi();
            $klasifikasi->create([
                'k_sbu_konstruksi_id' => $request->k_sbu_konstruksi_id,
                'kode' => $request->kode,
                'sub_klasifikasi' => $request->sub_klasifikasi,
                'lingkup_pekerjaan' => $request->lingkup_pekerjaan,
                'keterangan' => $request->keterangan
            ])->save();
            return back()->with('success', 'Anda Telah Berhasil Tambah Data Klasifikasi sbu Non Konstruksi');
        }
    }

    public function update_sub_klasifikasi_sbu_konstruksi(Request $request, $id)
    {
        $klasifikasi = sub_klasifikasi_sbu_konstruksi::find($id);
        $klasifikasi->update($request->all());
        return back()->with('success', 'Anda Telah Berhasil Update data Klasifikasi sbu Non Konstruksi');
    }

    public function delete_sub_klasifikasi_sbu_konstruksi($id)
    {
        $klasifikasi = sub_klasifikasi_sbu_konstruksi::find($id);
        $klasifikasi->delete();
        return back()->with('success', 'Anda Telah Berhasil Delete data Klasifikasi sbu Non Konstruksi');
    }

    public function sub_klasifikasi_sbu_non_konstruksi()
    {
        $sub_non_klasifikasi = DB::table('sub_klasifikasi_sbu_non_konstruksi')
                               ->select('id_sbu_k_sbu_non_k','k_sbu_non_konstruksi_id','kode',
                              'sub_klasifikasi','lingkup_pekerjaan','keterangan')
                               ->get();
        $klasifikasi = DB::table('klasifikasi_sbu_non_konstruksis')
                    ->select('id_k_sbu_konstruksi','klasifikasi_non_konstruksi')
                    ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.klasifikasi.s_k_sbu_non_konstruksi', $data,
            ['klasifikasi' => $klasifikasi, 'sub_non_klasifikasi' => $sub_non_klasifikasi]
        );
    }

    public function tambah_sub_klasifikasi_sbu_non_konstruksi(s_k_sbu_non_konstruksiRequest $request)
    {
        $klasifikasiRequest = sub_klasifikasi_sbu_non_konstruksi::where('sub_klasifikasi','=',$request->sub_non_klasifikasi)->first();
        if($klasifikasiRequest){
            return back()->with('fail', $request->sub_non_klasifikasi.'Sudah Diinput');
        }
        else{
            $klasifikasi = new sub_klasifikasi_sbu_non_konstruksi();
            $klasifikasi->create([
                'k_sbu_non_konstruksi_id' => $request->k_sbu_non_klasifikasi_id,
                'kode' => $request->kode,
                'sub_klasifikasi' => $request->sub_klasifikasi,
                'lingkup_pekerjaan' => $request->lingkup_pekerjaan,
                'keterangan' => $request->keterangan
            ])->save();
            return back()->with('success', 'Anda Berhasil Tambah Data sub Klasifikasi sbu Non Konstruksi');
        }

    }

    public function update_sub_klasifikasi_sbu_non_konstruksi(Request $request, $id)
    {
        $klasifikasi = sub_klasifikasi_sbu_non_konstruksi::find($id);
        $klasifikasi->update($request->all());
        return back()->with('success', 'Anda Telah Berhasil Update data sub Klasifikasi sbu non Konstruksi');
    }

    public function delete_sub_klasifikasi_sbu_non_konstruksi($id)
    {
        $klasifikasi = sub_klasifikasi_sbu_non_konstruksi::find($id);
        $klasifikasi->delete();
        return back()->with('success', 'Anda telah Delete Data Sub klasifikasi sbu non konstruksi');
    }

    public function data_perusahaan()
    {
        $anggota = DB::table('anggotas')
                 ->select('id_anggota','nomor_keanggotaan','nama_perusahaan'
                 ,'nama_penanggung_jawab','alamat_perusahaan','provinsi_id',
                 'kota_kabupaten_id','telepon_telex_fax','no_hp_1','no_hp_2','kta_sampai',
                 'foto_kta','password','email','foto_penanggung_jawab','level')
                 ->where('level','=','anggota')
                 ->get();
        $provinsi = DB::table('provinsis')
                   ->select('id_provinsi','provinsi')
                   ->get();
        $kota_kabupaten = DB::table('kota_kabupatens')
                        ->select('id_kota_kabupaten','provinsi_id','kota_kabupaten')
                        ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.perusahaan.data_perusahaan', $data, ['anggota' => $anggota
            , 'provinsi' => $provinsi, 'kota_kabupaten' => $kota_kabupaten
        ]);
    }

    public function tambah_data_perusahaan(AnggotaRequest $request)
    {
        $anggotaRequest = anggota::where('nomor_keanggotaan','=',$request->nomor_keanggotaan)->first();
        if($anggotaRequest){
            return back()->with('fail', $request->nomor_keanggotaaan.'Sudah Digunakan');
        }
        else{
            $anggota = new anggota();
            $anggota->nomor_keanggotaan = $request->nomor_keanggotaan;
            $anggota->nama_perusahaan = $request->nama_perusahaan;
            $anggota->nama_penanggung_jawab = $request->nama_penanggung_jawab;
            $anggota->alamat_perusahaan = $request->alamat_perusahaan;
            $anggota->provinsi_id = $request->provinsi_id;
            $anggota->kota_kabupaten_id = $request->kota_kabupaten_id;
            $anggota->telepon_telex_fax = $request->telepon_telex_fax;
            $anggota->no_hp_1 = $request->no_hp_1;
            $anggota->no_hp_2 = $request->no_hp_2;
            $anggota->kta_sampai = $request->kta_sampai;
            $anggota->email = $request->email;
            $anggota->level = "anggota";
            $anggota->password = Hash::make($request->nomor_keanggotaan);
            if ($request->hasFile('foto_penanggung_jawab')) //Cek apakah ada file avatar yang di Upload
            {
                $request->file('foto_penanggung_jawab')->move('foto_perusahaan/foto_penanggung_jawab/', $request->file('foto_penanggung_jawab')->getClientOriginalName());
                $anggota->foto_penanggung_jawab = $request->file('foto_penanggung_jawab')->getClientOriginalName();
            }
            if ($request->hasFile('foto_kta')) //Cek apakah ada file avatar yang di Upload
            {
                $request->file('foto_kta')->move('foto_perusahaan/foto_kta', $request->file('foto_kta')->getClientOriginalName());
                $anggota->foto_kta = $request->file('foto_kta')->getClientOriginalName();
                $anggota->save();
                return back()->with('success', 'Anda Berhasil Input Data Perusahaan');
            }
        }

    }

    public function update_data_perusahaan(Request $request, $id)
    {
        $anggota = anggota::find($id);
        $anggota->nomor_keanggotaan = $request->nomor_keanggotaan;
        $anggota->nama_perusahaan = $request->nama_perusahaan;
        $anggota->nama_penanggung_jawab = $request->nama_penanggung_jawab;
        $anggota->alamat_perusahaan = $request->alamat_perusahaan;
        $anggota->provinsi_id = $request->provinsi_id;
        $anggota->kota_kabupaten_id = $request->kota_kabupaten_id;
        $anggota->telepon_telex_fax = $request->telepon_telex_fax;
        $anggota->no_hp_1 = $request->no_hp_1;
        $anggota->no_hp_2 = $request->no_hp_2;
        $anggota->kta_sampai = $request->kta_sampai;
        $anggota->email = $request->email;
        $anggota->update();
        if ($request->hasFile('foto_penanggung_jawab')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $anggota->foto_penanggung_jawab;
            $file_path = public_path('foto_perusahaan/foto_penanggung_jawab/' . $file_name);
            File::delete($file_path);
            $request->file('foto_penanggung_jawab')->move('foto_perusahaan/foto_penanggung_jawab/',
                $request->file('foto_penanggung_jawab')->getClientOriginalName());
            $anggota->foto_penanggung_jawab = $request->file('foto_penanggung_jawab')->getClientOriginalName();

        }
        if ($request->hasFile('foto_kta')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $anggota->foto_kta;
            $file_path = public_path('foto_perusahaan/foto_kta/' . $file_name);
            File::delete($file_path);
            $request->file('foto_kta')->move('foto_perusahaan/foto_kta/', $request->file('foto_kta')->getClientOriginalName());
            $anggota->foto_kta = $request->file('foto_kta')->getClientOriginalName();
            $anggota->save();
            return back()->with('success', 'Anda Berhasil Input Data perusahaan');
        }

    }

    public function delete_data_perusahaan($id)
    {
        $anggota = anggota::find($id);
        $file_name_penanggung_jawab = $anggota->foto_penanggung_jawab;
        $file_path_penanggung_jawab = public_path('foto_perusahaan/foto_penanggung_jawab/' . $file_name_penanggung_jawab);
        File::delete($file_path_penanggung_jawab);
        $file_name_kta = $anggota->foto_kta;
        $file_path_kta = public_path('foto_perusahaan/foto_kta/' . $file_name_kta);
        File::delete($file_path_kta);
        $anggota->delete();
        return back()->with('success', 'Anda Berhasil Delete Data Perusahaan');
    }

    public function sbu_konstruksi()
    {
        $sub_konstruksi = sbu_konstruksi::with('detail_sbu_konstruksi')->get();
        $anggota = DB::table('anggotas')
                 ->select('id_anggota','nomor_keanggotaan','nama_perusahaan',
                 'nama_penanggung_jawab','alamat_perusahaan','provinsi_id','kota_kabupaten_id',
                 'telepon_telex_fax','no_hp_1','no_hp_2','kta_sampai','foto_kta','password','email'
                 ,'foto_penanggung_jawab','level')->get();
        $provinsi = DB::table('provinsis')
                   ->select('id_provinsi','provinsi')
                   ->get();
        $detail_sbu_konstruksi = DB::table('detail_sbu_konstruksis')
                                ->select('id_detail_sbu_konstruksi','sub_klasifikasi_sbu_konstruksi_id');
        $klasifikasi_sbu_konstruksi = DB::table('klasifikasi_sbu_konstruksis')
                                    ->select('id_detail_sbu_non_konstruksi','sbu_non_konstruksi_id',
                                'klasifikasi_sbu_non_konstruksi_id',' 	s_k_sbu_non_konstruksi_id ')
                                ->get();
        $sub_klasifikasi_sbu_konstruksi = DB::table('sub_klasifikasi_sbu_konstruksis')
                                        ->select('id_sub_klasifikasi_sbu_konstruksi','k_sbu_konstruksi_id',
                                        'kode','sub_klasifikasi','sub_klasifikasi','lingkup_pekerjaan','keterangan')
                                        ->get();
        $kota_kabupaten = DB::table('kota_kabupaten')
                         ->select('id_kota_kabupaten','provinsi_id','kota_kabupaten')
                         ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.perusahaan.sbu_konstruksi', $data,
            ['sub_konstruksi' => $sub_konstruksi, 'anggota' => $anggota
                , 'provinsi' => $provinsi, 'kota_kabupaten' => $kota_kabupaten,
                'klasifikasi_sbu_konstruksi' => $klasifikasi_sbu_konstruksi,
                'sub_klasifikasi_sbu_konstruksi' => $sub_klasifikasi_sbu_konstruksi,
                'detail_sbu_konstruksi' => $detail_sbu_konstruksi
            ]
        );
    }

    public function tambah_sbu_konstruksi(Sbu_konstruksiRequest $request)
    {
        $sbu_konstruksiRequest = sbu_konstruksi::where('no_seri_formulir','=',$request->no_seri_formulir)->first();
        if($sbu_konstruksiRequest){
            return back()->with('fail', 'no Seri Formulir Tidak Boleh Sama');
        }
        else{
            $sub_konstruksi = new sbu_konstruksi();
            $sub_konstruksi->no_seri_formulir = $request->no_seri_formulir;
            $sub_konstruksi->anggota_id = $request->anggota_id;
            $sub_konstruksi->tanggal_masuk = $request->tanggal_masuk;
            $sub_konstruksi->berlaku_sampai = $request->berlaku_sampai;
            $sub_konstruksi->registrasi_tahun_ke_2 = $request->registrasi_tahun_ke_2;
            $sub_konstruksi->registrasi_tahun_ke_3 = $request->registrasi_tahun_ke_3;
            $sub_konstruksi->tenaga_ahli = $request->tenaga_ahli;
            if ($request->hasFile('foto')) //Cek apakah ada file avatar yang di Upload
            {
                $request->file('foto')->move('foto_sbu_konstruksi/', $request->file('foto')->getClientOriginalName());
                $sub_konstruksi->foto = $request->file('foto')->getClientOriginalName();
                $sub_konstruksi->save();
                return back()->with('success', 'Anda Berhasil Tambah Data Sbu Konstruksi');
            }
        }


    }

    public function update_sbu_konstruksi(Request $request, $id)
    {
        $sub_konstruksi = sbu_konstruksi::find($id);
        $sub_konstruksi->no_seri_formulir = $request->no_seri_formulir;
        $sub_konstruksi->anggota_id = $request->anggota_id;
        $sub_konstruksi->tanggal_masuk = $request->tanggal_masuk;
        $sub_konstruksi->berlaku_sampai = $request->berlaku_sampai;
        $sub_konstruksi->registrasi_tahun_ke_2 = $request->registrasi_tahun_ke_2;
        $sub_konstruksi->registrasi_tahun_ke_3 = $request->registrasi_tahun_ke_3;
        $sub_konstruksi->tenaga_ahli = $request->tenaga_ahli;
        if ($request->hasFile('foto')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $sub_konstruksi->foto;
            $file_path = public_path('foto_sbu_konstruksi/' . $file_name);
            File::delete($file_path);
            $request->file('foto')->move('foto_sbu_konstruksi/', $request->file('foto')->getClientOriginalName());
            $sub_konstruksi->foto = $request->file('foto')->getClientOriginalName();
            $sub_konstruksi->save();
            return back()->with('success', 'Anda Berhasil Tambah Data Sbu Konstruksi');
        }
        $sub_konstruksi->update();
        return back()->with('success', 'Anda Berhasil Update Data Sbu Konstruksi');
    }

    public function delete_sbu_konstruksi($id)
    {
        $sub_konstruksi = sbu_konstruksi::find($id);
        $file_name = $sub_konstruksi->foto;
        $file_path = public_path('foto_sbu_konstruksi/' . $file_name);
        File::delete($file_path);
        $sub_konstruksi->delete();
        return back()->with('success', 'Anda Berhasil Delete Data Sbu Konstruksi');

    }

    public function sbu_non_konstruksi()
    {
        $sbu_non_konstruksi = sbu_non_konstruksi::with('detail_sbu_non_konstruksi')->get();
        $anggota = DB::table('anggotas')
                  ->select('id_anggota','nomor_keanggotaan','nama_perusahaan',
                  'nama_perusahaan','nama_penanggung_jawab','alamat_perusahaan','provinsi_id','kota_kabupaten_id',
                'telepon_telex_fax','no_hp_1','no_hp_2','kta_sampai','foto_kta','password',
                'email','foto_penanggung_jawab');
        $provinsi = DB::table('provinsis')
                   ->select('id_provinsi','provinsi')
                   ->get();
        $sub_klasifikasi_sbu_non_konstruksi = DB::table('sub_klasifikasi_sbu_non_konstruksis')
                                            ->select('id_sub_k_sbu_non_k','k_sbu_non_konstruksi_id','kode'
                                            ,'sub_klasifikasi','lingkup_pekerjaan','keterangan')
                                            ->get();
        $klasifikasi_non_konstruksi = DB::table('id_k_sbu_non_konstruksis')
                                    ->select('id_k_sbu_non_konstruksi','klasifikasi_non_konstruksi')
                                    ->get();
        $kota_kabupaten = DB::table('kota_kabupatens')
                        ->select('id_kota_kabupaten','kota_kabupaten')
                        ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.perusahaan.sbu_non_konstruksi', $data,
            ['sbu_non_konstruksi' => $sbu_non_konstruksi, 'anggota' => $anggota,
                'provinsi' => $provinsi, 'kota_kabupaten' => $kota_kabupaten,
                'sub_klasifikasi_sbu_non_konstruksi' => $sub_klasifikasi_sbu_non_konstruksi,
                'klasifikasi_non_konstruksi' => $klasifikasi_non_konstruksi
            ]
        );
    }

    public function tambah_sbu_non_konstruksi(sbu_non_konstruksiRequest $request)
    {
        $sbu_non_konstruksiRequest = sbu_non_konstruksi::where('no_seri_formulir','=',$request->no_seri_formulir)->first();
        if($sbu_non_konstruksiRequest){
            return back()->with('fail', $request->no_seri_formulir.'Sudah Diinput');
        }
        $sbu_non_konstruksi = new sbu_non_konstruksi();
        $sbu_non_konstruksi->no_seri_formulir = $request->no_seri_formulir;
        $sbu_non_konstruksi->anggota_id = $request->anggota_id;
        $sbu_non_konstruksi->tanggal_dikeluarkan_sbu = $request->tanggal_dikeluarkan_sbu;
        $sbu_non_konstruksi->pj_operasional = $request->pj_operasional;
        if ($request->hasFile('file')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('file')->move('foto_sbu_non_konstruksi/', $request->file('file')->getClientOriginalName());
            $sbu_non_konstruksi->file = $request->file('file')->getClientOriginalName();
            $sbu_non_konstruksi->save();
            return back()->with('success', 'Anda Berhasil Tambah Data Sbu Konstruksi');
        }
    }

    public function update_sbu_non_konstruksi(Request $request, $id)
    {
        $sbu_non_konstruksi = sbu_non_konstruksi::find($id);
        $sbu_non_konstruksi->no_seri_formulir = $request->no_seri_formulir;
        $sbu_non_konstruksi->tanggal_dikeluarkan_sbu = $request->tanggal_dikeluarkan_sbu;
        $sbu_non_konstruksi->pj_operasional = $request->pj_operasional;
        if ($request->hasFile('file')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $sbu_non_konstruksi->file;
            $file_path = public_path('foto_sbu_non_konstruksi/' . $file_name);
            File::delete($file_path);
            $request->file('file')->move('foto_sbu_non_konstruksi/', $request->file('file')->getClientOriginalName());
            $sbu_non_konstruksi->file = $request->file('file')->getClientOriginalName();
            $sbu_non_konstruksi->save();
            return back()->with('success', 'Anda Berhasil Update Data Sbu Konstruksi');
        }

    }

    public function delete_sbu_non_konstruksi($id)
    {
        $sbu_non_konstruksi = sbu_non_konstruksi::find($id);
        $file_name = $sbu_non_konstruksi->file;
        $file_path = public_path('foto_sbu_non_konstruksi/' . $file_name);
        File::delete($file_path);
        $sbu_non_konstruksi->delete();
        return back()->with('success', 'Anda Berhasil Delete Data Sbu Konstruksi');
    }

    public function pembayaran_kta()
    {
        $pembayaran_kta = DB::table('pembayaran_ktas')
                        ->select('id_pembayaran_kta','rekening_pembayaran_id','anggota_id'
                        ,'no_rekening','atas_nama','keterangan','bukti_pembayaran','sudah_diproses')
                        ->where('sudah_diproses','=','belum_diproses')
                        ->get();
        $rekening= DB::table('rekening_pembayarans')
                 ->select('nama_bank','no_rek','atas_nama','id_rekening_pembayaran')
                 ->get();
        $provinsi = DB::table('provinsis')
                  ->select('id_provinsi','provinsi')
                  ->get();
        $kota_kabupaten = DB::table('kota_kabupatens')
                        ->select('id_kota_kabupaten','provinsi_id','kota_kabupaten')
                        ->get();
        $anggota = DB::table('anggotas')
                 ->select('id_anggota','nomor_keanggotaan','nama_perusahaan',
                 'nama_penanggung_jawab','alamat_perusahaan','provinsi_id','kota_kabupaten_id'
                ,'telepon_telex_fax','no_hp_1','no_hp_2','kta_sampai','foto_kta','email','foto_penanggung_jawab')
                ->where('level','=','anggota')
                ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.pembayaran.kta', $data,
            ['anggota' => $anggota,'pembayaran_kta'=>$pembayaran_kta,'rekening'=>$rekening
            ,'provinsi'=>$provinsi, 'kota_kabupaten'=>$kota_kabupaten
            ]
        );
    }
    public function pembayaran_sbu_konstruksi()
    {
        $pembayaran_sbu_konstruksi = DB::table('pembayaran_sbu_konstruksi')
                                    ->select('id_pembayaran_sbu_konstruksi','rekening_pembayaran_id'
                                    ,'anggota_id','no_rekening','atas_nama','keterangan','bukti_pembayaran','sudah_diproses')
                                    ->where('sudah_diproses','=','belum_diproses')
                                    ->get();
        $rekening = DB::table('rekening_pembayarans')
                  ->select('id_rekening_pembayaran','nama_bank','no_rek','atas_nama')
                  ->get();
        $provinsi = DB::table('provinsis')
                    ->select('id_provinsi','provinsi')
                    ->get();
        $kota_kabupaten = DB::table('kota_kabupaten')
                        ->select('id_kota_kabupaten','kota_kabupaten','provinsi_id')
                        ->get();
        $anggota = DB::table('anggotas')
                  ->select('id_anggota','nomor_keanggotaan','nama_perusahaan','nama_penanggung_jawab',
                  'alamat_perusahaan','provinsi_id','kota_kabupaten_id','telepon_telex_fax','no_hp_1'
                  ,'no_hp_2','kta_sampai','foto_kta','email','foto_penanggung_jawab')
                  ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.pembayaran.sbu_konstruksi', $data,
            ['anggota' => $anggota,'pembayaran_sbu_konstruksi'=>$pembayaran_sbu_konstruksi,
                'rekening'=>$rekening,'provinsi'=>$provinsi,'kota_kabupaten'=>$kota_kabupaten]
        );
    }

    public function pembayaran_sbu_non_konstruksi()
    {
        $pembayaran_sbu_non_konstruksi = DB::table('pembayaran_sbu_non_konstruksis')
                                      ->select('id_pembayaran_sbu_non_konstruksi','anggota_id'
                                      ,'rekening_pembayaran_id','no_rekening','atas_nama','keterangan',
                                      'bukti_pembayaran','sudah_diproses')
                                      ->where('sudah_diproses','=','belum_diproses')
                                      ->get();
        $rekening = DB::table('rekening_pembayarans')
                  ->select('id_rekening_pembayaran','nama_bank','no_rek','atas_nama')
                  ->get();
        $provinsi = DB::table('provinsis')
                   ->select('id_provinsi','provinsi')
                   ->get();
        $kota_kabupaten = DB::table('kota_kabupatens')
                        ->select('id_kota_kabupaten','kota_kabupaten','provinsi_id')
                        ->get();
        $anggota = DB::table('anggotas')
                  ->select('id_anggota','nomor_keanggotaan','nama_perusahaan','nama_penanggung_jawab',
                    'alamat_perusahaan','provinsi_id','kota_kabupaten_id','telepon_telex_fax','no_hp_1','no_hp_2'
                    ,'kta_sampai','foto_kta','email','foto_penanggung_jawab')
                 ->where('level','=','anggota')
                 ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.pembayaran.sbu_non_konstruksi', $data,
            ['anggota' => $anggota,'rekening'=>$rekening
                ,'pembayaran_sbu_non_konstruksi'=>$pembayaran_sbu_non_konstruksi
                ,'provinsi'=>$provinsi,'kota_kabupaten'=>$kota_kabupaten
            ]
        );
    }

    public function pembayaran_Registrasi_sbu_konstruksi()
    {

        $pembayaran_registrasi_sbu_konstruksi = DB::table('pembayaran_registrasi_sbu_konstruksis')
                                               ->select('id_pembayaran_registrasi_sbu_konstruksi','rekening_pembayaran_id','anggota_id','no_rekening')
                                               ->get();
        $rekening = DB::table('rekening_pembayarans')
                 ->select('nama_bank','no_rek','atas_nama','id_rekening_pembayaran')
                 ->get();
        $provinsi = DB::table('provinsis')
                 ->select('id_provinsi','provinsi')
                 ->get();
        $kota_kabupaten = DB::table('kota_kabupatens')
                        ->select('id_kota_kabupaten','kota_kabupaten','provinsi_id')
                        ->get();
        $anggota = DB::table('anggotas')
        ->select('id_anggota','nomor_keanggotaan','nama_perusahaan','nama_penanggung_jawab',
          'alamat_perusahaan','provinsi_id','kota_kabupaten_id','telepon_telex_fax','no_hp_1','no_hp_2'
          ,'kta_sampai','foto_kta','email','foto_penanggung_jawab')
       ->where('level','=','anggota')
       ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.pembayaran.registrasi_sbu_konstruksi', $data,
            ['anggota' => $anggota,'provinsi'=>$provinsi,'kota_kabupaten'=>$kota_kabupaten,
                'rekening'=>$rekening,'pembayaran_registrasi_sbu_konstruksi'=>$pembayaran_registrasi_sbu_konstruksi]
        );
    }

    public function h_pembayaran_kta()
    {
        $rekening = DB::table('rekening_pembayarans')
                   ->select('id_rekening_pembayaran','nama_bank','no_rek','atas_nama')
                   ->get();
        $pembayaran_kta = DB::table('pembayaran_ktas')
                        ->select('id_pembayaran_kta','rekening_pembayaran_id','anggota_id','no_rekening'
                        ,'atas_nama','keterangan','bukti_pembayaran','sudah_diproses')
                        ->where('sudah_diproses','=','belum_diproses')
                        ->get();
        $anggota = DB::table('anggotas')
                ->select('id_anggota','nomor_keanggotaan','nama_perusahaan','nama_penanggung_jawab',
                'alamat_perusahaan','provinsi_id','kota_kabupaten_id','telepon_telex_fax','no_hp_1','no_hp_2'
                ,'kta_sampai','foto_kta','email','foto_penanggung_jawab')
                ->where('level','=','anggota')
                ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.history_pembayaran.kta', $data, ['anggota' => $anggota,
            'rekening'=>$rekening,'pembayaran_kta'=>$pembayaran_kta]);
    }

    public function h_pembayaran_sbu_konstruksi()
    {
        $rekening = DB::table('rekening_pembayarans')
        ->select('nama_bank','no_rek','atas_nama','id_rekening_pembayaran')
        ->get();
        $pembayaran_sbu_konstruksi = DB::table('pembayaran_sbu_konstruksis')
                                    ->select('id_pembayaran_sbu_konstruksi','rekening_pembayaran_id','anggota_id'
                                ,'no_rekening','atas_nama','keterangan','bukti_pembayaran','sudah_diproses')
                                ->where('sudah_diproses','=','belum_diproses')
                                ->get();
        $anggota = DB::table('anggotas')
                ->select('id_anggota','nomor_keanggotaan','nama_perusahaan','nama_penanggung_jawab',
                'alamat_perusahaan','provinsi_id','kota_kabupaten_id','telepon_telex_fax','no_hp_1','no_hp_2'
                ,'kta_sampai','foto_kta','email','foto_penanggung_jawab')
                ->where('level','=','anggota')
                ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.history_pembayaran.sbu_konstruksi', $data, ['anggota' => $anggota,
        'rekening'=>$rekening,'pembayaran_sbu_konstruksi'=>$pembayaran_sbu_konstruksi
        ]);
    }

    public function h_pembayaran_sbu_non_konstruksi()
    {
        $rekening = DB::table('rekening_pembayarans')
                  ->select('nama_bank','no_rek','atas_nama','id_rekening_pembayaran')
                  ->get();
        $pembayaran_sbu_non_konstruksi = DB::table('pembayaran_sbu_non_konstruksis')
                                        ->select('id_pembayaran_sbu_non_konstruksi','anggota_id',
                                        'rekening_pembayaran_id','no_rekening','atas_nama',
                                        'keterangan','bukti_pembayaran','sudah_diproses'
                                    )->get();
        $anggota = DB::table('anggotas')
                ->select('id_anggota','nomor_keanggotaan','nama_perusahaan','nama_penanggung_jawab',
                'alamat_perusahaan','provinsi_id','kota_kabupaten_id','telepon_telex_fax','no_hp_1','no_hp_2'
                ,'kta_sampai','foto_kta','email','foto_penanggung_jawab')
                ->where('level','=','anggota')
                ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.history_pembayaran.sbu_non_konstruksi', $data, ['anggota' => $anggota,
            'rekening'=>$rekening,'pembayaran_sbu_non_konstruksi'=>$pembayaran_sbu_non_konstruksi
            ]);
    }

    public function h_pembayaran_registrasi_sbu_konstruksi()
    {
        $rekening = DB::table('rekening_pembayarans')
                 ->select('nama_bank','no_rek','atas_nama','id_rekening_pembayaran')
                 ->get();
        $pembayaran_registrasi_sbu_konstruksi = DB::table('pembayaran_registrasi_sbu_konstruksis')
                                              ->select('id_pembayaran_registrasi_sbu_konstruksi','rekening_pembayaran_id',
                                            'anggota_id','no_rekening','atas_nama','keterangan','bukti_pembayaran',
                                            'registrasi_tahun_ke','tanggal_registrasi_tahun_ke','sudah_diproses')
                                            ->where('sudah_diproses','=','belum_diproses')
                                            ->get();
        $anggota =DB::table('anggotas')
                ->select('id_anggota','nomor_keanggotaan','nama_perusahaan','nama_penanggung_jawab',
                 'alamat_perusahaan','provinsi_id','kota_kabupaten_id','telepon_telex_fax','no_hp_1','no_hp_2'
                ,'kta_sampai','foto_kta','email','foto_penanggung_jawab')
                ->where('level','=','anggota')
                ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.history_pembayaran.registrasi_sbu_konstruksi', $data
            ,['anggota' => $anggota,'rekening'=>$rekening,
                'pembayaran_registrasi_sbu_konstruksi'=>$pembayaran_registrasi_sbu_konstruksi]
        );
    }
    public function admin_download()
    {
        $download = DB::table('downloads')
                  ->select('id_download','kategori_download_id','dokumen','judul')
                  ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.setting.download', $data, ['download' => $download]);
    }

    public function foto_pengurus()
    {
        $foto_pengurus = DB::table('foto_penguruses')
                        ->select('id_pengurus','periode','foto')
                        ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.pengurus.foto_pengurus', $data, ['foto_pengurus' => $foto_pengurus]);
    }

    public function update_foto_pengurus(Request $request, $id)
    {
        $foto_pengurus = foto_pengurus::find($id);
        $foto_pengurus->periode = $request->periode;
        $foto_pengurus->update();
        if ($request->hasFile('foto')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $foto_pengurus->foto;
            $file_path = public_path('foto_periode/' . $file_name);
            File::delete($file_path);
            $request->file('foto')->move('foto_periode/', $request->file('foto')->getClientOriginalName());
            $foto_pengurus->foto = $request->file('foto')->getClientOriginalName();
            $foto_pengurus->save();
            return back()->with('success', 'Anda Berhasil Update Data foto pengurus');
        }
    }

    public function struktur_organisasi()
    {
        $struktur_organisasi = DB::table('struktur_organisasis')
                            ->select('id_struktur_organisasi','periode','gambar')
                            ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.pengurus.struktur_organisasi', $data, ['struktur_organisasi' => $struktur_organisasi]);

    }

    public function update_struktur_organisasi(Request $request, $id)
    {
        $struktur_organisasi = struktur_organisasi::find($id);
        $struktur_organisasi->periode = $request->periode;
        if ($request->hasFile('gambar')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $struktur_organisasi->gambar;
            $file_path = public_path('foto_struktur_organisasi/' . $file_name);
            File::delete($file_path);
            $request->file('gambar')->move('foto_struktur_organisasi/', $request->file('gambar')->getClientOriginalName());
            $struktur_organisasi->gambar = $request->file('gambar')->getClientOriginalName();
            $struktur_organisasi->save();
            return back()->with('success', 'Anda Berhasil Update Data Struktur Organisasi');
        }
    }

    public function download()
    {
        $download = DB::table('downloads')
                  ->select('id_download','kategori_download_id','dokumen','judul')
                  ->get();
        $kategori_download = DB::table('kategori_downloads')
                            ->select('id_kategori_download','kategori_download')
                            ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.setting.download', $data, ['kategori_download' => $kategori_download,
            'download' => $download
        ]);
    }

    public function tambah_download(downloadRequest $request)
    {

        $download = new downloads();
        $download->judul = $request->judul;
        $download->kategori_download_id = $request->kategori_download_id;
        if ($request->hasFile('dokumen')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('dokumen')->move('dokumen/', $request->file('dokumen')->getClientOriginalName());
            $download->dokumen = $request->file('dokumen')->getClientOriginalName();
            $download->save();
        }
        return back()->with('success', 'Anda Berhasil Tambah Data Download');

    }

    public function update_download(Request $request, $id)
    {
        $download = downloads::find($id);
        $download->judul = $request->judul;
        $download->kategori_download_id = $request->kategori_download_id;
        if ($request->hasFile('dokumen')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $download->dokumen;
            $file_path = public_path('foto_dokumen/' . $file_name);
            File::delete($file_path);
            $request->file('dokumen')->move('dokumen/', $request->file('dokumen')->getClientOriginalName());
            $download->dokumen = $request->file('dokumen')->getClientOriginalName();
            $download->save();
            return back()->with('success', 'Anda Berhasil Update Data Download');
        }
    }

    public function delete_download($id)
    {
        $download = downloads::find($id);
        $download->delete();
        $file_name = $download->dokumen;
        $file_path = public_path('foto_dokumen/' . $file_name);
        File::delete($file_path);
        return back()->with('success', 'Anda Berhasil Delete Data Download');
    }

    public function download_dokumen(Request $request, $file)
    {

        return response()->download(public_path('dokumen/' . $file));

    }

    public function tambah_detail_sbu_konstruksi(Request $request)
    {
        $detail_sbu_konstruksi = new detail_sbu_konstruksi();
        $detail_sbu_konstruksi->sbu_konstruksi_id = $request->sbu_konstruksi_id;
        $detail_sbu_konstruksi->sub_klasifikasi_sbu_konstruksi_id = $request->sub_klasifikasi_sbu_konstruksi_id;
        $detail_sbu_konstruksi->save();
        return back()->with('success', 'Anda Berhasil Tambah Data detail sbu konstruksi');
    }

    public function tambah_detail_sbu_non_konstruksi(Request $request)
    {
        $detail_sbu_non_konstruksi = new detail_sbu_non_konstruksi();
        $detail_sbu_non_konstruksi->sbu_non_konstruksi_id = $request->sbu_non_konstruksi_id;
        $detail_sbu_non_konstruksi->s_k_sbu_non_konstruksi_id = $request->s_k_sbu_non_konstruksi_id;
        $detail_sbu_non_konstruksi->save();
        return back()->with('success', 'Anda Berhasil Tambah Data detal sbu Non Konstruksi');
    }
    public function tentang_kami(){
        $tentang_kami = DB::table('tentang_kamis')
                      ->select('id_tentang_kami','deskripsi','visi','misi','sejarah','nomor_hp','email1','email2',
                    'alamat','map','instagram','facebook')
                    ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.tentang_kami.index', $data, [
          'tentang_kami'=>$tentang_kami
        ]);
    }
    public function update_tentang_kami(Request $request, $id){
        $tentang_kami = tentang_kami::find($id);
        $tentang_kami->deskripsi = $request->deskripsi;
        $tentang_kami->visi = $request->visi;
        $tentang_kami->misi = $request->misi;
        $tentang_kami->sejarah = $request->sejarah;
        $tentang_kami->nomor_hp = $request->nomor_hp;
        $tentang_kami->email1 = $request->email1;
        $tentang_kami->email2 = $request->email2;
        $tentang_kami->alamat = $request->alamat;
        $tentang_kami->instagram = $request->instagram;
        $tentang_kami->facebook = $request->facebook;
        $tentang_kami->update();
        if ($request->hasFile('map')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('map')->move('foto_map/', $request->file('map')->getClientOriginalName());
            $tentang_kami->map = $request->file('map')->getClientOriginalName();
            $tentang_kami->save();
            return back()->with('success', 'Anda Berhasil Update Data Tentang Kami');
        }

    }
    public function profil(){
        $profil = DB::table('profils')
                ->select('id_profil','profil','kontak_perkindo','foto_lokasi')
                ->get();
        $data = ['datas' => anggota::where('id_anggota', '=', session('loginID'))->first()];
        return view('backend.admin.setting.profil', $data, [
            'profil' => $profil
        ]);
    }
    public function update_profil(Request $request, $id){
        $profil= profils::find($id);
        $profil->profil = $request->profil;
        $profil->kontak_perkindo = $request->kontak_perkindo;
        $profil->update();
        if ($request->hasFile('foto_lokasi')) //Cek apakah ada file avatar yang di Upload
        {
            $file_name = $profil->foto_lokasi;
            $file_path = public_path('foto_lokasi_profil/' . $file_name);
            File::delete($file_path);
            $request->file('foto_lokasi')->move('foto_lokasi_profil/', $request->file('foto_lokasi')->getClientOriginalName());
            $profil->foto_lokasi = $request->file('foto_lokasi')->getClientOriginalName();
            $profil->save();

        }
        return back()->with('success', 'Anda Berhasil Update Data Profil');

    }
    public function update_kta(Request $request,$id){
        $kta = pembayaran_kta::find($request->id_pembayaran_kta);
        $nomor_anggota = $kta->anggota_id;
        if($kta){
            $anggota_kta = anggota::find($nomor_anggota);
            $anggota_kta->kta_sampai = $request->kta_sampai;
            $anggota_kta->update();
            $kta->sudah_diproses = "sudah diproses";
            $kta->update();
            if ($request->hasFile('foto_kta')) //Cek apakah ada file avatar yang di Upload
            {
                $file_name = $anggota_kta->foto_kta;
                $file_path = public_path('foto_perusahaan/foto_kta/' . $file_name);
                File::delete($file_path);
                $request->file('foto_kta')->move('foto_perusahaan/foto_kta/', $request->file('foto_kta')->getClientOriginalName());
                $anggota_kta->foto_kta = $request->file('foto_kta')->getClientOriginalName();
                $anggota_kta->save();
                return back()->with('success', 'Anda Berhasil Update Data kta');
            }
        }
    }
    public function tambah_sbu_konstruksi_anggota(Sbu_konstruksiRequest $request){
        $sbu_konstruksiRequest = sbu_konstruksi::where('no_seri_formulir','=',$request->no_seri_formulir)->first();
        if($sbu_konstruksiRequest){
            return back()->with('fail', 'no Seri Formulir Tidak Boleh Sama');
        }
        else{
            $sub_konstruksi = new sbu_konstruksi();
            $sub_konstruksi->no_seri_formulir = $request->no_seri_formulir;
            $sub_konstruksi->anggota_id = $request->anggota_id;
            $sub_konstruksi->tanggal_masuk = $request->tanggal_masuk;
            $sub_konstruksi->berlaku_sampai = $request->berlaku_sampai;
            $sub_konstruksi->registrasi_tahun_ke_2 = $request->registrasi_tahun_ke_2;
            $sub_konstruksi->registrasi_tahun_ke_3 = $request->registrasi_tahun_ke_3;
            $sub_konstruksi->tenaga_ahli = $request->tenaga_ahli;
            $pembayaran_sbu_konstruksi = pembayaran_sbu_konstruksi::find($request->id_pembayaran_sbu_konstruksi);
            $pembayaran_sbu_konstruksi->sudah_diproses = "sudah diproses";
            $pembayaran_sbu_konstruksi->update();
            if ($request->hasFile('foto')) //Cek apakah ada file avatar yang di Upload
            {
                $request->file('foto')->move('foto_sbu_konstruksi/', $request->file('foto')->getClientOriginalName());
                $sub_konstruksi->foto = $request->file('foto')->getClientOriginalName();
                $sub_konstruksi->save();
                return back()->with('success', 'Anda Berhasil Tambah Data Sbu Konstruksi');
            }
        }
    }
    public function tambah_sbu_non_konstruksi_anggota(sbu_non_konstruksiRequest $request){
        $sbu_non_konstruksiRequest = sbu_non_konstruksi::where('no_seri_formulir','=',$request->no_seri_formulir)->first();
        if($sbu_non_konstruksiRequest){
            return back()->with('fail', $request->no_seri_formulir.'Sudah Diinput');
        }
        $sbu_non_konstruksi = new sbu_non_konstruksi();
        $sbu_non_konstruksi->no_seri_formulir = $request->no_seri_formulir;
        $sbu_non_konstruksi->anggota_id = $request->anggota_id;
        $sbu_non_konstruksi->tanggal_dikeluarkan_sbu = $request->tanggal_dikeluarkan_sbu;
        $sbu_non_konstruksi->pj_operasional = $request->pj_operasional;
        $pembayaran_sbu_non_konstruksi = pembayaran_sbu_non_konstruksi::find($request->id_pembayaran_sbu_non_konstruksi);
        $pembayaran_sbu_non_konstruksi->sudah_diproses = "sudah diproses";
        $pembayaran_sbu_non_konstruksi->update();

        if ($request->hasFile('file')) //Cek apakah ada file avatar yang di Upload
        {
            $request->file('file')->move('foto_sbu_non_konstruksi/', $request->file('file')->getClientOriginalName());
            $sbu_non_konstruksi->file = $request->file('file')->getClientOriginalName();
            $sbu_non_konstruksi->save();
            return back()->with('success', 'Anda Berhasil Tambah Data Sbu non Konstruksi');
        }
    }
    public function update_registrasi_sbu_konstruksi(Request $request,$id){
        $pembayaran_registrasi_sbu_konstruksi = pembayaran_registrasi_sbu_konstruksi::find($id);
        $pembayaran_registrasi_sbu_konstruksi->sudah_diproses = "sudah diproses";
        $pembayaran_registrasi_sbu_konstruksi->update();
        return back()->with('success', 'Anda Berhasil Update Data Registrasi Sbu Konstruksi');

    }

}

