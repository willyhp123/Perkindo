
@extends('frontend.halaman_utama.layout.header')

@section('title','Perkindo')
@section('content')
<section class="hero hero-bg d-flex justify-content-center align-items-center" id="home">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-10 col-12 d-flex flex-column justify-content-center align-items-center">
                <div class="hero-text">
                    <h2 class="text-white" data-aos="fade-up"> <strong>PERKINDO BERSATU </strong> <br>
                        <h2 class="text-white" style="font-family: 'Yellowtail', cursive;"> Tingkatkan kinerja dan
                            <strong>Loyalitas</strong>
                            Untuk Lebih Maju dan <strong>Profesional</strong>
                        </h2>
                    </h2>

                    <a href=" #hubungi" class="custom-btn btn-bg btn mt-3" data-aos="fade-up" data-aos-delay="300">
                        Hubungi Kami</a>

                </div>
            </div>

            <div class="col-lg-6 col-12">
                <br><br><br><br>
                <div class="hero-image" data-aos="fade-up" data-aos-delay="200">

                    <img src="{{asset('halaman_utama/images/logo1.png')}}" class="img-fluid" alt="working girl">
                </div>
            </div>

        </div>
    </div>
</section>
<section class="about section-padding pb-0" id="about">

    <div class="container">
        <div class="row">

            <div class="col-lg-7 mx-auto col-md-10 col-12">
                <div class="about-info">

                    <h2 class="mb-4" data-aos="fade-up">Tentang <strong>Kami</strong></h2>

                        <p class="mb-0" data-aos="fade-up">
                           Deskripsi
                        </p>

                        <div class="buttonprofil">
                            <ul>
                                <a data-aos="fade-up" href="#popup" class="left">Visi & Misi</a>
                                <a data-aos="fade-up" href="#popup1" class="right">Sejarah</a>
                            </ul>
                            <!-- Left Pop Up-->
                            <div class="popup-wrapper" id="popup">
                                <div class="popup-container" id="pop">
                                    <!-- Isi Popup -->
                                    <h1
                                        style="font-size : var(--h3-font-size); font-family:Montserrat-Bold; color:var(--primary-color);">
                                        Visi & Misi PERKINDO</h1>
                                    <h3 style="font-family: Montserrat-Bold; color: var(--secondary-color);">
                                        Visi
                                    </h3>
                                    <p style="font-size:13px; text-align: justify;">Visi</p>

                                    <h3 style="font-family: Montserrat-Bold; color: var(--secondary-color);">Misi
                                    </h3>
                                    <ul style="font-size:13px">
                                        <p style="font-size:12px; text-align: justify;"> Misi</p>
                                    </ul>
                                    <!-- Tombol Close Popup -->
                                    <a class="popup-close" href="#buttonprofil">X</a>
                                </div>
                            </div>
                            <!-- Left Pop Up-->
                            <div class="popup-wrapper" id="popup1">
                                <div class="popup-container" id="pop">
                                    <!-- Isi Popup -->
                                    <h1
                                        style="font-size : var(--h3-font-size); font-family:Montserrat-Bold; color:var(--primary-color);">
                                        Sejarah <strong>PERKINDO</strong> KALBAR</h1>
                                    <p style="font-size:12px; text-align: justify;"> Sejarah
                                    <!-- Tombol Close Popup -->
                                    <a class="popup-close" href="#buttonprofil">X</a>
                                </div>
                            </div>
                        </div>
                    <div class="about-image" data-aos="fade-up" data-aos-delay="200">

                        <img src="{{asset('halaman_utama/images/kantor.png')}}" class="img-fluid" alt="office">
                    </div>
                </div>

            </div>
        </div>

</section>
<!-- PROJECT -->
<section class="project section-padding" id="profil">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12 col-12">

                <h2 class="mb-5 text-center" data-aos="fade-up">
                    Kepuasan anda prioritas kami,
                    <strong>Terus berkarya</strong> untuk kemajuan Indonesia
                </h2>

                <div class="owl-carousel " id="project-slide">


                        <div class="item project-wrapper" data-aos="fade-up" data-aos-delay="100">
                            <img src="" class="img-fluid" alt="project image"
                                 style="height:300px!important">
                            <div class="project-info">
                                <span>
                                    <a href="" class="btn btn-info">
                                        Berita
                                    </a>
                                </span>
                            </div>
                        </div>

                </div>
            </div>

        </div>
    </div>
</section>
    @endsection



