<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <style>
        li{
            font-size: 15px;
        }
    </style>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
    <!-- Theme style -->
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/summernote/summernote-bs4.min.css')}}">
    <!-- CodeMirror -->
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">

            <li class="nav-item dropdown user-menu">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="user-image img-circle elevation-2" alt="User Image">
                    <span class="d-none d-md-inline">Alexander Pierce</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <!-- User image -->
                    <li class="user-header bg-primary">
                        <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">

                        <p>
                            Alexander Pierce - Web Developer
                            <small>Member since Nov. 2012</small>
                        </p>
                    </li>
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="row">
                            <div class="col-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </div>
                        <!-- /.row -->
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                        <a href="#" class="btn btn-default btn-flat float-right">Sign out</a>
                    </li>
                </ul>
            </li>

        </ul>
    </nav>
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">Alexander Pierce</a>
                </div>
            </div>
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-user" style="color: cyan;"></i>
                            <p>
                                History Pembayaran
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('h_kta_frontend')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan"></i>
                                    <p>KTA</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('h_registrasi_sbu_konstruksi_frontend')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan"></i>
                                    <p>Registrasi SBU Konstruksi </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('h_sbu_konstruksi_frontend')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan"></i>
                                    <p>SBU Konstruksi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('h_sbu_non_konstruksi_frontend')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan"></i>
                                    <p>SBU Non Konstruksi</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-cogs" style="color: cyan"></i>
                            <p>
                                KTA DAN SBU
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('kta_sbu_kta_frontend')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color:cyan;"></i>
                                    <p>KTA</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('kta_sbu_konstruksi_frontend')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color:cyan;"></i>
                                    <p>SBU Konstruksi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('kta_sbu_non_konstruksi_frontend')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color:cyan"></i>
                                    <p>SBU Non Konstruksi</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            <i class="nav-icon fas fa-book" style="color: cyan;"></i>
                            <p>
                                Pembayaran
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('pembayaran_kta_and_sbu_frontend')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan;"></i>
                                    <p>KTA dan SBU</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('pembayaran_registrasi_sbu_konstruksi_frontend')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan;"></i>
                                    <p>Registrasi SBU Konstruksi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('pembayaran_registrasi_sbu_non_konstruksi_frontend')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan;"></i>
                                    <p>Registrasi SBU Non Konstruksi</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('user_pengguna_frontend')}}" class="nav-link">
                            <i class="nav-icon far fa-image" style="color:cyan"></i>
                            <p>
                                User Pengguna
                            </p>
                        </a>
                    </li>
                </ul>

            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
@yield('content')
@include('frontend.admin.layout.footer')
