@extends('frontend.admin.layout.header')
@section('title','Berita')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Berita
                        </button>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Berita</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="">
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Judul</label>
                                                <input type ="text" name ="judul" class ="form-control" placeholder="Nama Agama">
                                            </div>
                                                <div class ="form-group">
                                                    <label>Isi Berita</label>
                                                    <textarea id="summernote"></textarea>
                                                </div>
                                            <div class ="form-group">
                                                <label>Foto</label>
                                                <input type="file" name ="foto_berita">
                                            </div>
                                            <div class ="form-group">
                                                <label>Tanggal Posting</label>
                                                <input type="date" name ="tanggal_posting" class ="form-control">
                                            </div>
                                            </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Berita</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Judul</th>
                            <th>Foto</th>
                            <th>Tanggal Posting</th>
                            <th>publish</th>
                            <th width="20%">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
