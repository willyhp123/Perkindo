@extends('frontend.admin.layout.header')
@section('title','Data Perusahaan')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Perusahaan
                        </button>
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Perusahaan</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post">
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Nomor Keanggotaan</label>
                                                <input type ="text" name ="nomor_keanggotaan" class ="form-control" placeholder="Nomor Keanggotaan">
                                            </div>
                                            <div class ="form-group">
                                                <label>Nama Perusahaan</label>
                                                <input type ="text" name ="nama_perusahaan" class ="form-control" placeholder="Nama Perusahaan">
                                            </div>
                                            <div class ="form-group">
                                                <label>Nama Penanggung Jawab</label>
                                                <input type ="text" name ="nama_penanggung_jawab" class ="form-control" placeholder="Nama Penanggung Jawab">
                                            </div>
                                            <div class ="form-group">
                                                <label>Alamat Perusahaan</label>
                                                <input type ="text" name ="alamat_perusahaan" class ="form-control" placeholder="Alamat Perusahaan">
                                            </div>
                                            <div class ="form-group">
                                                <label>Provinsi</label>
                                                <select class ="form-control">
                                                    <option>--pilih Provinsi</option>
                                                </select>
                                            </div>
                                            <div class ="form-group">
                                                <label>Kota/Kabupaten</label>
                                                <select class ="form-control">
                                                    <option>--pilih Kota/Kabupaten--</option>
                                                </select>
                                            </div>
                                            <div class ="form-group">
                                                <label>Telepon</label>
                                                <input type ="text" name ="kota_kabupaten" class ="form-control" placeholder="Telepon/fax/telex">
                                            </div>
                                            <div class ="form-group">
                                                <label>No HP 1</label>
                                                <input type ="text" name ="no_hp_1" class ="form-control" placeholder="Nomor HP 1">
                                            </div>
                                            <div class ="form-group">
                                                <label>No HP 2</label>
                                                <input type ="text" name ="no_hp_2" class ="form-control" placeholder="Nomor HP 2">
                                            </div>
                                            <div class ="form-group">
                                                <label>Email</label>
                                                <input type ="email" name ="email" class ="form-control" placeholder="Email">
                                            </div>
                                            <div class ="form-group">
                                                <label>Foto Penanggung Jawab</label>
                                                <input type ="file" name ="file_penanggung_jawab">
                                            </div>
                                            <div class ="form-group">
                                                <label>KTP Berlaku Sampai</label>
                                                <input type ="date" name ="kta_berlaku_sampai" class ="form-control">
                                            </div>
                                            <div class ="form-group">
                                                <label>KTA</label>
                                                <input type ="file" name ="kta">
                                            </div>

                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Data Perusahaan</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nomor Keanggotaan</th>
                            <th>Nama Perusahaan</th>
                            <th>Nama Penanggung Jawab</th>
                            <th width="20%">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
