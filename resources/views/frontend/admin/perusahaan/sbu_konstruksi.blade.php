@extends('frontend.admin.layout.header')
@section('title','Sbu Konstruksi')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class ="row">
                        <div class ="col-6">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                                <i class="fa fa-plus"></i> Tambah Data Sbu Konstruksi
                            </button>
                            <div class="modal fade" id="modal-default">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Tambah Data Agama</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="post">
                                            <div class="modal-body">
                                                <div class ="form-group">
                                                    <label>No Seri Formulir</label>
                                                    <input type ="text" name ="no_seri_formulir" class ="form-control" placeholder="No Seri Formulir">
                                                </div>
                                                <div class ="form-group">
                                                    <label>Anggota</label>
                                                 <select class ="form-control" name ="anggota">
                                                     <option>--pilih Perusahaan</option>
                                                 </select>
                                                </div>
                                                <div class ="form-group">
                                                    <label>Tanggal Dikeluarkan SBU</label>
                                                    <input type ="date" name ="tanggal_dikeluarkan_sbu" class ="form-control">
                                                </div>
                                                <div class ="form-group">
                                                    <label>Berlaku Sampai SBU</label>
                                                    <input type ="date" name ="tanggal_dikeluarkan_sbu" class ="form-control">
                                                </div>
                                                <div class ="form-group">
                                                    <label>Registrasi Tahun ke 2</label>
                                                    <input type ="date" name ="registrasi_tahun_ke_2" class ="form-control">
                                                </div>
                                                <div class ="form-group">
                                                    <label>Registrasi Tahun ke 3</label>
                                                    <input type ="date" name ="registrasi_tahun_ke_3" class ="form-control">
                                                </div>
                                                <div class ="form-group">
                                                    <label>Tenaga Ahli</label>
                                                    <input type ="text" name ="tenaga_ahli" class ="form-control" placeholder="Tenaga Ahli">
                                                </div>
                                                <div class ="form-group">
                                                    <label>file</label>
                                                    <input type ="file" name ="file">
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                        <div class ="col-6">
                            <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Sbu Konstruksi</li>
                            </ol>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>No seri Formulir</th>
                            <th>Nomor Keanggotaan</th>
                            <th>Nama Perusahaan</th>
                            <th>Tanggal Masuk</th>
                            <th>Berlaku Sampai</th>
                            <th width="20%">Aksi</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>12324</td>
                            <td>123123</td>
                            <td>PT123</td>
                            <td>Tanggal Masuk</td>
                            <td>Berlaku Sampai</td>
                            <td>
                                <!-- Button trigger modal -->
                                <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-1'><i class="ft ft-edit"></i>Edit</a>
                                <div class="modal fade" id="m_edit-1">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Edit Pejabat</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            <div class="modal-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-1">
                                    <i class = "ft ft-trash"></i> Hapus
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="del-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">

                                            </div>
                                            <div class = "border"></div>
                                            <div class="modal-body">
                                                Apa Anda Yakin ?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <a href = "" class = "btn btn-danger"><i class = "ft ft-trash"></i>Hapus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
