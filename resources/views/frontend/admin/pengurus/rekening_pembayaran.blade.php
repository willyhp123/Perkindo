@extends('frontend.admin.layout.header')
@section('title','Rekening Pembayaran')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Rekening
                        </button>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Rekening Pembayaran</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post">
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Nama BANK</label>
                                                <input type ="text" name ="nama_bank" class ="form-control" placeholder="Nama Bank">
                                            </div>
                                            <div class ="form-group">
                                                <label>No Rek</label>
                                                <input type ="text" name ="no_rek" class ="form-control" placeholder="No Rekening">
                                            </div>
                                            <div class ="form-group">
                                                <label>Atas Nama</label>
                                                <input type ="text" name ="atas_nama" class ="form-control" placeholder="Nama Pemilik Rekening">
                                            </div>
                                            <div class ="form-group">
                                                <label>Nama BANK</label>
                                                <input type ="text" name ="nama_bank" class ="form-control" placeholder="Nama Bank">
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Home</li>
                            <li class="breadcrumb-item active">Rekening Pembayaran</li>
                        </ol>
                    </div>
                </div>
                <div class="card-tools">

                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nama Bank</th>
                            <th>No Rek</th>
                            <th>Atas Nama</th>
                            <th width="20%">Aksi</th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
