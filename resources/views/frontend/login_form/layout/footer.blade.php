<script src="{{asset('login_template/vendor/jquery/jquery-3.2.1.min.js')}}"></script>

<script src="{{asset('login_template/vendor/animsition/js/animsition.min.js')}}"></script>

<script src="{{asset('login_template/vendor/bootstrap/js/popper.js')}}"></script>
<script src="{{asset('login_template/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{asset('login_template/vendor/select2/select2.min.js')}}"></script>

<script src="{{asset('login_template/vendor/daterangepicker/moment.min.js')}}"></script>
<script src="{{asset('login_template/vendor/daterangepicker/daterangepicker.js')}}"></script>

<script src="{{asset('login_template/vendor/countdowntime/countdowntime.js')}}"></script>

<script src="{{asset('login_template/js/main.js')}}"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
</script>
<script defer src="https://static.cloudflareinsights.com/beacon.min.js/v652eace1692a40cfa3763df669d7439c1639079717194" integrity="sha512-Gi7xpJR8tSkrpF7aordPZQlW2DLtzUlZcumS8dMQjwDHEnw9I7ZLyiOj/6tZStRBGtGgN6ceN6cMH8z7etPGlw==" data-cf-beacon='{"rayId":"6da2dac6ef774d9f","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2021.12.0","si":100}' crossorigin="anonymous"></script>
</body>
</html>

