@include('backend.halaman_utama.layout.header_table')
@section('title','Perkindo Kalbar Agenda')
<br><br>
<div class="container">
    <h2 align="center" style="font-family: 'Trebuchet MS'; color: #10125f;">DATA ANGGOTA PERKINDO KALBAR
    </h2>
    <br>
    <form method="post">
        <div class="form-group">
            <!--Script untuk memanggil Javascript-->
            <table id="example" class="display responsive nowrap" style="width:100%" cellpadding="1000px">
                <thead>
                <tr style="color: #f15a24; background-color: #10125f;">
                    <th width="5%">No</th>
                    <th>Nama Agenda</th>
                    <th>Tanggal Masuk</th>
                    <th>Tanggal Selesai</th>
                </tr>
                </thead>
                <tbody style="color: #10125f;">
                    @foreach($agenda as $key=>$val)
                        <Tr>
                            <td>{{$key+1}}</td>
                            <td>{{$val->nama_agenda}}</td>
                            <td>{{$val->tanggal_mulai}}</td>
                            <td>{{$val->tanggal_selesai}}</td>
                        </Tr>
                     @endforeach
                </tbody>
            </table>
        </div>

    </form>
</div>
@include('backend.halaman_utama.layout.footer_table')
