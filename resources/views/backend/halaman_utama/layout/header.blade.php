<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="icon" href="{{asset('halaman_utama/images/logotitle.png')}}" type="image" size=96x96;>
    <title>@yield('title')</title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/templatemo-digital-trend.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/templatemo-digital-trend.css')}}">

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="51">

<!-- MENU BAR -->
<div class="navbar navbar-expand-lg bg-light navbar-light">
    <div class="container-fluid">
        <a href="/" class="navbar-brand"> <img src="{{asset('halaman_utama/images/logo.png')}}"> PERKINDO
            <h6>persatuan konsultan indonesia</h6>
        </a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
            <div class="navbar-nav ml-auto">
                <a href="/" class="nav-item nav-link active">Beranda</a>
                <a href="#about" class="nav-item nav-link ">Profil</a>
                <a href="#profil" class="nav-item nav-link ">Informasi</a>
                <a href="#hubungi" class="nav-item nav-link ">Kontak</a>
                <a href="" class="nav-item nav-link "></a>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Menu
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('berita')}}">Berita</a>
                        <a class="dropdown-item" href="{{route('agenda')}}">Agenda</a>
                        <a class="dropdown-item" href="{{route('download')}}">Download</a>
                        <a class="dropdown-item" href="{{route('pengurus')}}">Pengurus</a>
                        <a class="dropdown-item" href="{{route('keanggotaan')}}">Anggota</a>
                        <a class="dropdown-item" href="{{route('profil')}}">Profil</a>

                    </div>
                </li>
                <a href="{{route('login')}}" class="nav-item nav-link">Login</a>
            </div>
        </div>
    </div>
</div>
@yield('content')
@include('backend.halaman_utama.layout.footer')
