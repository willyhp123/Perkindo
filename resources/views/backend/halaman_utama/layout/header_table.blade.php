
<!DOCTYPE html>
<html>

<head>
    <link rel="icon" href="{{asset('halaman_utama/images/logotitle.png')}}" type="image" size=96x96;>
    <title>@yield('title')</title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link type="text/css" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' rel='stylesheet'>
    <link type="text/css" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' rel='stylesheet'>
    <link type="text/css" href='https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css'
          rel='stylesheet'>
    <link type="text/css" href='https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css'
          rel='stylesheet'>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="{{asset('halaman_utama/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/templatemo-digital-trend.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/font_awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('halaman_utama/css/templatemo-digital-trend.css')}}">

</head>
<body>
<div class="navbar navbar-expand-lg bg-light navbar-light" style="margin-bottom: -20px">
    <div class="container-fluid">
        <a href="/" class="navbar-brand"> <img src="{{asset('halaman_utama/images/logo.png')}}"></img> PERKINDO
            <h6>persatuan konsultan indonesia</h6>
        </a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
            <div class="navbar-nav ml-auto">
                <a href="{{url('/')}}" class="nav-item nav-link ">Beranda</a>
                <a href="{{route('berita')}}" class="nav-item nav-link ">Berita</a>
                <a href="{{route('agenda')}}" class="nav-item nav-link ">Agenda</a>
                <a href="{{route('download')}}" class="nav-item nav-link ">Download</a>
                <a href="{{route('keanggotaan')}}" class="nav-item nav-link ">Keanggotaan</a>
                <a href="{{route('pengurus')}}" class="nav-item nav-link">Pengurus</a>
                <a href="{{route('profil')}}" class="nav-item nav-link">Profil</a>
                <a href="{{route('login')}}" class="nav-item nav-link">Login</a>
            </div>
        </div>
    </div>
</div>
