@extends('backend.halaman_utama.layout.header')

@section('title','Perkindo Kalbar detail Berita')
@section('content')
<br><br>
<div class="container">
    <h2 align="center" style="font-family: 'Trebuchet MS'; color: #10125f;">Berita PERKINDO KALBAR
    </h2>
    <br>
    <H4>{{ $data['judul'] }}</H4>
    <div style="height:10px"></div>
    <img src="{{ asset('foto_berita/'.$data['foto']) }}" alt="" width="400px" height="400px">
   <div style="height: 10px"></div>
    <p>{!! $data['isi'] !!}</p>
   
   
</div>
@endsection
