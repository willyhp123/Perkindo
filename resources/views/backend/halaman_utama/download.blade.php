@include('backend.halaman_utama.layout.header_table')
@section('title','Perkindo Kalbar Agenda')
<br><br>
<div class="container">
    <h2 align="center" style="font-family: 'Trebuchet MS'; color: #10125f;">DATA ANGGOTA PERKINDO KALBAR
    </h2>
    <br>
    <form method="post">
        <div class="form-group">
            <!--Script untuk memanggil Javascript-->
            <table id="example" class="display responsive nowrap" style="width:100%" cellpadding="1000px">
                <thead>
                <tr style="color: #f15a24; background-color: #10125f;">
                    <th width="5%">No</th>
                    <th>Judul</th>
                    <th>Kategori Download</th>
                    <th>Download</th>

                </tr>
                </thead>
                <tbody style="color: #10125f;">

                    @foreach($download as $key=>$val)
                        <Tr>
                            <Td>{{$key+1}}</Td>
                            <td>{{$val->judul}}</td>
                            @foreach($kategori_download as $vad)
                                @if($val->kategori_download_id == $vad->id_kategori_download)
                            <td>{{$vad->kategori_download}}</td>
                                @endif
                                @endforeach
                            <td><a href="{{route('download_dokumens',$val->dokumen)}}"><u>{{$val->dokumen}}</u></a></td>

                        </Tr>
                     @endforeach

                </tbody>
            </table>
        </div>

    </form>
</div>
@include('backend.halaman_utama.layout.footer_table')
