@include('backend.halaman_utama.layout.header_table')
@section('title','Perkindo Kalbar Anggota')
<br><br>
<div class="container">
    <h2 align="center" style="font-family: 'Trebuchet MS'; color: #10125f;">DATA ANGGOTA PERKINDO KALBAR
    </h2>
    <br>
    <form method="post">
        <div class="form-group">
            <!--Script untuk memanggil Javascript-->
            <table id="example" class="display responsive nowrap" style="width:100%" cellpadding="1000px">
                <thead>
                <tr style="color: #f15a24; background-color: #10125f;">
                    <th width="5%">No</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>No KTP</th>
                    <Th>Aksi</Th>
                </tr>
                </thead>
                <tbody style="color: #10125f;">
                @foreach($pengurus as $key=>$val)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$val->nik}}</td>
                        <Td>{{$val->nama}}</Td>
                        <td>{{$val->no_ktp}}</td>
                        <td>
                            <a class="btn btn-secondary btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_pengurus}}'> detail</a>
                            <div class="modal fade" id="m_edit-{{$val->id_pengurus}}">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class ="modal-header"></div>
                                        <div class ="modal-body">
                                            <div class ="row">
                                                <div class ="col-4">
                                                    <img src = "{{asset('foto_pengurus/'.$val->foto)}}" width="200px" height="200px">
                                                </div>
                                                <div class ="col-8">
                                                    <div class ="row">
                                                        <div class ="col-3 ">
                                                            <p><b>NIK</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            {{$val->nik}}
                                                        </div>
                                                        <div class ="col-3">
                                                           <p><b>No KTP</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            {{$val->no_ktp}}
                                                        </div>
                                                        <div class ="col-3 ">
                                                            <p><b>Nama</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            {{$val->nama}}
                                                        </div>
                                                        <div class ="col-3">
                                                            <p><b>Tempat Lahir</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            {{$val->tempat_lahir}}
                                                        </div>
                                                        <div class ="col-3">
                                                            <p><b>Tanggal Lahir</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            {{$val->tanggal_lahir}}
                                                        </div>
                                                        <div class ="col-3">
                                                            <p><b>Jenis Kelamin</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            {{$val->jenis_kelamin}}
                                                        </div>
                                                        <div class ="col-3">
                                                            <p><b>Alamat</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            {{$val->alamat}}
                                                        </div>
                                                        <div class ="col-3">
                                                            <p><b>No hp 1</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            {{$val->no_hp_1}}
                                                        </div>
                                                        <div class ="col-3">
                                                            <p><b>No hp 2</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            {{$val->no_hp_2}}
                                                        </div>
                                                        <div class ="col-3">
                                                            <p><b>Agama</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            @foreach($agama as $vaa)
                                                                @if($vaa->id_agama == $val->agama_id)
                                                                    {{$vaa->agama}}
                                                                @endif
                                                            @endforeach

                                                        </div>
                                                        <div class ="col-3">
                                                            <p><b>Jabatan</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                            @foreach($jabatan as $vaj)
                                                                @if($val->jabatan_id == $vaj->id_jabatan)
                                                                    {{$vaj->jabatan}}
                                                                 @endif
                                                             @endforeach
                                                        </div>
                                                        <div class ="col-3">
                                                            <p><b>Email</b></p>
                                                        </div>
                                                        <div class ="col-1">:</div>
                                                        <div class ="col-8">
                                                           {{$val->email}}
                                                        </div>

                                                    </div>
                                                </div>

                                        </div>

                                       </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </td>

                @endforeach
                </tbody>
            </table>
        </div>

    </form>
</div>
@include('backend.halaman_utama.layout.footer_table')
