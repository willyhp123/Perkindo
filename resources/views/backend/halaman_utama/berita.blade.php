@include('backend.halaman_utama.layout.header_table')
@section('title','Perkindo Kalbar Agenda')
<br><br>
<div class="container">
    <h2 align="center" style="font-family: 'Trebuchet MS'; color: #10125f;">DATA ANGGOTA PERKINDO KALBAR
    </h2>
    <br>
    @foreach($berita as $val)
            <!--Script untuk memanggil Javascript-->
            <div class ="row">
                <div class ="col-3 mr-5">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="{{asset('foto_berita/'.$val->foto)}}" width="100px" height="300px" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$val->judul}}</h5>
                            <p class="card-text" style="font-size: 13px;">{!!Str::limit($val->isi,25)!!}</p>
                            <div class ="row">
                                <div class ="col-6">
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_berita}}'>Berita</a>
                                    <div class="modal fade" id="m_edit-{{$val->id_berita}}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title ">{{$val->judul}}</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                    <div class="modal-body">
                                                        <center><img src ="{{asset('foto_berita/'.$val->foto)}}" width="300px" height="300px"></center>
                                                        <div style="height: 10px;"></div>
                                                        <p style="font-size: 15px;">{!! $val->isi !!}</p>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class ="col-6">
                                    <div style="height: 10px"></div>
                                   <p style ="font-size:13px">{{$val->created_at->diffForHumans()}}</p>
                                </div>
                            </div>

                        </div>
                </div>

            </div>


    @endforeach
</div>
@include('backend.halaman_utama.layout.footer_table')
