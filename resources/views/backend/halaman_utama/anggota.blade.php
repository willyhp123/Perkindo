@include('backend.halaman_utama.layout.header_table')
@section('title','Perkindo Kalbar Anggota')
<br><br>
<div class="container">
    <h2 align="center" style="font-family: 'Trebuchet MS'; color: #10125f;">DATA ANGGOTA PERKINDO KALBAR
    </h2>
    <br>
    <form method="post">
        <div class="form-group">
            <!--Script untuk memanggil Javascript-->
            <table id="example" class="display responsive nowrap" style="width:100%" cellpadding="1000px">
                <thead>
                <tr style="color: #f15a24; background-color: #10125f;">
                    <th width="5%">No</th>
                    <th>Nomor Keanggotaan</th>
                    <th>Nama Perusahaan</th>
                    <th>Nama Penanggung Jawab</th>
                    <th>Alamat</th>
                    <th>Provinsi</th>
                    <th>Kota/Kabupaten</th>
                </tr>
                </thead>
                <tbody style="color: #10125f;">
                    @foreach($keanggotaan as $key=>$val)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$val->nomor_keanggotaan}}</td>
                            <Td>{{$val->nama_perusahaan}}</Td>
                            <td>{{$val->nama_penanggung_jawab}}</td>
                            <td>{{$val->alamat_perusahaan}}</td>
                            @foreach($provinsi as $vap)
                                @if($val->provinsi_id == $vap->id_provinsi)
                                <Td>{{$vap->provinsi}}</Td>
                                @endif
                                @endforeach
                           @foreach($kota_kabupaten as $vak)
                               @if($val->kota_kabupaten_id == $vak->id_kota_kabupaten)
                            <td>{{$vak->kota_kabupaten}}</td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </form>
</div>
@include('backend.halaman_utama.layout.footer_table')
