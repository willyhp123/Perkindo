@include('backend.halaman_utama.layout.header_table')
@section('title','Perkindo Kalbar Profil')
<br><br>
<div class="container">
    <h2 align="center" style="font-family: 'Trebuchet MS'; color: #10125f;">PROFIL PERKINDO KALBAR
    </h2>
    <br>
    <H4>Sejarah Perkindo Kalbar</H4>
    <p>{!! $datas['profil'] !!}</p>
    <H4>Kontak Perkindo</H4>
    <p>{!! $datas['profil'] !!}</p>
    <h4>Lokasi Perkindo</h4>
    <img src="{{ asset('foto_lokasi_profil/'.$datas['foto_lokasi']) }}" width="200px" height="200px" alt="">
   
</div>
@include('backend.halaman_utama.layout.footer_table')
