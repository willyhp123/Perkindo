@extends('backend.login.layout.header')
@section('content')

<form  method="post" action="{{ route('proses_login') }}" class="login100-form validate-form">
<span class="login100-form-title p-b-49">
<img src="{{asset('halaman_utama/images/logo1.png')}}" width="200px;" height="150px;">
</span>
@if(Session::get('success'))
<div class ="alert alert-success">
     {{ Session::get('success') }}       
 </div>
@endif
@if(Session::get('fail'))
<div class ="alert alert-danger">
  <p style="font-size:14px;">{{ Session::get('fail') }} </p>    
</div>
@endif
    <div class="wrap-input100 validate-input m-b-23" data-validate="Username is reauired">
        <span class="label-input100">Email</span>
      
        @csrf
                    <input class="input100" type="email" name="email" placeholder="Type your Email">
                    <span class="focus-input100" data-symbol="&#xf206;"></span>
                </div>
                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <span class="label-input100">Password</span>
                    <input class="input100" type="password" name="password" placeholder="Type your password">
                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                </div>
               
                <div class="container-login100-form-btn mt-2">
                    <div class="wrap-login100-form-btn mb-2">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" class="login100-form-btn">
                            Login
                        </button>
                    </div>
                </div>
        
    </form>
    </div>
</div>
<div id="dropDownSelect1"></div>
@endsection

