@extends('backend.anggota.layout.header')
@section('title','History Pembayaran KTA')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">

                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">History Pembayaran KTA</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Rekening Tujuan</th>
                            <th>No Rekening</th>
                            <th>Atas Nama</th>
                            <Th>Keterangan</Th>
                            <th>Sudah Diproses</th>
                        
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $h_p_kta as $key=>$val)
                            @if($val->anggota_id == $datas['id_anggota'])
                            <tr>
                                <td>{{$key+1}}</td>
                                @foreach($rekening as $vao)
                                    @if($val->rekening_pembayaran_id == $vao->id_rekening_pembayaran)
                                <td>{{$vao->nama_bank}} | {{$vao->no_rek}}</td>
                                    @endif
                                @endforeach
                                <td>{{$val->no_rekening}}</td>
                                <td>{{$val->atas_nama}}</td>
                                <td>{{$val->keterangan}}</td>
                                <td>{{$val->sudah_diproses}}</td>
                            
                            </tr>
                            @endif
                         @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
