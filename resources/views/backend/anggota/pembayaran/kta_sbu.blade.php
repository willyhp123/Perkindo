@extends('backend.anggota.layout.header')
@section('title','KTA dan SBU KTA')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        @if(Session::get('success'))
                            <div class ="alert alert-success">
                                <p>{{Session::get('success')}}</p>
                            </div>
                        @endif
                            @if(Session::get('fail'))
                                <div class ="alert alert-danger">
                                    <p>{{Session::get('fail')}}</p>
                                </div>
                            @endif
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Pembayaran KTA dan SBU</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">KTA</a></li>
                        <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Sbu Konstruksi</a></li>
                        <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Sbu Non Konstruksi</a></li>
                    </ul>
                    <div class ="border mt-2"></div>
                    <div style="height: 10px;"></div>

                    <div class="tab-content">
                        <div class="active tab-pane" id="activity">
                            <p>Pembayaran KTA</p>
                            <form method="post" action="{{route('tambah_p_kta_sbu_kta')}}" enctype="multipart/form-data">
                                @csrf
                            <div class ="row">
                                <div class ="col-6">
                                    <input type ="text" name ="anggota_id" hidden value ="{{$datas['id_anggota']}}">
                                    <div class ="form-group">
                                        <label>No Rekening</label>
                                        <input type ="text" name ="no_rekening"
                                               placeholder="No Rekening" class ="form-control @error('no_rekening') is-invalid @enderror">
                                        @error('no_rekening')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>

                                    <div class ="form-group">
                                        <label>Keterangan</label>
                                        <Input type ="text" name ="keterangan"
                                               class ="form-control @error('keterangan') is-invalid @enderror" placeholder="Keterangan">
                                        @error('keterangan')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class ="col-6">
                                    <div class ="form-group">
                                        <label>Rekening Tujuan</label>
                                        <select class ="form-control @error('rekening_pembayaran_id') is-invalid @enderror" name ="rekening_pembayaran_id">
                                            <option value ="">---Rekening Tujuan---</option>
                                            @foreach($rekening as $var)
                                                <option value ="{{$var->id_rekening_pembayaran}}">{{$var->nama_bank}} | {{$var->no_rek}}</option>
                                            @endforeach
                                        </select>
                                        @error('rekening_pembayaran_id')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class ="form-group">
                                        <label>Atas Nama</label>
                                        <input type ="text" name ="atas_nama"
                                               class ="form-control @error('atas_nama') is-invalid @enderror" placeholder="Atas Nama">
                                        @error('atas_nama')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class ="form-group">
                                        <label>Bukti Pembayaran</label><br>
                                        <button type ="button" id ="inputimage"  class ="btn btn-primary"><i class ="fa fa-upload"></i> Bukti Pembayaran</button>
                                        <div style ="height: 10px"></div>
                                        <div id ="foto"></div>
                                        <p> Filesize <i id ="filesize"></i> </p>
                                        <input type ="file" id = "idInputImage" onchange="image_foto_bukti_pembayaran(event)" name ="bukti_pembayaran" hidden>
                                    </div>

                                </div>
                            </div>
                                <button type ="submit" class ="btn btn-primary">
                                    <i class=" fa fa-paper-plane" style="padding-right: 2px;"></i>
                                    Kirim</button>
                            </form>
                        </div>
                        <div class="tab-pane" id="timeline">
                            <p>Pembayaran sbu Konstruksi</p>
                            <form method="post" action="{{route('tambah_p_kta_sbu_sbu_konstruksi')}}" enctype="multipart/form-data">
                                @csrf
                            <div class ="row">
                                <div class ="col-6">
                                    <input type ="text" name ="anggota_id" hidden value ="{{$datas['id_anggota']}}">

                                    <div class ="form-group">
                                        <label>No Rekening</label>
                                        <input type ="text" name ="no_rekening"
                                               placeholder="No Rekening" class ="form-control @error('no_rekening') is-invalid @enderror">
                                        @error('no_rekening')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class ="form-group">
                                        <label>Keterangan</label>
                                        <Input type ="text" name ="keterangan"
                                               class ="form-control @error('keterangan') is-invalid @enderror" placeholder="Keterangan">
                                        @error('keterangan')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class ="col-6">
                                    <div class ="form-group">
                                        <label>Rekening Tujuan</label>
                                        <select class ="form-control @error('rekening_pembayaran_id') is-invalid @enderror" name ="rekening_pembayaran_id">
                                            <option value ="">---Rekening Tujuan---</option>
                                            @foreach($rekening as $var)
                                                <option value ="{{$var->id_rekening_pembayaran}}">{{$var->nama_bank}} | {{$var->no_rek}}</option>
                                            @endforeach
                                        </select>
                                        @error('rekening_pembayaran_id')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class ="form-group">
                                        <label>Atas Nama</label>
                                        <input type ="text" name ="atas_nama"
                                               class ="form-control @error('atas_nama') is-invalid @enderror" placeholder="Atas Nama">
                                        @error('atas_nama')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class ="form-group">
                                        <label>Bukti Pembayaran</label><br>
                                        <button type ="button" id ="inputimage_sbu_konstruksi"  class ="btn btn-primary"><i class ="fa fa-upload"></i> Bukti Pembayaran</button>
                                        <div style ="height: 10px"></div>
                                        <div id ="foto_sbu_konstruksi"></div>
                                        <p> Filesize <i id ="filesize_sbu_konstruksi"></i> </p>
                                        <input type ="file" id = "idInputImage_sbu_konstruksi" onchange="image_foto_bukti_pembayaran_sbu_konstruksi(event)" name ="bukti_pembayaran" hidden>
                                    </div>

                                </div>
                            </div>
                                <button type ="submit" class ="btn btn-primary">
                                    <i class=" fa fa-paper-plane" style="padding-right: 2px;"></i>
                                    Kirim</button>
                            </form>
                        </div>
                        <div class="tab-pane" id="settings">
                            <form method="post" action="{{route('tambah_p_kta_sbu_sbu_non_konstruksi')}}" enctype="multipart/form-data">
                                @csrf
                            <p> Pembayaran Sbu non Konstruksi</p>
                            <div class ="row">
                                <div class ="col-6">
                                    <input type ="text" name ="anggota_id" hidden value ="{{$datas['id_anggota']}}">
                                    <div class ="form-group">
                                        <label>No Rekening</label>
                                        <input type ="text" name ="no_rekening"
                                               placeholder="No Rekening" class ="form-control @error('no_rekening') is-invalid @enderror">
                                        @error('no_rekening')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class ="form-group">
                                        <label>Keterangan</label>
                                        <Input type ="text" name ="keterangan"
                                               class ="form-control @error('keterangan') is-invalid @enderror" placeholder="Keterangan">
                                        @error('keterangan')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class ="col-6">
                                    <div class ="form-group">
                                        <label>Rekening Tujuan</label>
                                        <select class ="form-control @error('rekening_pembayaran_id') is-invalid @enderror" name ="rekening_pembayaran_id">
                                            <option value ="">---Rekening Tujuan---</option>
                                            @foreach($rekening as $var)
                                                <option value ="{{$var->id_rekening_pembayaran}}">{{$var->nama_bank}} | {{$var->no_rek}}</option>
                                            @endforeach
                                        </select>
                                        @error('rekening_pembayaran_id')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class ="form-group">
                                        <label>Atas Nama</label>
                                        <input type ="text" name ="atas_nama"
                                               class ="form-control @error('atas_nama') is-invalid @enderror" placeholder="Atas Nama">
                                        @error('atas_nama')
                                        <span class ="invalid-feedback">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class ="form-group">
                                        <label>Bukti Pembayaran</label><br>
                                        <button type ="button" id ="inputimage_sbu_non_konstruksi"  class ="btn btn-primary"><i class ="fa fa-upload"></i> Bukti Pembayaran</button>
                                        <div style ="height: 10px"></div>
                                        <div id ="foto_sbu_non_konstruksi"></div>
                                        <p> Filesize <i id ="filesize_sbu_non_konstruksi"></i> </p>
                                        <input type ="file" id = "idInputImage_sbu_non_konstruksi" onchange="image_foto_bukti_pembayaran_sbu_non_konstruksi(event)" name ="bukti_pembayaran" hidden>
                                    </div>

                                </div>
                            </div>
                            <button type ="submit" class ="btn btn-primary">
                                <i class=" fa fa-paper-plane" style="padding-right: 2px;"></i>
                                Kirim</button>
                            </form>
                        </div>

                    </div>


            </div>

            </div>

            <!-- /.card -->
        </section>


        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
        let button = document.getElementById('inputimage'),
            image = document.getElementById('idInputImage'),
            filesize = document.getElementById('filesize'),
            filesize_sbu_konstruksi = document.getElementById('filesize_sbu_konstruksi'),
            image_sbu_konstruksi = document.getElementById('idInputImage_sbu_konstruksi'),
            button_sbu_konstruksi = document.getElementById('inputimage_sbu_konstruksi'),
            filesize_sbu_non_konstruksi= document.getElementById('filesize_sbu_non_konstruksi'),
            button_sbu_non_konstruksi = document.getElementById('inputimage_sbu_non_konstruksi'),
            image_sbu_non_konstruksi = document.getElementById('idInputImage_sbu_non_konstruksi')
        button.onclick=()=>{
            image.click();
        }
        button_sbu_konstruksi.onclick=()=>{
            image_sbu_konstruksi.click();
        }
        button_sbu_non_konstruksi.onclick=()=>{
            image_sbu_non_konstruksi.click();
        }
        function image_foto_bukti_pembayaran(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }

        function  updatesize(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];

                }
                filesize.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize());
        function  updatesize_sbu_konstruksi(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image_sbu_konstruksi.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];

                }
                filesize_sbu_konstruksi.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize_sbu_konstruksi());

        function image_foto_bukti_pembayaran_sbu_konstruksi(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto_sbu_konstruksi');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
        function image_foto_bukti_pembayaran_sbu_non_konstruksi(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto_sbu_non_konstruksi');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
        function  updatesize_sbu_non_konstruksi(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image_sbu_non_konstruksi.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];

                }
                filesize_sbu_non_konstruksi.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize_sbu_non_konstruksi());
    </script>
@endsection
