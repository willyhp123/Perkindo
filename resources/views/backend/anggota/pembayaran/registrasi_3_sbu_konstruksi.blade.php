@extends('backend.anggota.layout.header')
@section('title','KTA dan SBU KTA')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        @if(Session::get('success'))
                            <div class ="alert alert-success">
                                <p>{{Session::get('success')}}</p>
                            </div>
                        @endif
                        @if(Session::get('fail'))
                            <div class ="alert alert-danger">
                                <p>{{Session::get('fail')}}</p>
                            </div>
                        @endif
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Pembayaran Registrasi #3 Sbu Konstruksi</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{route('tambah_r3_sbu_konstruksi')}}" enctype="multipart/form-data">
                        @csrf
                    <div class ="row">
                        <div class ="col-6">
                            <input type ="text" name ="anggota_id" hidden value ="{{$datas['id_anggota']}}">
                            <div class ="form-group">
                                <label>No Rekening</label>
                                <input type ="text" name ="no_rekening"
                                       placeholder="No Rekening"
                                       class ="form-control @error('no_rekening') is-invalid @enderror">
                                @error('no_rekening')
                                    <span class ="invalid-feedback">{{$message}}</span>
                                @enderror
                            </div>
                            <div class ="form-group">
                                <label>Keterangan</label>
                                <Input type ="text" name ="keterangan"
                                       class ="form-control @error('keterangan') is-invalid @enderror"
                                       placeholder="Keterangan">
                                @error('keterangan')
                                    <span class ="@error('keterangan') invalid-feedback @enderror">{{$message}}</span>
                                @enderror
                            </div>
                            <div class ="form-group">
                                <label>Bukti Pembayaran</label><br>
                                <input type ="file"
                                       class ="@error('bukti_pembayaran') is-invalid @enderror"
                                       name ="bukti_pembayaran">
                                @error('bukti_pembayaran')
                                    <span class ="invalid-feedback">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class ="col-6">
                            <div class ="form-group">
                                <label>Rekening Tujuan</label>
                                <select class ="form-control @error('rekening_pembayaran_id') @enderror" name ="rekening_pembayaran_id">
                                    <option value ="">---Rekening Tujuan---</option>
                                    @foreach($rekening as $vag)
                                     <option value ="{{$vag->id_rekening_pembayaran}}">{{$vag->nama_bank}} | {{$vag->no_rek}}</option>
                                    @endforeach
                                </select>
                                @error('rekening_pembayaran_id')
                                    <span class ="invalid-feedback">{{$message}}</span>
                                @enderror
                            </div>
                            <div class ="form-group">
                                <label>Atas Nama</label>
                                <input type ="text" name ="atas_nama"
                                       class ="form-control @error('atas_nama') is-invalid @enderror" placeholder="Atas Nama">
                                @error('atas_nama')
                                    <span class ="invalid-feedback">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                <div class ="card-footer">
                    <button type ="submit" class ="btn btn-primary"><i class=" fa fa-paper-plane" style="padding-right: 2px;"></i>  Kirim</button>
                </div>

                    </form>
                </div>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->
@endsection
