@extends('backend.anggota.layout.header')

@section('title','Anggota Sbu Non Konstruksi')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">

                <div class ="row">
                    <div class ="col-6">


                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Sbu Non Konstruksi</li>
                            </ol>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>No Seri Formulir</th>
                            <th>tanggal Dikeluarkan SBU</th>
                            <th>PJ Operasional</th>
                            <th>foto</th>
                        </tr>
                        <tbody>
                            @foreach ($anggota_sbu_non_konstruksi as $key=>$val)
                                @if($datas['id_anggota'] == $val->anggota_id)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{$val->no_seri_formulir  }}</td>
                                    <td>{{$val->tanggal_dikeluarkan_sbu  }}</td>
                                    <td>{{$val->pj_operasional  }}</td>
                                    <td><img src="{{asset('foto_sbu_non_konstruksi/'.$val->file)  }}"
                                         alt="" width="150px" height="150px"></td>

                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
