<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="icon" href="{{asset('halaman_utama/images/logotitle.png')}}" type="image" size=96x96;>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <style>
        li{
            font-size: 15px;
        }
        p{
            font-style: italic;
            font-weight: normal;
            font-family: "Nunito", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" ;
        }
    </style>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">

    <link rel="stylesheet" href="{{asset('adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/summernote/summernote-bs4.min.css')}}">
    <!-- CodeMirror -->
    <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/toastr/toastr.min.css')}}">
</head>
<!------------Modal Logout--------->
<div class="modal fade" id="modal-default_logout">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Logout Akun {{$datas['email']}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h2>Apa Anda Yakin?</h2>
            </div>
            <form method="post" action="{{route('logout')}}">
                @csrf
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger"><i class ="fa fa-power-off"></i> Logout </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">

            <li class="nav-item dropdown user-menu">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="user-image img-circle elevation-2" alt="User Image">
                    <span class="d-none d-md-inline">{{$datas['email']}}</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <!-- User image -->
                    <li class="user-header bg-primary">
                        <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">

                        <p>
                            {{$datas['email']}}
                            <small>Member since Nov. 2012</small>
                        </p>
                    </li>
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="row">
                            <div class="col-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </div>
                        <!-- /.row -->
                    </li>
                    <!-- Menu Footer-->
                    <form method="post" action="{{route('logout')}}">
                        @csrf
                        <li class="user-footer">
                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                            <button type="button" class="btn btn-outline-danger btn-flat float-right" data-toggle="modal" data-target="#modal-default_logout">
                                <i class ="fa fa-power-off"></i> Logout
                            </button>


                        </li>
                    </form>
                </ul>
            </li>

        </ul>
    </nav>
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">Perkindo Kalbar</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-user" style="color: cyan;"></i>
                            <p>
                                History Pembayaran
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('anggota_h_p_kta')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan"></i>
                                    <p>KTA</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('anggota_h_p_registrasi_sbu_konstruksi')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan"></i>
                                    <p>R SBU Konstruksi </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('anggota_h_p_sbu_konstruksi')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan"></i>
                                    <p>SBU Konstruksi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('anggota_h_p_sbu_non_konstruksi')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan"></i>
                                    <p>SBU Non Konstruksi</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-cogs" style="color: cyan"></i>
                            <p>
                                KTA DAN SBU
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('anggota_kta_sbu_kta')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color:cyan;"></i>
                                    <p>KTA</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('anggota_sbu_konstruksi')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color:cyan;"></i>
                                    <p>SBU Konstruksi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('anggota_sbu_non_konstruksi')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color:cyan"></i>
                                    <p>SBU Non Konstruksi</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            <i class="nav-icon fas fa-book" style="color: cyan;"></i>
                            <p>
                                Pembayaran
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('anggota_p_kta_sbu')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan;"></i>
                                    <p>KTA dan SBU</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('anggota_registrasi_2_sbu_konstruksi')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan;"></i>
                                    <p>R #2 SBU Konstruksi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('anggota_registrasi_3_sbu_konstruksi')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon" style="color: cyan;"></i>
                                    <p>R #3 Non Konstruksi</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('anggota_ubah_user_pengguna')}}" class="nav-link">
                            <i class="nav-icon far fa-image" style="color:cyan"></i>
                            <p>
                                User Pengguna
                            </p>
                        </a>
                    </li>
                </ul>

            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
@yield('content')
@include('backend.anggota.layout.footer')
