@extends('backend.anggota.layout.header')

@section('title','User Pengguna')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class ="row">
                <div class ="col-6">
                  
                </div>
                <div class ="col-6">
                    <div class="card-tools">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Home</li>
                            <li class="breadcrumb-item active">Pembayaran KTA dan SBU</li>
                        </ol>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class ="row">
                    <div class="col-6">
                        <form method="POST" action="{{ route('update_ubah_user_pengguna',$datas['id_anggota']) }}">
                            @csrf
                           
                            <div class="form-group">
                                <label for="">Password lama</label>
                                <input type="password" name ="password_lama" class ="form-control" placeholder="Password lama">
                            </div>
                            <div class ="form-group">
                                <label for="">Password Baru</label>
                                <input type="password" name ="password_baru" class ="form-control" placeholder="passoword baru">
                            </div>
                            <button type="submit" class ="btn btn-warning"><i class="fa fa-edit"></i> Update</button>
        
                        </form>
                    </div>
                    <div class="col-6"></div>
                </div>
               
                </div>


        </div>

        </div>

        <!-- /.card -->
    </section>


    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection