@extends('backend.admin.layout.header')
@section('title','Data Perusahaan')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Perusahaan
                        </button>
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Perusahaan</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('tambah_data_perusahaan')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="row">
                                                <div class ="col-6">
                                                    <div class ="form-group">
                                                        <label>Nomor Keanggotaan</label>
                                                        <input type ="text" name ="nomor_keanggotaan" class ="form-control @error('nomor_keanggotaan') is-invalid @enderror" placeholder="Nomor Keanggotaan">
                                                        @error('nomor_keanggotaan')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Nama Perusahaan</label>
                                                        <input type ="text" name ="nama_perusahaan" class ="form-control @error('nama_perusahaan') is-invalid @enderror" placeholder="Nama Perusahaan">
                                                        @error('nama_perusahaan')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Nama Penanggung Jawab</label>
                                                        <input type ="text" name ="nama_penanggung_jawab"
                                                               class ="form-control @error('nama_penanggung_jawab') is-invalid @enderror"
                                                               placeholder="Nama Penanggung Jawab">
                                                        @error('nama_penanggung_jawab')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Alamat Perusahaan</label>
                                                        <input type ="text" name ="alamat_perusahaan" class ="form-control
                                                        @error('alamat_perusahaan') is-invalid @enderror" placeholder="Alamat Perusahaan">
                                                        @error('alamat_perusahaan')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Provinsi</label>
                                                        <select class ="form-control  @error('provinsi_id') is-invalid @enderror" name ="provinsi_id">
                                                            <option value ="">--pilih Provinsi</option>
                                                            @foreach($provinsi as $val)
                                                                <option value ="{{$val->id_provinsi}}">{{$val->provinsi}}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('provinsi_id')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Kota/Kabupaten</label>
                                                        <select class ="form-control  @error('kota_kabupaten_id') is-invalid @enderror" name ="kota_kabupaten_id">
                                                            <option value ="">--pilih Kota/Kabupaten--</option>
                                                            @foreach($kota_kabupaten as $vaj)
                                                                <option value ="{{$val->id_kota_kabupaten}}">{{$vaj->kota_kabupaten}}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('kota_kabupaten_id')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Telepon</label>
                                                        <input type ="text" name ="telepon_telex_fax" class ="form-control  @error('telepon_telex_fax') is-invalid @enderror" placeholder="Telepon/fax/telex">
                                                        @error('telepon_telex_fax')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class ="col-6">
                                                    <div class ="form-group">
                                                        <label>No HP 1</label>
                                                        <input type ="text" name ="no_hp_1" class ="form-control  @error('no_hp_1') is-invalid @enderror" placeholder="Nomor HP 1">
                                                        @error('no_hp_1')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>No HP 2</label>
                                                        <input type ="text" name ="no_hp_2" class ="form-control @error('no_hp_2') is-invalid @enderror" placeholder="Nomor HP 2">
                                                        @error('no_hp_2')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Email</label>
                                                        <input type ="email" name ="email" class ="form-control  @error('email') is-invalid @enderror" placeholder="Email">
                                                        @error('email')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Foto Penanggung Jawab</label>
                                                        <div id ="foto"></div>
                                                        <div style ="height: 10px;"></div>
                                                        <p>FileSize  <span id ="filesize"> </span> </p>
                                                        <button type  ="button" id ="inputimage" class ="btn btn-primary"><i class ="fa fa-upload"></i> Upload</button>
                                                        <input type ="file" onchange ="image_foto_anggota(event)" id ="idInputImage"  hidden name ="foto_penanggung_jawab">
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>KTP Berlaku Sampai</label>
                                                        <input type ="date" name ="kta_sampai" class ="form-control  @error('email') is-invalid @enderror">
                                                        @error('kta_sampai')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>KTA</label><br>
                                                        <button type ="button" id ="inputimage_kta" class ="btn btn-primary">
                                                            <i class ="fa fa-upload"></i> Upload
                                                        </button>
                                                        <div style = "height: 10px"></div>
                                                        <div id ="foto_kta"></div>

                                                        <p> Filesize <span id ="filesize_kta"></span></p>
                                                        <input type ="file" onchange="image_foto_kta(event)" id="idInputImage_kta" hidden name ="foto_kta">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Data Perusahaan</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nomor Keanggotaan</th>
                            <th>Nama Perusahaan</th>
                            <th>Nama Penanggung Jawab</th>
                            <th width="20%">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($anggota as $key=>$val)
                            <tr>
                                <Td>{{$key+1}}</Td>
                                <td>{{$val->nomor_keanggotaan}}</td>
                                <td>{{$val->nama_perusahaan}}</td>
                                <td>{{$val->nama_penanggung_jawab}}</td>
                                <td>
                                    <a class="btn btn-secondary btn-sm" data-toggle="modal" href='#m_detail-{{$val->id_anggota}}'><i class="fa fa-eye"></i></a>
                                    <div class="modal fade" id="m_detail-{{$val->id_anggota}}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Detail Data Anggota</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                    <div class="modal-body">
                                                       <div class ="row">
                                                           <div class ="col-4">
                                                               <div class ="form-group">
                                                                   <label>Foto Penanggung Jawab</label>
                                                                   <img src = "{{asset('foto_perusahaan/foto_penanggung_jawab/'
                                                                .$val->foto_penanggung_jawab)}}"
                                                                        width="200px" height="200px"/>
                                                               </div>
                                                               <div class ="form-group">
                                                                   <label>Foto Kta</label>
                                                                   <img src = "{{asset('foto_perusahaan/foto_kta/'
                                                                .$val->foto_kta)}}" width="200px" height="200px"/>
                                                               </div>

                                                           </div>
                                                           <div class ="col-4">
                                                               <div class ="form-group">
                                                                   <label>Nomor Keanggotaan</label>
                                                                   <p>{{$val->nomor_keanggotaan}}</p>
                                                               </div>
                                                               <div class ="form-group">
                                                                   <label>Nama Perusahaan</label>
                                                                   <p>{{$val->nama_perusahaan}}</p>
                                                               </div>
                                                               <div class ="form-group">
                                                                   <label>Nama Penanggung Jawab</label>
                                                                   <p>{{$val->nama_penanggung_jawab}}</p>
                                                               </div>
                                                               <div class ="form-group">
                                                                   <label>Alamat Perusahaan</label>
                                                                   <p>{{$val->alamat_perusahaan}}</p>
                                                               </div>
                                                               <div class ="form-group">
                                                                   <label>Provinsi</label>
                                                                   @foreach($provinsi as $vac)
                                                                       @if($val->provinsi_id == $vac->id_provinsi)
                                                                   <p>{{$vac->provinsi}}</p>
                                                                       @endif
                                                                   @endforeach
                                                               </div>
                                                               <div class ="form-group">
                                                                   <label>Kota Kabupaten</label>
                                                                   @foreach($kota_kabupaten as $vaz)
                                                                       @if($val->kota_kabupaten_id == $vaz->id_kota_kabupaten)
                                                                           <p>{{$vaz->kota_kabupaten}}</p>
                                                                       @endif
                                                                   @endforeach
                                                               </div>
                                                           </div>
                                                           <div class ="col-4">
                                                               <div class ="form-group">
                                                                   <label>Telepon telex fax</label>
                                                                   <p>{{$val->telepon_telex_fax}}</p>
                                                               </div>
                                                               <div class ="form-group">
                                                                   <label>No Hp 1</label>
                                                                   <p>{{$val->no_hp_1}}</p>
                                                               </div>
                                                               <div class ="form-group">
                                                                   <label>No Hp 2</label>
                                                                   <p>{{$val->no_hp_2}}</p>
                                                               </div>
                                                               <div class ="form-group">
                                                                   <label>Email</label>
                                                                   <p>{{$val->email}}</p>
                                                               </div>
                                                               <div class ="form-group">
                                                                   <label>KTA sampai</label>
                                                                   <p>{{$val->kta_sampai}}</p>
                                                               </div>
                                                           </div>
                                                       </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>

                                            </div>
                                        </div>
                                    </div>
                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_anggota}}'><i class="fa fa-edit"></i></a>
                                    <div class="modal fade" id="m_edit-{{$val->id_anggota}}">
                                        <div class="modal-dialog modal-xl">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Data Anggota</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_data_perusahaan',$val->id_anggota)}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                    <div class ="row">
                                                        <div class ="col-4">
                                                            <div class ="form-group">
                                                                <label>Foto Penanggung Jawab</label>
                                                                <img src ="{{asset('foto_perusahaan/foto_penanggung_jawab/'.$val->foto_penanggung_jawab)}}" width="200px" height="200px"/>
                                                                <div style="height: 10px"></div>
                                                                <input type ="file" name ="foto_penanggung_jawab"/>
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Foto KTA</label><br>
                                                                <img src ="{{asset('foto_perusahaan/foto_kta/'.$val->foto_kta)}}" width="200px" height="200px">
                                                                <div style="height: 10px"></div>
                                                                <input type ="file" name ="foto_kta"/>
                                                            </div>
                                                        </div>
                                                        <div class ="col-4">
                                                            <div class ="form-group">
                                                                <label>Nomor Keanggotaan</label>
                                                                <input type ="text" name ="nomor_keanggotaan"
                                                                       class ="form-control" value ="{{$val->nomor_keanggotaan}}"
                                                                       placeholder="Nomor Keanggotaan">
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Nama Perusahaan</label>
                                                                <input type ="text" name ="nama_perusahaan"
                                                                class ="form-control" value ="{{$val->nama_perusahaan}}"
                                                                       placeholder="Nama Perusahaan"
                                                                >
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Nama Penanggung jawab</label>
                                                                <input type ="text" name ="nama_penanggung_jawab"
                                                                class ="form-control" value ="{{$val->nama_penanggung_jawab}}"
                                                                >
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>alamat Perusahaan</label>
                                                                <input type ="text" name ="alamat_perusahaan" class ="form-control"
                                                                  value ="{{$val->alamat_perusahaan}}"
                                                                >
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Provinsi</label>
                                                                <select class ="form-control" name ="provinsi_id">
                                                                    @foreach($provinsi as $vaj)
                                                                        @if($val->provinsi_id == $vaj->id_provinsi)
                                                                            <option value ="{{$vaj->id_provinsi}}">{{$vaj->provinsi}}</option>
                                                                         @endif
                                                                        <option value ="{{$vaj->id_provinsi}}">{{$vaj->provinsi}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Kota Kabupaten</label>
                                                                <select class ="form-control" name ="kota_kabupaten_id">
                                                                  @foreach($kota_kabupaten as $vak)
                                                                      @if($val->kota_kabupaten_id == $vak->id_kota_kabupaten)
                                                                          <option value ="{{$vak->id_kota_kabupaten}}">{{$vak->kota_kabupaten}}</option>
                                                                       @endif
                                                                      <option value ="{{$vak->id_kota_kabupaten}}">{{$vak->kota_kabupaten}}</option>
                                                                   @endforeach
                                                                </select>
                                                            </div>

                                                        </div>
                                                        <div class ="col-4">
                                                            <div class ="form-group">
                                                                <label>Telepon Telex Fax</label>
                                                                <input type ="text" value ="{{$val->telepon_telex_fax}}"
                                                                       name ="telepon_telex_fax" class ="form-control">
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>No Hp 1</label>
                                                                <input type="text" name ="no_hp_1" value ="{{$val->no_hp_1}}"
                                                                       class ="form-control" placeholder="No Hp 1"/>
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>No Hp 2</label>
                                                                <input type ="text" name ="no_hp_2" value ="{{$val->no_hp_2}}"
                                                                    class ="form-control" placeholder="No Hp 2">
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Email</label>
                                                                <input type ="text" name ="email" value ="{{$val->email}}"
                                                                    class ="form-control" placeholder="Email"
                                                                >
                                                            </div>
                                                                <div class ="form-group">
                                                                    <label>KTA Sampai</label>
                                                                    <input type ="text" name ="kta_sampai"  value ="{{$val->kta_sampai}}"
                                                                    class ="form-control" placeholder="KTA Sampai"
                                                                    >
                                                                </div>
                                                        </div>
                                                    </div>

                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_anggota}}">
                                        <i class = "fa fa-trash"></i>
                                    </button>
                                    <div class="modal fade" id="del-{{$val->id_anggota}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data {{$val->nomor_keanggotaan}}</h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action ="{{route('delete_data_perusahaan',$val->id_anggota)}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class ="form-group">
                                                            <H3>Apa Anda Yakin?</H3>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type ="submit" class = "btn btn-danger"><i class = "fa fa-trash"></i>Hapus</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
        let image = document.getElementById('idInputImage'),
            button  = document.getElementById('inputimage'),
            image_kta = document.getElementById('idInputImage_kta'),
            button_kta = document.getElementById('inputimage_kta'),
            filesize_kta = document.getElementById('filesize_kta')
            filesize = document.getElementById('filesize');
        button.onclick = () =>{
            image.click();
        }
        button_kta.onclick = () =>{
            image_kta.click();
        }
        function  updatesize_kta(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image_kta.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];
                }
                filesize_kta.innerHTML = output;
            },1000)
        }
        image_kta.addEventListener("change",updatesize_kta());
        function  updatesize(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];
                }
                filesize.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize());
        function image_foto_anggota(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
        function image_foto_kta(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto_kta');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
    </script>
@endsection
