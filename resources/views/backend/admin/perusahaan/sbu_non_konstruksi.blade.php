@extends('backend.admin.layout.header')
@section('title','Sbu Non Konstruksi')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Sbu Konstruksi
                        </button>
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Sbu Non Konstruksi</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('tambah_sbu_non_konstruksi')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>No Seri Formulir</label>
                                                <input type ="text" name ="no_seri_formulir" class ="form-control" placeholder="No Seri Formulir">
                                            </div>
                                            <div class ="form-group">
                                                <label>Anggota</label>
                                                <select class ="form-control" name ="anggota_id">
                                                    <option value ="">--pilih Perusahaan--</option>
                                                    @foreach($anggota as $vab)
                                                        @if($vab->level == "anggota")
                                                            <option value ="{{$vab->id_anggota}}">{{$vab->nama_perusahaan}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class ="form-group">
                                                <label>Tanggal dikeluarkan sbu</label>
                                                <input type ="date" name ="tanggal_dikeluarkan_sbu" class ="form-control">
                                            </div>
                                            <div class ="form-group">
                                                <label>Pj Operasional</label>
                                                <input type ="text" placeholder="PJ Operasional" name ="pj_operasional" class ="form-control">
                                            </div>
                                            <div class ="form-group">
                                                <label>File</label><br>
                                                <button type ="button" id ="inputimage" class ="btn btn-primary">
                                                    <i class ="fa fa-upload"></i> Upload
                                                </button>
                                                <div style="height: 10px"></div>
                                                <div id ="foto"></div>
                                                <p> Filesize <span id ="filesize"></span></p>
                                                <input type ="file" hidden id ="idInputImage" onchange="image_foto_sbu_non_konstruksi(event)" name ="file">
                                            </div>

                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Sbu Konstruksi</li>
                            </ol>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>No seri Formulir</th>
                            <th>Nomor Keanggotaan</th>
                            <th>Tanggal Dikeluarkan SBU</th>
                            <th>PJ Operasional</th>
                            <th>Foto</th>
                            <th width="20%">Aksi</th>
                        </tr>
                        </thead>
                      <tbody>
                      @foreach($sbu_non_konstruksi as $key=>$val)
                          <tr>
                              <Td>{{$key+1}}</Td>
                                     <td>{{$val->no_seri_formulir}}</td>

                              <td>
                                  @foreach($anggota as $vap)
                                      @if($val->anggota_id == $vap->id_anggota)
                                          {{$vap->nomor_keanggotaan}}
                                          <a class="btn btn-secondary btn-sm" data-toggle="modal"
                                             href='#m_anggota-{{$val->id_sbu_non_konstruksi}}'><i class="fa fa-eye"></i></a>
                                          <div class="modal fade" id="m_anggota-{{$val->id_sbu_non_konstruksi}}">
                                              <div class="modal-dialog modal-xl">
                                                  <div class="modal-content">
                                                      <div class="modal-header">
                                                          <h4 class="modal-title">Detail Data Perusahaan</h4>
                                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                      </div>
                                                      <div class ="modal-body">
                                                          @foreach($anggota as $vab)
                                                              @if($val->anggota_id == $vab->id_anggota)
                                                          <Div class ="row">
                                                              <div class ="col-4">
                                                                  <div class ="form-group">
                                                                      <label>Foto Kta</label><br>
                                                                      <img src ="{{asset('foto_perusahaan/foto_kta/'.$vab->foto_kta)}}" width="200px" height="200px">
                                                                  </div>
                                                                  <div class ="form-group">
                                                                      <label>Foto Penanggung jawab</label>
                                                                      <img src = "{{asset('foto_perusahaan/foto_penanggung_jawab/'.$vab->foto_penanggung_jawab)}}" width="200px" height="200px">
                                                                  </div>
                                                              </div>
                                                              <div class ="col-4">
                                                                  <div class ="form-group">
                                                                      <label>Nomor Keanggotaan</label>
                                                                      <p>{{$vab->nomor_keanggotaan}}</p>
                                                                  </div>
                                                                  <div class ="form-group">
                                                                      <label>Nama Perusahaan</label>
                                                                      <p>{{$vab->nama_perusahaan}}</p>
                                                                  </div>
                                                                  <div class ="form-group">
                                                                      <label>nama Penanggung Jawab</label>
                                                                      <p>{{$vab->nama_penanggung_jawab}}</p>
                                                                  </div>
                                                                  <div class ="form-group">
                                                                      <label>Alamat Perusahaan</label>
                                                                      <p>{{$vab->alamat_perusahaan}}</p>
                                                                  </div>
                                                                  <div class ="form-group">
                                                                      <label>Provinsi</label>
                                                                      <p>
                                                                          @foreach($provinsi as $vap)
                                                                              @if($vab->provinsi_id == $vap->id_provinsi)
                                                                                  {{$vap->provinsi}}
                                                                              @endif
                                                                          @endforeach
                                                                      </p>
                                                                  </div>
                                                                  <div class ="form-group">
                                                                      <label>Kota Kabupaten</label>
                                                                      <p>
                                                                          @foreach($kota_kabupaten as $vac)
                                                                              @if($vac->id_kota_kabupaten == $vab->kota_kabupaten_id)
                                                                                  {{$vab->kabupaten_kota}}
                                                                              @endif
                                                                          @endforeach
                                                                      </p>
                                                                  </div>
                                                              </div>
                                                              <div class ="col-4">
                                                                  <div class ="form-group">
                                                                      <label>Telepon/Telex/fax</label>
                                                                      <p>{{$vab->telepon_telex_fax}}</p>
                                                                  </div>
                                                                  <div class ="form-group">
                                                                      <label>No Hp 1</label>
                                                                      <p>{{$vab->no_hp_1}}</p>
                                                                  </div>
                                                                  <div class ="form-group">
                                                                      <label>No Hp 2</label>
                                                                      <p>{{$vab->no_hp_2}}</p>
                                                                  </div>
                                                                  <div class ="form-group">
                                                                      <label>kta_sampai</label>
                                                                      <p>{{$vab->kta_sampai}}</p>
                                                                  </div>
                                                                  <div class ="form-group">
                                                                      <label>Email</label>
                                                                      <p>{{$vab->email}}</p>
                                                                  </div>
                                                              </div>
                                                          </Div>
                                                              @endif
                                                          @endforeach
                                                      </div>
                                                      <div class ="modal-footer"></div>
                                                  </div>
                                              </div>
                                          </div>
                                       @endif
                                   @endforeach
                              </td>
                              <td>{{$val->tanggal_dikeluarkan_sbu}}</td>
                               <td>{{ $val->pj_operasional }}</td>
                                 <td><img src="{{asset('foto_sbu_non_konstruksi/'.$val->file)}}" width="150px" height="150px"></td>

                              <td>
                                  <a class="btn btn-secondary btn-sm" data-toggle="modal"
                                     href='#m_detail-{{$val->id_sbu_non_konstruksi}}'><i class="fa fa-eye"></i></a>
                                  <div class="modal fade" id="m_detail-{{$val->id_sbu_non_konstruksi}}">
                                      <div class="modal-dialog ">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <h4 class="modal-title">Detail sbu Non Konstruksi</h4>
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                              </div>

                                              <div class ="modal-body">
                                                  <div class ="form-group">
                                                      <table id="example1" class="table table-bordered table-striped">
                                                        <thead>
                                                        <tr>
                                                            <Td>No</Td>
                                                            <td>Sub Klasifikasi</td>
                                                        </tr>
                                                        </thead>
                                                          <tbody>
                                                              @foreach($val->detail_sbu_non_konstruksi as $keyz=> $vab)
                                                                  <Tr>
                                                                      <Td>{{$keyz+1}}</Td>
                                                                      <td>
                                                                          @foreach($sub_klasifikasi_sbu_non_konstruksi as $vah)
                                                                              @foreach($klasifikasi_non_konstruksi as $vau)
                                                                                  @if($vau->id_k_sbu_non_konstruksi == $vah->k_sbu_non_konstruksi_id )
                                                                              @if($vab->s_k_sbu_non_konstruksi_id  ==  $vah->id_sub_k_sbu_non_k)
                                                                          {{$vah->sub_klasifikasi}} ({{$vau->klasifikasi_non_konstruksi}}) </td>
                                                                      @endif
                                                                      @endif
                                                                      @endforeach
                                                                      @endforeach
                                                                  </Tr>
                                                              @endforeach

                                                          </tbody>
                                                      </table>

                                                  </div>
                                              </div>
                                              <div class ="modal-footer"></div>
                                          </div>
                                      </div>
                                  </div>
                                  <a class="btn btn-warning btn-sm" data-toggle="modal"
                                     href='#m_edit-{{$val->id_sbu_non_konstruksi}}'><i class="fa fa-edit"></i></a>
                                  <div class="modal fade" id="m_edit-{{$val->id_sbu_non_konstruksi}}">
                                      <div class="modal-dialog">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <h4 class="modal-title">Edit Data sbu Non Konstruksi</h4>
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                              </div>
                                              <form method="post" action="{{route('update_sbu_non_konstruksi',$val->id_sbu_non_konstruksi)}}" enctype="multipart/form-data">
                                                  @csrf
                                            <div class ="modal-body">
                                                <div class ="form-group">
                                                    <label>No Seri Formulir</label>
                                                    <input type ="text" name ="no_seri_formulir"
                                                          value ="{{$val->no_seri_formulir}}" class ="form-control">
                                                </div>
                                                <div class ="form-group">
                                                    <label>tanggal Dikeluarkan Sbu</label>
                                                    <input type ="date" class ="form-control" name ="tanggal_dikeluarkan_sbu" value ="{{$val->tanggal_dikeluarkan_sbu}}">
                                                </div>
                                                <div class ="form-group">
                                                    <label>pj_operasional</label>
                                                    <input type ="text" class ="form-control" name ="pj_operasional" value ="{{$val->pj_operasional}}">
                                                </div>
                                                <div class ="form-group">
                                                    <label>File</label><br>
                                                    <img src ="{{asset('foto_sbu_non_konstruksi/'.$val->file)}}" width="200px" height="200px">
                                                    <div style="height: 10px"></div>
                                                    <input type ="file" name ="file">
                                                </div>
                                            </div>
                                                  <div class="modal-footer justify-content-between">
                                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Edit</button>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                                  <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_sbu_non_konstruksi}}">
                                      <i class = "fa fa-trash"></i>
                                  </button>
                                  <div class="modal fade" id="del-{{$val->id_sbu_non_konstruksi}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                      <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <h3>Hapus Data {{$val->no_seri_formulir}}</h3>
                                              </div>
                                              <div class = "border"></div>
                                              <form method="post" action ="{{route('delete_sbu_non_konstruksi',$val->id_sbu_non_konstruksi)}}" enctype="multipart/form-data">
                                                  @csrf
                                                  <div class="modal-body">
                                                      <div class ="form-group">
                                                          <H3>Apa Anda Yakin?</H3>
                                                      </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                      <button type ="submit" class = "btn btn-danger"><i class = "fa fa-trash"></i>Hapus</button>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                                     <a class="btn btn-primary btn-sm" data-toggle="modal"
                                     href='#m_plus-{{$val->id_sbu_non_konstruksi}}'><i class="fa fa-plus"></i></a>
                                  <div class="modal fade" id="m_plus-{{$val->id_sbu_non_konstruksi}}">
                                      <div class="modal-dialog ">
                                          <div class="modal-content">
                                              <div class="modal-header">
                                                  <h4 class="modal-title">Tambah Detail sbu Non Konstruksi</h4>
                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                              </div>
                                              <form method="post" action ="{{route('tambah_detail_sbu_non_konstruksi')}}">
                                                  @csrf
                                              <div class ="modal-body">
                                                      <div class ="form-group">
                                                            <input type="text" name="sbu_non_konstruksi_id" hidden value="{{ $val->id_sbu_non_konstruksi }}">
                                                       </div>
                                                       <div class ="form-group">
                                                         <select name ="s_k_sbu_non_konstruksi_id" class ="form-control">
                                                          @foreach ($sub_klasifikasi_sbu_non_konstruksi  as $vau)
                                                            @foreach ($klasifikasi_non_konstruksi as $vao)
                                                              @if($vau->k_sbu_non_konstruksi_id== $vao->id_k_sbu_non_konstruksi)
                                                                        <option value ="{{ $vau->id_sub_k_sbu_non_k }}">{{ $vau->sub_klasifikasi }} ({{ $vao->klasifikasi_non_konstruksi  }})</option>
                                                              @endif
                                                            @endforeach
                                                          @endforeach

                                                         </select>

                                                       </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                      <button type ="submit" class = "btn btn-primary"><i class = "fa fa-plus"></i> Tambah</button>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                       @endforeach

                      </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
        let image = document.getElementById('idInputImage'),
            button = document.getElementById('inputimage'),
            filesize = document.getElementById('filesize');
        button.onclick = () =>{
            image.click();
        }
        function  updatesize(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];
                }
                filesize.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize());
        function image_foto_sbu_non_konstruksi(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
    </script>
@endsection
