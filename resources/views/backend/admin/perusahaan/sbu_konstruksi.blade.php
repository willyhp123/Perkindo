@extends('backend.admin.layout.header')
@section('title','Sbu Konstruksi')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Sbu Non Konstruksi
                        </button>
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Sbu Konstruksi</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('tambah_sbu_konstruksi')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>No Seri Formulir</label>
                                                <input type ="text" name ="no_seri_formulir" class ="form-control @error('no_seri_formulir') is-invalid @enderror" placeholder="No Seri Formulir">
                                                @error('no_seri_formulir')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Anggota</label>
                                                <select class ="form-control  @error('anggota_id') is-invalid @enderror" name ="anggota_id">
                                                    <option value ="">--pilih Perusahaan</option>
                                                    @foreach($anggota as $vab)
                                                        @if($vab->level == "anggota")
                                                        <option value ="{{$vab->id_anggota}}">{{$vab->nama_perusahaan}}</option>
                                                    @endif
                                                            @endforeach
                                                </select>
                                                @error('anggota_id')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Tanggal Masuk</label>
                                                <input type ="date" name ="tanggal_masuk" class ="form-control @error('tanggal_masuk') is-invalid @enderror">
                                                @error('tanggal_masuk')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Berlaku Sampai</label>
                                                <input type ="date" name ="berlaku_sampai" class ="form-control @error('berlaku_sampai') is-invalid @enderror">
                                                @error('berlaku_sampai')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Registrasi Tahun ke 2</label>
                                                <input type ="date" name ="registrasi_tahun_ke_2" class ="form-control">
                                            </div>
                                            <div class ="form-group">
                                                <label>Registrasi Tahun ke 3</label>
                                                <input type ="date" name ="registrasi_tahun_ke_3" class ="form-control">
                                            </div>
                                            <div class ="form-group">
                                                <label>Tenaga Ahli</label>
                                                <input type ="text" name ="tenaga_ahli"
                                                       class ="form-control @error('tenaga_ahli') is-invalid @enderror" placeholder="Tenaga Ahli">
                                                @error('tenaga_ahli')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>file</label><br>
                                                <button type = "button" id ="inputimage" class ="btn btn-primary"><i class ="fa fa-upload"></i> Upload</button>
                                                <div style="height: 10px"></div>
                                                <div id ="foto"></div>
                                                <input type ="file" id ="idInputImage" onchange="image_foto_sbu_konstruksi(event)" hidden name ="foto" class ="@error('foto') is-invalid @enderror">
                                                <p> Filesize :<span id="filesize"></span></p>
                                                @error('foto')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Sbu Konstruksi</li>
                            </ol>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>No seri Formulir</th>
                            <th>Nomor Keanggotaan</th>
                            <th>Nama Perusahaan</th>
                            <th>Tanggal Masuk</th>
                            <th>Berlaku Sampai</th>
                            <th width="20%">Aksi</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sub_konstruksi as $key=>$val)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$val->no_seri_formulir}}</td>
                                @foreach($anggota as $vax)
                                    @if($val->anggota_id == $vax->id_anggota)
                                <td>{{$vax->nomor_keanggotaan}}
                                    <a class="btn btn-secondary btn-sm" data-toggle="modal" href='#m_detail-{{$val->id_sbu_konstruksi}}'><i class="fa fa-eye"></i></a>
                                    <div class="modal fade" id="m_detail-{{$val->id_sbu_konstruksi}}">
                                        <div class="modal-dialog modal-xl">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Detail Data Anggota</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_sbu_konstruksi',$val->id_sbu_konstruksi)}}" enctype="multipart/form-data">
                                                <div class="modal-body">
                                                    @foreach($anggota as $vaq)
                                                        @if($val->anggota_id == $vaq->id_anggota)
                                                            <div class ="row">
                                                                <div class ="col-4">
                                                                    <div class ="form-group">
                                                                        <label>Foto Penanggung Jawab</label>
                                                                        <img src = "{{asset('foto_perusahaan/foto_penanggung_jawab/'
                                                                .$vaq->foto_penanggung_jawab)}}"
                                                                             width="200px" height="200px"/>
                                                                    </div>
                                                                    <div class ="form-group">
                                                                        <label>Foto Kta</label><br>
                                                                        <img src = "{{asset('foto_perusahaan/foto_kta/'
                                                                .$vaq->foto_kta)}}" width="200px" height="200px"/>
                                                                    </div>
                                                                </div>
                                                                <div class ="col-4">
                                                                    <div class ="form-group">
                                                                        <label>Nomor Keanggotaan</label>
                                                                        <p>{{$vaq->nomor_keanggotaan}}</p>
                                                                    </div>
                                                                    <div class ="form-group">
                                                                        <label>Nama Perusahaan</label>
                                                                        <p>{{$vaq->nama_perusahaan}}</p>
                                                                    </div>
                                                                    <div class ="form-group">
                                                                        <label>Nama Penanggung Jawab</label>
                                                                        <p>{{$vaq->nama_penanggung_jawab}}</p>
                                                                    </div>
                                                                    <div class ="form-group">
                                                                        <label>Alamat Perusahaan</label>
                                                                        <p>{{$vaq->alamat_perusahaan}}</p>
                                                                    </div>
                                                                    <div class ="form-group">
                                                                        <label>Provinsi</label>
                                                                        @foreach($provinsi as $vap)
                                                                            @if($vaq->provinsi_id == $vap->id_provinsi)
                                                                                <p>{{$vap->provinsi}}</p>
                                                                            @endif
                                                                        @endforeach
                                                                    </div>
                                                                    <div class ="form-group">
                                                                        @foreach($kota_kabupaten as $vak)
                                                                            @if($vaq->kota_kabupaten_id == $vak->id_kota_kabupaten)
                                                                                <label>Kota Kabupaten</label>
                                                                                <p>{{$vak->kota_kabupaten}}</p>
                                                                            @endif
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                                <div class ="col-4">
                                                                    <div class ="form-group">
                                                                        <label>Telepon/Telex/Fax</label>
                                                                        <p>{{$vaq->telepon_telex_fax}}</p>
                                                                    </div>
                                                                    <div class ="form-group">
                                                                        <label>No Hp 1</label>
                                                                        <p>{{$vaq->no_hp_1}}</p>
                                                                    </div>
                                                                    <div class ="form-group">
                                                                        <label>No Hp 2</label>
                                                                        <p>{{$vaq->no_hp_2}}</p>
                                                                    </div>
                                                                    <div class ="form-group">
                                                                        <label>Email</label>
                                                                        <p>{{$vaq->email}}</p>
                                                                    </div>
                                                                    <div class ="form-group">
                                                                        <label>KTA Sampai</label>
                                                                        <p>{{$val->kta_sampai}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>

                                <td>{{$vax->nama_perusahaan}}}</td>
                                    @endif
                                @endforeach
                                <td>{{$val->tanggal_masuk}}</td>
                                <td>{{$val->berlaku_sampai}}</td>
                                <td>
                                    <a class="btn btn-secondary btn-sm" data-toggle="modal" href='#m_details-{{$val->id_sbu_konstruksi}}'><i class="fa fa-eye"></i></a>
                                    <div class="modal fade" id="m_details-{{$val->id_sbu_konstruksi}}">
                                        <div class="modal-dialog modal-lg ">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Detail Sbu Konstruksi</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                               <div class ="modal-body">
                                                   <table id ="example2" class="table table-bordered table-striped">
                                                       <thead>
                                                       <Tr>
                                                           <Th>No</Th>
                                                           <Th>Sub Klasifikasi sbu Konstruksi</Th>
                                                       </Tr>
                                                       </thead>
                                                       <tbody>
                                                       @foreach($val->detail_sbu_konstruksi as  $Key=>$vao)

                                                           <tr>
                                                               <td>{{$key+1}}</td>
                                                               @foreach( $sub_klasifikasi_sbu_konstruksi as $vak)
                                                                   @foreach($klasifikasi_sbu_konstruksi as $vau)
                                                                       @if($vau->id_klasifikasi_sbu_konstruksi == $vak->k_sbu_konstruksi_id )
                                                                   @if($vak->id_sub_klasifikasi_sbu_konstruksi == $vao->sub_klasifikasi_sbu_konstruksi_id)
                                                                   <td>{{$vak->sub_klasifikasi}} ({{$vau->klasifikasi}})</td>
                                                                   @endif
                                                                       @endif
                                                                   @endforeach
                                                               @endforeach

                                                           </tr>
                                                       @endforeach
                                                       </tbody>

                                                   </table>
                                               </div>

                                            </div>
                                        </div>
                                    </div>
                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_sbu_konstruksi}}'><i class="fa fa-edit"></i></a>
                                    <div class="modal fade" id="m_edit-{{$val->id_sbu_konstruksi}}">
                                        <div class="modal-dialog modal-lg ">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Sbu Konstruksi</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_sbu_konstruksi',$val->id_sbu_konstruksi)}}" enctype="multipart/form-data">
                                                    @csrf
                                                <div class="modal-body">
                                                    <Div class ="row">
                                                        <div class ="col-6">
                                                            <div class ="form-group">
                                                                <label>Foto</label><br>
                                                                <img src="{{asset('foto_sbu_konstruksi/'.$val->foto)}}"
                                                                     width="200px" height="200px">
                                                                <div style ="height: 10px"></div>
                                                                <input type ="file" name ="foto">
                                                            </div>
                                                        </div>
                                                        <div class ="col-6">
                                                            <div class ="form-group">
                                                                <label>Nomor Keanggotaan</label>
                                                                <select name ="anggota_id" class ="form-control">
                                                                    @foreach($anggota as $vaq)
                                                                        @if($val->anggota_id == $vaq->id_anggota)
                                                                            <option value ="{{$vaq->id_anggota}}">{{$vaq->nomor_keanggotaan}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>No Seri Formulir</label>
                                                                <input type ="text" name ="no_seri_formulir"
                                                                       class ="form-control" placeholder="No Seri Formulir"
                                                                       value ="{{$val->no_seri_formulir}}"
                                                                >
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Tanggal Masuk</label>
                                                                <input type ="date" name ="tanggal_masuk" value ="{{$val->tanggal_masuk}}" class="form-control">
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Berlaku sampai</label>
                                                                <input type ="date" name ="berlaku_sampai" value ="{{$val->berlaku_sampai}}" class="form-control">
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Registrasi Tahun Ke 2</label>
                                                                <input type ="date" name ="registrasi_tahun_ke_2"
                                                                       value ="{{$val->registrasi_tahun_ke_2}}" class="form-control">
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Registrasi Tahun Ke 3</label>
                                                                <input type ="date" name ="registrasi_tahun_ke_3"
                                                                       value ="{{$val->registrasi_tahun_ke_3}}"
                                                                       class="form-control">
                                                            </div>
                                                            <div class ="form-group">
                                                                <label>Tenaga Ahli</label>
                                                                <input type ="text" name ="tenaga_ahli"
                                                                       value ="{{$val->tenaga_ahli}}"
                                                                       class="form-control">
                                                            </div>
                                                        </div>
                                                    </Div>
                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_sbu_konstruksi}}">
                                        <i class = "fa fa-trash"></i>
                                    </button>
                                    <div class="modal fade" id="del-{{$val->id_sbu_konstruksi}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data {{$val->no_seri_formulir}}</h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action="{{route('delete_sbu_konstruksi',$val->id_sbu_konstruksi)}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                        Apa Anda Yakin ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type ="submit" class = "btn btn-danger"><i class = "fa fa-trash"></i>Hapus</button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" href='#m_plus-{{$val->id_sbu_konstruksi}}'><i class="fa fa-plus"></i></a>
                                    <div class="modal fade" id="m_plus-{{$val->id_sbu_konstruksi}}">
                                        <div class="modal-dialog ">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Tambah Klasifikasi sbu Konstruksi</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('tambah_detail_sbu_konstruksi')}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <input type ="text" name ="sbu_konstruksi_id" value ="{{$val->id_sbu_konstruksi}}" hidden>
                                                        <div class ="form-group">
                                                            <label>Sub Klasifikasi sbu Konstruksi</label>
                                                            <select name ="sub_klasifikasi_sbu_konstruksi_id" class ="form-control">
                                                                @foreach($sub_klasifikasi_sbu_konstruksi as $vav)
                                                                    @foreach($klasifikasi_sbu_konstruksi as $vap)
                                                                        @if($vav->k_sbu_konstruksi_id == $vap->id_klasifikasi_sbu_konstruksi)
                                                                    <option value ="{{$vav->id_sub_klasifikasi_sbu_konstruksi}} ">
                                                                        {{$vav->sub_klasifikasi}}  ({{$vap->klasifikasi}})
                                                                    </option>
                                                                        @endif
                                                                @endforeach
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
        let button = document.getElementById('inputimage'),
            image = document.getElementById('idInputImage'),
            filesize = document.getElementById('filesize');

        button.onclick = () =>{
            image.click();
        }
        function  updatesize(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];
                }
                filesize.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize());
        function image_foto_sbu_konstruksi(event){
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
    </script>
@endsection
