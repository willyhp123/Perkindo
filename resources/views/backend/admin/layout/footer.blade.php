<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        
    </div>
    <strong>2022 Perkindo Kalbar</strong> 
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">

</aside>
<!-- /.control-sidebar -->
</div>

<script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('adminlte/dist/js/demo.js')}}"></script>
<!-- DataTables  & Plugins -->
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('adminlte/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('adminlte/plugins/toastr/toastr.min.js')}}"></script>

</body>
</html>
<script>
    $("#example1").DataTable({
        "responsive": true, "autoWidth": false,
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $("#example2").DataTable({
        "responsive": true, "autoWidth": false,
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $(function () {
        // Summernote
        $('#summernote').summernote()
        $('#summernote1').summernote()
        $('#summernote_deskripsi').summernote()
        $('#summernote_visi').summernote()
        $('#summernote_misi').summernote()
        $('#summernote_sejarah').summernote()
        $('#summernote_alamat').summernote()

    })
    @if(Session::get('success'))
    toastr.success('{{Session::get('success')}}')
        @endif
        @if(Session::get('fail'))
        toastr.error('{{Session::get('fail')}}')
    @endif

@error('agama')
    toastr.error('{{$message}}')
@enderror
@error('nama_bank')
toastr.error('{{$message}}')
    @enderror
    @error('atas_nama')
    toastr.error('{{$message}}')
    @enderror
    @error('no_rek')
    toastr.error('{{$message}}')
    @enderror
    @error('jabatan')
    toastr.error('{{$message}}')
    @enderror
    @error('nomor_keanggotaa')
    toastr.error('{{$message}}')
    @enderror
    @error('nama_perusahaan')
    toastr.error('{{$message}}')
    @enderror
    @error('nama_penanggung_jawab')
    toastr.error('{{$message}}')
    @enderror
    @error('alamat_perusahaan')
    toastr.error('{{$message}}')
    @enderror
    @error('provinsi')
    toastr.error('{{$message}}')
    @enderror
    @error('kota_kabupaten')
    toastr.error('{{$message}}')
    @enderror
    @error('telepon_telex_fax')
    toastr.error('{{$message}}')
    @enderror
    @error('no_hp_1')
    toastr.error('{{$message}}')
    @enderror
    @error('no_hp_2')
    toastr.error('{{$message}}')
    @enderror
    @error('kta_sampai')
    toastr.error('{{$message}}')
    @enderror
    @error('password')
    toastr.error('{{$message}}')
    @enderror
    @error('email')
    toastr.error('{{$message}}')
    @enderror
    @error('nik')
    toastr.error('{{$message}}')
    @enderror
    @error('nama')
    toastr.error('{{$message}}')
    @enderror
    @error('tempat_lahir')
    toastr.error('{{$message}}')
    @enderror
    @error('tanggal_lahir')
    toastr.error('{{$message}}')
    @enderror
    @error('alamat')
    toastr.error('{{$message}}')
    @enderror
    @error('no_seri_formulir')
    toastr.error('{{$message}}')
    @enderror
    @error('alamat')
    toastr.error('{{$message}}')
    @enderror
    @error('tanggal_masuk')
    toastr.error('{{$message}}')
    @enderror
    @error('berlaku_sampai')
    toastr.error('{{$message}}')
    @enderror
    @error('tenaga_ahli')
    toastr.error('{{$message}}')
    @enderror
    @error('kategori_download')
    toastr.error('{{$message}}')
    @enderror
    @error('kategori_download_id')
    toastr.error('{{$message}}')
    @enderror
    @error('dokumen')
    toastr.error('{{$message}}')
    @enderror
    @error('judul')
    toastr.error('{{$message}}')
    @enderror
    @error('gambar_slide')
    toastr.error('{{$message}}')
    @enderror
    @error('url')
    toastr.error('{{$message}}')
    @enderror
    @error('isi')
    toastr.error('{{$message}}')
    @enderror
    @error('tanggal_mulai')
    toastr.error('{{$message}}')
    @enderror
    @error('tanggal_selesai')
    toastr.error('{{$message}}')
    @enderror
    @error('nama_agenda')
    toastr.error('{{$message}}')
    @enderror
    @error('provinsi_id')
    toastr.error('{{$message}}')
    @enderror
    @error('kota_kabupaten')
    toastr.error('{{$message}}')
    @enderror
    @error('pj_operasional')
    toastr.error('{{$message}}')
    @enderror
    @error('tanggal_dikeluarkan_sbu')
    toastr.error('{{$message}}')
    @enderror
    @error('file')
    toastr.error('{{$message}}')
    @enderror
    @error('klasifikasi')
    toastr.error('{{$message}}')
    @enderror
    @error('klasifikasi_non_konstruksi')
    toastr.error('{{$message}}')
    @enderror
    @error('kode')
    toastr.error('{{$message}}')
    @enderror
    @error('sub_klasifikasi')
    toastr.error('{{$message}}')
    @enderror
    @error('lingkup_pekerjaan')
    toastr.error('{{$message}}')
    @enderror
    @error('keterangan')
    toastr.error('{{$message}}')
    @enderror
    @error('k_sbu_konstruksi_id')
    toastr.error('{{$message}}')
    @enderror
    @error('k_sbu_non_konstruksi_id')
    toastr.error('{{$message}}')
    @enderror

</script>
