@extends('backend.admin.layout.header')
@section('title','Sub Klasifikasi SBU Konstruksi')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Klasifikasi
                        </button>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Klasifikasi</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('tambah_sub_klasifikasi_sbu_konstruksi')}}">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Klasifikasi SBU Konstruksi</label>
                                                <select name ="k_sbu_konstruksi_id" class ="form-control @error('s_sbu_konstruksi_id') is-invalid @enderror">
                                                    <option value ="">---Pilih Klasifikasi SBU Konstruksi---</option>
                                                    @foreach($klasifikasi as $val)
                                                    <option value ="{{$val->id_klasifikasi_sbu_konstruksi}}">{{$val->klasifikasi}}</option>
                                                    @endforeach
                                                </select>
                                                @error('k_sbu_konstruksi_id')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Kode</label>
                                                <input type ="text" name ="kode" class ="form-control @error('kode') is-invalid @enderror" placeholder="Kode">
                                                @error('kode')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Sub Klasifikasi</label>
                                                <input type ="text" name ="sub_klasifikasi" class ="form-control @error('sub_klasifikasi') is-invalid @enderror" placeholder="Sub Klasifikasi">
                                                @error('sub_klasifikasi')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Lingkup Pekerjaaan</label>
                                                <input type ="text" name ="lingkup_pekerjaan" class ="form-control @error('lingkup_pekerjaan') is-invalid @enderror" placeholder="Lingkup Pekerjaan">
                                                @error('lingkup_pekerjaan')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Keterangan</label>
                                                <textarea class ="form-control @error('keterangan') is-invalid @enderror" name ="keterangan" placeholder="Keterangan"></textarea>
                                                @error('keterangan')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Klasifikasi sbu Non Konstruksi</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Klasifikasi SBU Konstruksi</th>
                            <th>Kode</th>
                            <th>Sub Klasifikasi</th>
                            <th width="20%">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sub_klasifikasi as $key=>$val)
                            <tr>
                                <td>{{$key+1}}</td>
                                @foreach($klasifikasi as $vaj)
                                    <td>
                                        @if($val->k_sbu_konstruksi_id == $vaj->id_klasifikasi_sbu_konstruksi)
                                        {{$vaj->klasifikasi}}
                                        @endif
                                    </td>
                                @endforeach
                                <Td>{{$val->kode}}</Td>
                                <td>{{$val->sub_klasifikasi}}</td>
                                <Td>
                                    <a class="btn btn-secondary btn-sm" data-toggle="modal" href='#m_detail-{{$val->id_sub_klasifikasi_sbu_konstruksi}}'><i class="fa fa-eye"></i></a>
                                    <div class="modal fade" id="m_detail-{{$val->id_sub_klasifikasi_sbu_konstruksi}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Detail Data Sub Klasifikasi Sbu Konstruksi</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                    <div class="modal-body">
                                                        <div class ="form-group">
                                                            <label>Klasifikasi SBU Konstruksi</label>
                                                               @foreach($klasifikasi as $vaj)
                                                                  <p>
                                                                @if($val->k_sbu_konstruksi_id == $vaj->id_klasifikasi_sbu_konstruksi)
                                                                    {{$vaj->klasifikasi}}
                                                                @endif
                                                                  </p>
                                                                @endforeach
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Kode</label>
                                                            <p>{{$val->kode}}</p>
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Sub Klasifikasi</label>
                                                            <p>{{$val->sub_klasifikasi}}</p>
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Lingkup Pekerjaan</label>
                                                            <p>{{$val->lingkup_pekerjaan}}</p>
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Keterangan</label>
                                                            <p>{!! $val->keterangan !!}</p>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_sub_klasifikasi_sbu_konstruksi}}'><i class="fa fa-edit"></i></a>
                                    <div class="modal fade" id="m_edit-{{$val->id_sub_klasifikasi_sbu_konstruksi}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Data Sub Klasifikasi Sbu Konstruksi</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_sub_klasifikasi_sbu_konstruksi',$val->id_sub_klasifikasi_sbu_konstruksi)}}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class ="form-group">
                                                            <label>Klasifikasi Non Konstruksi</label>
                                                            <select name ="k_sbu_konstruksi_id" class ="form-control">
                                                                @foreach($klasifikasi as $vaj)
                                                                    <option
                                                                        @if($val->k_sbu_konstruksi_id == $vaj->id_klasifikasi_sbu_konstruksi)
                                                                        value ="{{$vaj->id_klasifikasi_sbu_konstruksi}}">{{$vaj->klasifikasi}}
                                                                        @endif
                                                                    </option>
                                                                        <option value ="{{$vaj->id_klasifikasi_sbu_konstruksi}}">{{$vaj->klasifikasi}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Kode</label>
                                                            <input type ="text" name ="kode" value ="{{$val->kode}}" class ="form-control" placeholder="Kode">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Sub Klasifikasi</label>
                                                            <input type ="text" name ="sub_klasifikasi" value ="{{$val->sub_klasifikasi}}" class ="form-control"
                                                            placeholder="Sub Klasifikasi"
                                                            >
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Lingkup Pekerjaan</label>
                                                            <input type ="text" value ="{{$val->lingkup_pekerjaan}}"
                                                                   class ="form-control" name ="lingkup_pekerjaan">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Keterangan</label>
                                                            <textarea class ="form-control" name ="keterangan">{!! $val->keterangan !!}}</textarea>

                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_sub_klasifikasi_sbu_konstruksi}}">
                                        <i class = "fa fa-trash"></i>
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="del-{{$val->id_sub_klasifikasi_sbu_konstruksi}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data  </h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action="{{route('delete_sub_klasifikasi_sbu_konstruksi',$val->id_sub_klasifikasi_sbu_konstruksi)}}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        Apa Anda Yakin ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class = "btn btn-danger"><i class = "fa fa-trash"></i> Hapus</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </Td>

                            </tr>
                         @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
