@extends('backend.admin.layout.header')
@section('title','History Pembayaran KTA')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-tools">
                    <div class ="row">
                        <div class ="col-6">

                        </div>
                        <div class ="col-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">KTA</li>
                            </ol>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nomor Keanggotaan</th>
                            <th>Nama Keanggotaan</th>
                            <th>Nama Bank</th>
                            <th>Keterangan</th>
                            <th width="20%">Aksi</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pembayaran_kta as $key=>$val)
                            <tr>
                                <td>{{$key++}}</td>
                                @foreach($anggota as $vaa)
                                    @if($val->anggota == $vaa->id_anggota)
                                <td>{{$vaa->nomor_keanggotaan}}</td>
                                        <td>{{$vaa->nama_perusahaan}}</td>
                                    @endif
                                @endforeach
                                @foreach($rekening as $vag)
                                    @if($val->rekening_pembayaran_id == $vag->id_pembayaran_rekening)
                                <td>{{$vag->nama_bank}}</td>
                                    @endif
                                @endforeach
                                <td>{{$val->keterangan}}</td>
                                <td>proses</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
