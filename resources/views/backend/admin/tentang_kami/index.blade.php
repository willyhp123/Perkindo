@extends('backend.admin.layout.header')

@section('title','Tentang Kami')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="card">
                <div class ="row">
                    <div class ="col-6"></div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Tentang Kami</li>
                            </ol>
                        </div>
                    </div>
                    </div>
                @foreach($tentang_kami as $val)
                <div class="card-body">
                  <form method="post" action="{{route('update_tentang_kami',$val->id_tentang_kami)}}" enctype="multipart/form-data">
                      @csrf
                      <div class ="form-group">
                          <label>Deskripsi</label>
                          <textarea id ="summernote_deskripsi" name ="deskripsi" placeholder="Deskripsi">{!! $val->deskripsi !!}</textarea>
                      </div>
                      <div class ="form-group">
                          <label>Visi</label>
                          <textarea id ="summernote_visi" name ="visi" placeholder="Visi">{!! $val->visi !!}</textarea>
                      </div>
                      <div class ="form-group">
                          <label>Misi</label>
                          <textarea id ="summernote_misi" name ="misi" placeholder="Misi">{!! $val->misi !!}</textarea>
                      </div>
                      <div class ="form-group">
                          <label>sejarah</label>
                          <textarea id ="summernote_sejarah" name ="sejarah" placeholder="Sejarah">{!! $val->sejarah !!}</textarea>
                      </div>
                      <div class ="form-group">
                          <label>Alamat</label>
                          <textarea id ="summernote_alamat" name ="alamat" placeholder="Alamat">{!! $val->alamat !!}</textarea>
                      </div>
                      <div class ="row">
                          <div class ="col-4">
                              <div class ="form-group">
                                  <label>Nomor Hp</label>
                                  <input type ="text" name ="nomor_hp" value="{{$val->nomor_hp}}"
                                         class ="form-control" placeholder="Nomor Hp">
                              </div>
                          </div>
                          <div class ="col-4">
                              <div class ="form-group">
                                  <label>Email 1</label>
                                  <input type ="email" name="email1" value ="{{$val->email1}}" class ="form-control" placeholder="Email 1">
                              </div>
                          </div>
                          <div class ="col-4">
                              <div class ="form-group">
                                  <label>Email 2</label>
                                  <input type="email" name="email2" value="{{$val->email2}}" class ="form-control" placeholder="Email 2">
                              </div>
                          </div>
                      </div>
                      <div class ="row">
                          <div class ="col-4">
                              <div class ="form-group">
                                  <label>Map</label><br>
                                  <img src="" width="150px" height="150px"><br>
                                  <div style="height: 10px;"></div>
                                  <input type ="file" name ="map">
                              </div>
                          </div>
                          <div class ="col-4">
                              <div class ="form-group">
                                  <label>Instagram</label>
                                  <input type ="text" name ="instagram" value ="{{$val->instagram}}"
                                         class ="form-control" placeholder="Instagram">
                              </div>
                          </div>
                          <div class ="col-4">
                              <div class ="form-group">
                                  <label>facebook</label>
                                  <input type ="text" name ="facebook" value ="{{$val->facebook}}"
                                         class ="form-control" placeholder=" Facebook">
                              </div>
                          </div>
                      </div>

                </div>
                <div class ="card-footer">
                    <button type ="submit" class ="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                </div>
                    </form>
                @endforeach
                </div>
        </section>

            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection


