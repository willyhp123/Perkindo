@extends('Backend.admin.layout.header')
@section('title','Pembayaran Registrasi Sbu Konstruksi')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-tools">
                    <div class ="row">
                        <div class ="col-6">

                        </div>
                        <div class ="col-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">KTA</li>
                            </ol>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nomor Keanggotaan</th>
                            <th>Nama Keanggotaan</th>
                            <th>Nama Bank</th>
                            <th>Keterangan</th>
                            <th>Proses</th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pembayaran_registrasi_sbu_konstruksi as $key=>$val)

                            <tr>
                                <td>{{$key++}}</td>
                                @foreach($anggota as $vaa)
                                    @if($val->anggota_id == $vaa->id_anggota)
                                        <td>{{$vaa->nomor_keanggotaan}}</td>
                                        <td>{{$vaa->nama_perusahaan}}</td>
                                    @endif
                                @endforeach
                                @foreach($rekening as $vag)
                                    @if($val->rekening_pembayaran_id == $vag->id_rekening_pembayaran)
                                <td>{{$vag->nama_bank}} | {{$vag->no_rek}}</td>
                                    @endif
                                @endforeach
                                <td>{{$val->keterangan}}</td>
                                <td>
                                    <a class="btn btn-secondary btn-sm" data-toggle="modal" href='#m_detail-{{$val->id_pembayaran_registrasi_sbu_konstruksi}}'><i class="fa fa-eye"></i></a>
                                    <div class="modal fade" id="m_detail-{{$val->id_pembayaran_registrasi_sbu_konstruksi}}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Proses Data </h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                @csrf
                                                <div class="modal-body">
                                                    <div class ="form-group">
                                                        @foreach($anggota as $vat)
                                                            @if($val->anggota_id == $vat->id_anggota)
                                                                <div class ="row">
                                                                    <div class ="col-4">
                                                                        <div class ="form-group">
                                                                            <label>Foto Penanggung Jawab</label>
                                                                            <div style ="height: 10px"></div>
                                                                            <img src ="{{asset('foto_perusahaan/foto_penanggung_jawab/'.$vat->foto_penanggung_jawab)}}" width="200px" height="200px">
                                                                            <p> Filesize <span>{{number_format(File::size(public_path('foto_perusahaan/foto_penanggung_jawab/'.$vat->foto_penanggung_jawab))/1024 ,2)}} KB</span></p>
                                                                        </div>

                                                                    </div>
                                                                    <div class ="col-8">
                                                                        <div class ="row">
                                                                            <div class="col-5">
                                                                                <p><b>Nomor Anggota</b></p>
                                                                            </div>
                                                                            <div  class ="col-1">=></div>
                                                                            <div class="col-6">
                                                                                <p>{{$vat->nomor_keanggotaan}}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class ="row">
                                                                            <div class="col-5">
                                                                                <p><b>Nama Perusahaan</b></p>
                                                                            </div>
                                                                            <div  class ="col-1">=></div>
                                                                            <div class="col-6">
                                                                                <p>{{$vat->nama_perusahaan}}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class ="row">
                                                                            <div class="col-5">
                                                                                <p><b>Nama Penanggung Jawab</b></p>
                                                                            </div>
                                                                            <div  class ="col-1">=></div>
                                                                            <div class="col-6">
                                                                                <p>{{$vat->nama_penanggung_jawab}}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class ="row">
                                                                            <div class="col-5">
                                                                                <p><b>alamat Perusahaan</b></p>
                                                                            </div>
                                                                            <div  class ="col-1">=></div>
                                                                            <div class="col-6">
                                                                                <p>{{$vat->alamat_perusahaan}}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class ="row">
                                                                            <div class="col-5">
                                                                                <p><b>Provinsi</b></p>
                                                                            </div>
                                                                            <div  class ="col-1">=></div>
                                                                            <div class="col-6">
                                                                                @foreach($provinsi as $vai)
                                                                                    @if($vat->provinsi_id == $vai->id_provinsi)
                                                                                        <p>{{$vai->provinsi}}</p>
                                                                                    @endif
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                        <div class ="row">
                                                                            <div class="col-5">
                                                                                <p><b>Kota / Kabupaten</b></p>
                                                                            </div>
                                                                            <div  class ="col-1">=></div>
                                                                            <div class="col-6">
                                                                                @foreach($kota_kabupaten as $van)
                                                                                    @if($vat->kota_kabupaten_id == $van->id_kota_kabupaten)
                                                                                        <p>{{$van->kota_kabupaten}}</p>
                                                                                    @endif
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                        <div class ="row">
                                                                            <div class="col-5">
                                                                                <p><b>Telepon / Telex / fax</b></p>
                                                                            </div>
                                                                            <div  class ="col-1">=></div>
                                                                            <div class="col-6">
                                                                                <p>{{$vat->telepon_telex_fax}}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class ="row">
                                                                            <div class="col-5">
                                                                                <p><b>no Hp 1</b></p>
                                                                            </div>
                                                                            <div  class ="col-1">=></div>
                                                                            <div class="col-6">
                                                                                <p>{{$vat->no_hp_1}}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class ="row">
                                                                            <div class="col-5">
                                                                                <p><b>no Hp 2</b></p>
                                                                            </div>
                                                                            <div  class ="col-1">=></div>
                                                                            <div class="col-6">
                                                                                <p>{{$vat->no_hp_2}}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class ="row">
                                                                            <div class="col-5">
                                                                                <p><b>Email</b></p>
                                                                            </div>
                                                                            <div  class ="col-1">=></div>
                                                                            <div class="col-6">
                                                                                <p>{{$vat->email}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                    </div>
                                                    <div class ="border"></div>
                                                    <div style ="height: 10px"></div>
                                                        <div class ="row">
                                                            <div class ="col-6">
                                                                <div class ="formm-group">
                                                                    <label><b>Nomor Anggota</b></label>
                                                                    <p>{{$vat->nomor_keanggotaan}}</p>
                                                                </div>
                                                               <div class ="form-group">
                                                                   <label><b>Nama Perusahaan</b></label>
                                                                   <p>{{$vat->nama_perusahaan}}</p>
                                                               </div>
                                                                <div class ="form-group">
                                                                    <label><b>Rekening Pembayaran</b></label>
                                                                    @foreach($rekening as $vag)
                                                                        @if($val->rekening_pembayaran_id == $vag->id_rekening_pembayaran)
                                                                            <p>{{$vag->nama_bank}} | {{$vag->no_rek}}</p>
                                                                        @endif
                                                                    @endforeach
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label><b>Registrasi Tahun ke</b></label>
                                                                    <p>{{$val->registrasi_tahun_ke}}</p>
                                                                </div>

                                                            </div>
                                                            <div class ="col-6">
                                                                <div class ="form-group">
                                                                    <label><b>Atas Nama</b></label>
                                                                    <p>{{$val->atas_nama}}</p>
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label><b>keterangan</b></label>
                                                                    <p>{{$val->keterangan}}</p>
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label><b>bukti Pembayaran</b></label><br>
                                                                    <img src ="{{asset('foto_registrasi_sbu_konstruksi/'.$val->bukti_pembayaran)}}" width="200px" height="200px">
                                                                    <p><span></span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                     <a class="btn btn-primary btn-sm" data-toggle="modal" href='#m_proses-{{$val->id_pembayaran_registrasi_sbu_konstruksi}}'><i class=""></i> Proses</a>
                                      <div class="modal fade" id="m_proses-{{$val->id_pembayaran_registrasi_sbu_konstruksi}}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Proses Data </h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            </div>
                                            @csrf
                                            <div class="modal-body">
                                                <h4>Apa anda yakin?</h4>
                                            </div>
                                            <form method="post" action="{{route('update_registrasi_sbu_konstruksi',$val->id_pembayaran_registrasi_sbu_konstruksi)}}">
                                                @csrf
                                            <div class ="modal-footer">
                                                <button  type ="submit" class ="btn btn-primary">Proses</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
