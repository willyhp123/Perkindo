@extends('Backend.admin.layout.header')
@section('title','Pembayaran Sbu Konstruksi')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-tools">
                    <div class ="row">
                        <div class ="col-6">

                        </div>
                        <div class ="col-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">KTA</li>
                            </ol>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nomor Keanggotaan</th>
                            <th>Nama Keanggotaan</th>
                            <th>Nama Bank</th>
                            <th>Keterangan</th>
                            <th>Proses</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pembayaran_sbu_konstruksi as $key=>$val)
                            <tr>
                                <td>{{$key+1}}</td>
                                @foreach($anggota as $vaa)
                                    @if($val->anggota_id == $vaa->id_anggota)
                                        <td>{{$vaa->nomor_keanggotaan}}</td>
                                        <td>{{$vaa->nama_perusahaan}}</td>
                                    @endif
                                @endforeach
                                @foreach($rekening as $vag)
                                    @if($val->rekening_pembayaran_id == $vag->id_rekening_pembayaran)
                                <td>{{$vag->nama_bank}}</td>
                                    @endif
                                @endforeach
                                <td>{{$val->keterangan}}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_pembayaran_sbu_konstruksi}}'><i class=""></i>Proses</a>
                                    <div class="modal fade" id="m_edit-{{$val->id_pembayaran_sbu_konstruksi}}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content ">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Proses Data Pembayaran Konstruksi</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('tambah_sbu_konstruksi_anggota')}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class ="form-group">
                                                            @foreach($anggota as $vat)
                                                                @if($val->anggota_id == $vat->id_anggota)
                                                                    <div class ="row">
                                                                        <div class ="col-4">
                                                                            <div class ="form-group">
                                                                                <label>Foto Penanggung Jawab</label>
                                                                                <div style ="height: 10px"></div>
                                                                                <img src ="{{asset('foto_perusahaan/foto_penanggung_jawab/'.$vat->foto_penanggung_jawab)}}" width="200px" height="200px">
                                                                                <p> Filesize <span>{{number_format(File::size(public_path('foto_perusahaan/foto_penanggung_jawab/'.$vat->foto_penanggung_jawab))/1024 ,2)}} KB</span></p>
                                                                            </div>

                                                                        </div>
                                                                        <div class ="col-8">
                                                                            <div class ="row">
                                                                                <div class="col-5">
                                                                                    <p><b>Nomor Anggota</b></p>
                                                                                </div>
                                                                                <div  class ="col-1">=></div>
                                                                                <div class="col-6">
                                                                                    <p>{{$vat->nomor_keanggotaan}}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class ="row">
                                                                                <div class="col-5">
                                                                                    <p><b>Nama Perusahaan</b></p>
                                                                                </div>
                                                                                <div  class ="col-1">=></div>
                                                                                <div class="col-6">
                                                                                    <p>{{$vat->nama_perusahaan}}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class ="row">
                                                                                <div class="col-5">
                                                                                    <p><b>Nama Penanggung Jawab</b></p>
                                                                                </div>
                                                                                <div  class ="col-1">=></div>
                                                                                <div class="col-6">
                                                                                    <p>{{$vat->nama_penanggung_jawab}}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class ="row">
                                                                                <div class="col-5">
                                                                                    <p><b>alamat Perusahaan</b></p>
                                                                                </div>
                                                                                <div  class ="col-1">=></div>
                                                                                <div class="col-6">
                                                                                    <p>{{$vat->alamat_perusahaan}}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class ="row">
                                                                                <div class="col-5">
                                                                                    <p><b>Provinsi</b></p>
                                                                                </div>
                                                                                <div  class ="col-1">=></div>
                                                                                <div class="col-6">
                                                                                    @foreach($provinsi as $vai)
                                                                                        @if($vat->provinsi_id == $vai->id_provinsi)
                                                                                            <p>{{$vai->provinsi}}</p>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                            <div class ="row">
                                                                                <div class="col-5">
                                                                                    <p><b>Kota / Kabupaten</b></p>
                                                                                </div>
                                                                                <div  class ="col-1">=></div>
                                                                                <div class="col-6">
                                                                                    @foreach($kota_kabupaten as $van)
                                                                                        @if($vat->kota_kabupaten_id == $van->id_kota_kabupaten)
                                                                                            <p>{{$van->kota_kabupaten}}</p>
                                                                                        @endif
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                            <div class ="row">
                                                                                <div class="col-5">
                                                                                    <p><b>Telepon / Telex / fax</b></p>
                                                                                </div>
                                                                                <div  class ="col-1">=></div>
                                                                                <div class="col-6">
                                                                                    <p>{{$vat->telepon_telex_fax}}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class ="row">
                                                                                <div class="col-5">
                                                                                    <p><b>no Hp 1</b></p>
                                                                                </div>
                                                                                <div  class ="col-1">=></div>
                                                                                <div class="col-6">
                                                                                    <p>{{$vat->no_hp_1}}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class ="row">
                                                                                <div class="col-5">
                                                                                    <p><b>no Hp 2</b></p>
                                                                                </div>
                                                                                <div  class ="col-1">=></div>
                                                                                <div class="col-6">
                                                                                    <p>{{$vat->no_hp_2}}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class ="row">
                                                                                <div class="col-5">
                                                                                    <p><b>Email</b></p>
                                                                                </div>
                                                                                <div  class ="col-1">=></div>
                                                                                <div class="col-6">
                                                                                    <p>{{$vat->email}}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class = "border"></div>
                                                                    <div style="height: 10px"></div>

                                                                            @csrf
                                                                            <div class ="row">
                                                                                <div class ="col-6">
                                                                                    <input type ="text" hidden name ="id_pembayaran_sbu_konstruksi" value ="{{$val->id_pembayaran_sbu_konstruksi}}">
                                                                                    <input type ="text" name ="anggota_id" hidden value ="{{$vat->id_anggota}}">
                                                                                    <div class ="form-group">
                                                                                    <label>No Seri Formulir</label>
                                                                                    <input type ="text" name ="no_seri_formulir" class ="form-control @error('no_seri_formulir') is-invalid @enderror" placeholder="No Seri Formulir">
                                                                                    @error('no_seri_formulir')
                                                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                                                     @enderror
                                                                                    </div>
                                                                                    <div class ="form-group">
                                                                                        <label>Tanggal masuk</label>
                                                                                        <input type ="date" name ="tanggal_masuk"
                                                                                               class ="form-control @error('tanggal_masuk') is-invalid @enderror">
                                                                                        @error('tanggal_masuk')
                                                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                    <div class ="form-group">
                                                                                        <label>berlaku Sampai</label>
                                                                                        <input type ="date" name ="berlaku_sampai"
                                                                                               class ="form-control @error('berlaku_sampai') is-invalid @enderror">
                                                                                        @error('berlaku_sampai')
                                                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                    <div class ="form-group">
                                                                                        <label>Registrasi tahun ke 2</label>
                                                                                        <input type ="date" name ="registrasi_tahun_ke_2"
                                                                                               class ="form-control @error('registrasi_tahun_ke_2') is-invalid @enderror">
                                                                                    </div>
                                                                                    @error('registrasi_tahun_ke_2')
                                                                                    <span class ="invalid-feedback">{{$message}}</span>
                                                                                    @enderror
                                                                                </div>
                                                                                <div class ="col-6">
                                                                                    <div class ="form-group">
                                                                                        <label>Registrasi tahun ke 3</label>
                                                                                        <input type ="date" name ="registrasi_tahun_ke_3" class ="form-control @error('registrasi_tahun_ke_3') is-invalid @enderror">
                                                                                        @error('registrasi_tahun_ke_3')
                                                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                    <div class ="form-group">
                                                                                        <label>tenaga ahli</label>
                                                                                        <input type ="text" name ="tenaga_ahli" class ="form-control @error('tenaga_ahli') is-invalid @enderror"
                                                                                               placeholder="Tenaga Ahli">
                                                                                        @error('tenaga_ahli')
                                                                                            <span class ="invalid-feedback">{{$message}}</span>
                                                                                        @enderror
                                                                                    </div>
                                                                                    <div class ="form-group">
                                                                                        <label>File</label><br>
                                                                                        <button type="button" class ="btn btn-primary" id ="inputimage">
                                                                                            <i class ="fa fa-upload"></i> Upload
                                                                                        </button>
                                                                                        <div style="height: 10px"></div>
                                                                                        <div id ="foto"></div>
                                                                                        <p>Filesize <span id ="filesize"></span></p>
                                                                                        <input type ="file" hidden onchange="image_sbu_konstruksi(event)" id="idInputImage" name ="foto">
                                                                                    @error('foto')
                                                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                                                     @enderror
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                 @endif
                                                             @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary"> Proses</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<script>
    let button = document.getElementById('inputimage'),
        image  = document.getElementById('idInputImage'),
        filesize = document.getElementById('filesize');
    button.onclick = ()=>{
        image.click();
    }
    function image_sbu_konstruksi(event) {
        var image = URL.createObjectURL(event.target.files[0]);
        var imagediv = document.getElementById('foto');
        var newimg = document.createElement('img');
        newimg.src = image;
        newimg.width = 200;
        newimg.height = 200;
        imagediv.appendChild(newimg);
    }
    function  updatesize(){
        setInterval(() =>{
            let nBytes = 0;
            let Files = image.files;
            for(let i = 0;i <Files.length;i++){
                nBytes += Files[i].size;
            }
            let output = nBytes +"bytes" ;
            let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
            for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                output = nApprox.toFixed(3)+" "+multiple[j];

            }
            filesize.innerHTML = output;
        },1000)
    }
    image.addEventListener("change",updatesize());
</script>
@endsection
