@extends('backend.admin.layout.header')
@section('title','Agenda')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Agenda
                        </button>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Agenda</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action ="{{route('tambah_agenda')}}">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Nama Agenda</label>
                                                <input type ="text" name ="nama_agenda" class ="form-control @error('nama_agenda') is-invalid @enderror" placeholder="Nama Agenda">
                                                @error('nama_agenda')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Tanggal Mulai</label>
                                                <input type ="date" name ="tanggal_mulai" class ="form-control  @error('tanggal_mulai') is-invalid @enderror" placeholder="Tanggal Mulai">
                                                @error('tanggal_mulai')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Tanggal Selesai</label>
                                                <input type ="date" name ="tanggal_selesai" class ="form-control  @error('tanggal_selesai') is-invalid @enderror" placeholder="Tanggal Selesai">
                                                @error('tanggal_selesai')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Agenda</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Nama Agenda</th>
                            <th width="20%">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($agenda as $key=>$val)
                            <tr>
                                <Td>{{$key+1}}</Td>
                                <td>{{$val->nama_agenda}}</td>
                                <td>{{$val->tanggal_mulai}}</td>
                                <td>{{$val->tanggal_selesai}}</td>
                                <td>

                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_agenda}}'><i class="fa fa-edit"></i>Edit</a>
                                    <div class="modal fade" id="m_edit-{{$val->id_agenda}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Agenda</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action ="{{route('update_agenda',$val->id_agenda)}}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class ="form-group">
                                                            <label>Nama Agenda</label>
                                                            <input type ="text" name ="nama_agenda" value ="{{$val->nama_agenda}}" class ="form-control" placeholder="Nama Agenda">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Tanggal Mulai</label>
                                                            <input type ="date" name ="tanggal_mulai" value ="{{$val->tanggal_mulai}}" class ="form-control">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Tanggal Selesai</label>
                                                            <input type ="date" name ="tanggal_selesai" value ="{{$val->tanggal_selesai}}" class ="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_agenda}}">
                                        <i class = "fa fa-trash"></i> Hapus
                                    </button>

                                    <div class="modal fade" id="del-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data {{$val->nama_agenda}}</h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action="{{route('delete_agenda',$val->id_agenda)}}">
                                                   @csrf
                                                <div class="modal-body">
                                                    Apa Anda Yakin ?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type = "submit" class = "btn btn-danger"><i class = "fa fa-trash"></i>Hapus</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection
