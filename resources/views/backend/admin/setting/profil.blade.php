@extends('backend.admin.layout.header')

@section('title','Perkindo Kalbar Profil')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="card">
                <div class ="row">
                    <div class ="col-6">

                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Profil</li>
                            </ol>
                        </div>
                    </div>

                </div>
                @foreach ($profil as $val)
                <form method="post" action="{{route('update_profil',$val->id_profil)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                       <p>Profil Perkindo Kalbar</p>
                        <textarea id ="summernote" name ="profil">{!! $val->profil !!}</textarea>
                        <p>Lokasi Perkindo kalbar</p>
                        <textarea id ="summernote1" name ="kontak_perkindo">{!! $val->kontak_perkindo !!}</textarea>
                        <p>Lokasi Perkindo Kalbar</p>
                        <img src="{{asset('foto_lokasi_profil/'.$val->foto_lokasi)}}" id ="foto_lama" width="200px" height="200px">
                        <div id ="foto"></div>
                        <div style="height: 10px"></div>
                        <p> Filesize <span id ="filesize">
                                {{number_format(File::size(public_path('foto_lokasi_profil/'.$val->foto_lokasi))/1024, 2 )}}</span> KB</p>
                        <input type ="file" name ="foto_lokasi" class ="custom-file">
                    </div>
                    <div class ="card-footer">
                        <button type ="submit" class ="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                    </div>
                    </form>
                @endforeach
               
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
        let button = document.getElementById('inputimage'),
            image = document.getElementById('idInputImage'),
            img = document.getElementById('foto_lama'),
            filesize = document.getElementById('filesize');

    </script>
@endsection

