@extends('backend.admin.layout.header')
@section('title','slide')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Slide
                        </button>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Slide</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('tambah_slide')}}" enctype="multipart/form-data">
                                       @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Gambar Slide</label><br>
                                                <div id ="foto"></div>
                                                <div style="height: 10px;"></div>
                                                <p>Filesize : <span id ="filesize"> 0</span></p>
                                                <button type ="button" id ="inputimage" class ="btn btn-primary"><i class ="fa fa-upload"></i> Upload</button>
                                                <input type ="file"  hidden onchange="image_foto_slide(event)" id ="idInputImage" name ="gambar_slide" class ="@error('gambar_slide') is-invalid @enderror">
                                                @error('gambar_slide')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Judul</label>
                                                <input type ="text" name ="judul" class ="form-control @error('judul') is-invalid @enderror" placeholder="Judul">
                                                @error('judul')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Url</label>
                                                <input type ="text" name ="url" class ="form-control @error('url') is-invalid @enderror" placeholder="URL">
                                                @error('url')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Slide</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Gambar Slide</th>
                            <th>Judul</th>
                            <th>Url</th>
                            <th>Aktif</th>
                            <th width="20%">Aksi</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($slide as $key=>$val)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td><img src="{{asset('gambar_slide/'.$val->gambar_slide)}} " width="150px" height="150px"></td>
                                <td>{{$val->judul}}</td>
                                <td>{{$val->url}}</td>
                                <td>{{$val->aktif}}</td>
                                <Td>
                                    <!-- Button trigger modal -->
                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_slide}}'><i class="fa fa-edit"></i>Edit</a>
                                    <div class="modal fade" id="m_edit-{{$val->id_slide}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Data Slide</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_slide',$val->id_slide)}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class ="row">
                                                            <div class ="col-6">
                                                                <label>Gambar Slide</label><br>
                                                                <img src="{{asset('gambar_slide/'.$val->gambar_slide)}}" width="200px" height="200px">
                                                                <div style="height: 10px"></div>
                                                                <p> Filesize : <span>{{number_format(File::size(public_path('gambar_slide/'.$val->gambar_slide))/1024,2)}} KB</span></p>
                                                            </div>
                                                            <div class ="col-6">
                                                                <label>Gambar Slide Baru</label>
                                                                <div id ="foto_update"></div>
                                                                <div style="height: 10px"></div>
                                                                <p>Filesize <span id ="filesize_update"> 0</span></p>
                                                            </div>
                                                        </div>
                                                        <div class ="form-group">
                                                            <button type ="button" id ="inputimage_update" class ="btn btn-primary"><i class ="fa fa-upload"></i> Upload</button>
                                                            <input type ="file" onchange="image_foto_slide_update(event)" id ="idInputImage_update" hidden name ="gambar_slide">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Judul</label>
                                                            <input type ="text" name ="judul" value ="{{$val->judul}}" class ="form-control" placeholder="Judul">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Url</label>
                                                            <input type ="text" name ="url" class ="form-control" value ="{{$val->url}}"  >
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_slide}}">
                                        <i class = "fa fa-trash"></i> Hapus
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="del-{{$val->id_slide}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data {{$val->judul}} </h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action="{{route('delete_slide',$val->id_slide)}}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        Apa Anda Yakin ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" onclick="submit()" class = "btn btn-danger"><i class = "fa fa-trash"></i> Hapus</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </Td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>
        function image_foto_slide_update(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto_update');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
        function image_foto_slide(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
        function submit(){
            let filesize = document.getElementById("filesize_baru");
            filesize.innerHTML = 0;

        }
        let button = document.getElementById("inputimage"),
            image =document.getElementById("idInputImage"),
            filesize = document.getElementById("filesize"),
            button_update = document.getElementById("inputimage_update"),
            image_update = document.getElementById("idInputImage_update"),
            filesize_update = document.getElementById("filesize_update");
        button.onclick= ()=>{
            image.click();
        }
        button_update.onclick= ()=>{
            image_update.click()
        }
        function  updatesize(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];

                }
                filesize.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize());
        function  updatesize_update(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image_update.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];

                }
                filesize_update.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize_update());
    </script>
@endsection
