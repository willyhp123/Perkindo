@extends('backend.admin.layout.header')
@section('title','Download')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Download
                        </button>
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Download</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('tambah_download')}}"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Kategori Download</label>
                                                <select name ="kategori_download_id" class ="form-control @error('kategori_download_id') is-invalid @enderror">
                                                    <option value ="">---Pilih Kategori Download---</option>
                                                    @foreach($kategori_download as $val)
                                                        <option value ="{{$val->id_kategori_download}}">
                                                            {{$val->kategori_download}}</option>
                                                    @endforeach
                                                </select>
                                                @error('kategori_download_id')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>dokumen</label><br>
                                                <input type ="file" name ="dokumen" class ="@error('dokumen') is-invalid @enderror">
                                                @error('dokumen')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Judul</label>
                                                <input type ="text" name ="judul" class ="form-control @error('judul') is-invalid @enderror" placeholder="Judul Download">
                                                @error('judul')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Kategori Download</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Kategori Download</th>
                            <th>dokumen</th>
                            <Th>Judul</Th>
                            <th width="20%">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($download as $key=>$val)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>
                                    @foreach($kategori_download as $vap)
                                        @if($val->kategori_download_id == $vap->id_kategori_download)

                                            {{$vap->kategori_download}}
                                         @endif
                                    @endforeach
                                </td>
                                <td>

                                    <a href="{{route('download_dokumen',$val->dokumen)}}">{{$val->dokumen}}</a>
                                </td>
                                <td>{{$val->judul}}</td>
                                <td>
                                    <!-- Button trigger modal -->
                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_download}}'><i class="fa fa-edit"></i>Edit</a>
                                    <div class="modal fade" id="m_edit-{{$val->id_download}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Data Download</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_download',$val->id_download)}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                       <div class ="form-group">
                                                           <label>Kategori Download</label>
                                                           <select name ="kategori_download_id" class ="form-control">
                                                               @foreach($kategori_download as $vap)
                                                                   @if($val->kategori_download_id == $vap->id_kategori_download)
                                                                        <option value ="{{$vap->id_kategori_download}}">{{$vap->kategori_download}}</option>
                                                                   @endif
                                                               @endforeach
                                                           </select>
                                                       </div>
                                                        <div class ="form-group">
                                                            <label>Document</label><br>
                                                            <p>{{$val->dokumen}}</p>
                                                            <input type ="file" name ="dokumen">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Judul</label>
                                                            <input type ="text" name ="judul" class ="form-control" value ="{{$val->judul}}">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_download}}">
                                        <i class = "fa fa-trash"></i> Hapus
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="del-{{$val->id_download}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data  </h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action="{{route('delete_download',$val->id_download)}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                        Apa Anda Yakin ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class = "btn btn-danger"><i class = "fa fa-trash"></i> Hapus</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
