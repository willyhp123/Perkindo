@extends('backend.admin.layout.header')
@section('title','Berita')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data Berita
                        </button>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Berita</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('tambah_berita')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Judul</label>
                                                <input type ="text" name ="judul" class ="form-control @error('judul') is-invalid @enderror" placeholder="judul Berita">
                                                @error('judul')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Isi Berita</label>
                                                <textarea id="summernote" class ="@error('isi') is-invalid @enderror" name ="isi" style="height: 200px;"></textarea>
                                            </div>
                                            @error('isi')
                                            <span class ="invalid-feedback">{{$message}}</span>
                                            @enderror
                                            <div class ="form-group">
                                                <label>Url</label>
                                                <input type ="text" name ="url" class ="form-control @error('url') is-invalid @enderror" placeholder="URL">
                                                @error('url')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Foto</label><br>
                                                <div id ="foto"></div>
                                                <div style="height: 10px"></div>
                                                <button type ="button" id ="inputimage" class ="btn btn-primary"><i class ="fa fa-upload"></i> Upload</button>
                                                <input type="file" hidden id ="idInputImage" name ="foto_berita" class ="@error('foto') is-invalid @enderror">
                                                @error('foto')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>

                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Berita</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Foto</th>
                            <th>Judul</th>
                            <th>Tanggal Posting</th>
                            <th>publish</th>
                            <th width="20%">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($berita as $key => $val)
                            <Tr>
                                <td>{{$key+1}}</td>
                                <Td><img src ="{{asset('foto_berita/'.$val->foto)}}" width="100px" height="100px"></Td>
                                <td>{{$val->judul}}</td>
                                <td>{{$val->created_at}}</td>
                                <td>{{$val->publish}}</td>
                                <td>
                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_berita}}'><i class="fa fa-edit"></i>Edit</a>
                                    <div class="modal fade" id="m_edit-{{$val->id_berita}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Data Slide</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_berita',$val->id_berita)}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class ="form-group">
                                                            <label>Judul</label>
                                                            <input type ="text" name ="judul" value ="{{$val->judul}}" class ="form-control" placeholder="judul Berita">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Isi Berita</label>
                                                            <textarea id="summernote1" name ="isi"  style="height: 200px;">{!! $val->isi !!}</textarea>
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Url</label>
                                                            <input type ="text" name ="url" value ="{{$val->url}}" class ="form-control" placeholder="URL">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Foto</label><br>
                                                            <img src ="{{asset('foto_berita/'.$val->foto)}}" width="200px" height="200px">
                                                            <div style="height: 10px"></div>
                                                            <input type="file" name ="foto_berita">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_berita}}">
                                        <i class = "fa fa-trash"></i> Hapus
                                    </button>
                                    <div class="modal fade" id="del-{{$val->id_berita}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data {{$val->judul}} </h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action="{{route('delete_berita',$val->id_berita)}}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        Apa Anda Yakin ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class = "btn btn-danger"><i class = "fa fa-trash"></i> Hapus</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </Tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>

    </script>
@endsection
