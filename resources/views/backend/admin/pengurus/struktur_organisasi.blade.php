@extends('backend.admin.layout.header')

@section('title','Struktur Organisasi')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">

                <div class ="row">
                    <div class ="col-6">

                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Struktur Organisasi</li>
                            </ol>
                        </div>
                    </div>

                </div>
                    @foreach($struktur_organisasi as $val)
                    <div class="card-body">
                        <p class ="alert alert-success" >{!! $val->periode !!}</p>
                        <img src ="{{asset('foto_struktur_organisasi/'.$val->gambar)}}"width="300px" height="300px">
                    </div>
                    <div class ="card-footer">
                        <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_struktur_organisasi}}'><i class="fa fa-edit"></i>Edit</a>
                        <div class="modal fade" id="m_edit-{{$val->id_struktur_organisasi}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Edit Data Struktur Organisasi</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <form method="post" action="{{route('update_struktur_organisasi',$val->id_struktur_organisasi)}}"
                                          enctype="multipart/form-data" >
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Periode</label>
                                                <textarea id ="summernote" name ="periode">{!! $val->periode !!}</textarea>
                                            </div>
                                            <div class ="row">
                                                <div class ="col-6">
                                                    <div class ="form-group">
                                                        <label>Foto Struktur Organisasi</label><br>
                                                        <img src="{{asset('foto_struktur_organisasi/'.$val->gambar)}}" width="200px" height="200px">
                                                        <div style="height: 10px"></div>
                                                        <p>Filesize : <span>{{number_format(File::size(public_path('foto_struktur_organisasi/'.$val->gambar)) /1024,2)}} KB</span></p>
                                                    </div>
                                                </div>

                                                    <div class ="col-6" id ="image_baru">
                                                        <div class ="form-group">
                                                            <label>Foto Baru Struktur Organisasi</label>
                                                            <div id ="foto"  class ="mb-1"></div>
                                                            <p> Filesize  :<br><span id ="filesize_baru"></span></p>
                                                        </div>
                                                    </div>


                                            </div>
                                            <div class ="form-group">
                                                <button type ="button" class ="btn btn-primary" id ="inputimage"><i class ="fa fa-upload"></i> Upload</button>
                                                <input type ="file" id ="idInputImage" onchange="foto_struktur_organisasi(event)" name ="gambar" hidden>
                                            </div>

                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" onclick="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
            <!-- /.card -->
            @endforeach

        </section>
        <!-- /.content -->
    </div>
    <script>
        function submit(){
            let filesize = document.getElementById("filesize_baru");
            filesize.innerHTML = 0;

        }
        image_baru();
        let image_barus = document.getElementById("image_baru");
        let filesize = document.getElementById("filesize_baru"),
            button = document.getElementById("inputimage"),
            image = document.getElementById("idInputImage"),
            box = document.getElementById("box");
        button.onclick = ()=>{
            let image_barus = document.getElementById("image_baru");
            image.click();
            image_barus.style.visibility="visible";

        }
        function image_baru (){
            let image_barus = document.getElementById("image_baru");
            image_barus.style.visibility="hidden";
        }
        function  updatesize(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];

                }
                filesize.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize());
        function foto_struktur_organisasi(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
    </script>
@endsection
