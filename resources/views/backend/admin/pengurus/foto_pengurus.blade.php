@extends('backend.admin.layout.header')

@section('title','Foto Pengurus')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">

                <div class ="row">
                    <div class ="col-6">

                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Foto Pengurus</li>
                            </ol>
                        </div>
                    </div>

                </div>
                @foreach($foto_pengurus as $val)
                <div class="card-body">
                    <p class ="alert alert-success" >{!! $val->periode !!}</p>
                    <img src ="{{asset('foto_periode/'.$val->foto)}}"width="300px" height="300px">
                </div>
                    <div class ="card-footer">
                        <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_pengurus}}'><i class="fa fa-edit"></i>Edit</a>
                        <div class="modal fade" id="m_edit-{{$val->id_pengurus}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Edit Data foto Pengurus</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <form method="post" action="{{route('update_foto_pengurus',$val->id_pengurus)}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Periode</label>
                                               <textarea id ="summernote" name ="periode">{!! $val->periode !!}</textarea>
                                            </div>
                                                <div class ="row">
                                                    <div class ="form-group" id ="box">
                                                        <label>Foto Lama Pengurus</label><br>
                                                    <div class ="col-6">
                                                        <img src="{{asset('foto_periode/'.$val->foto)}}"   width="200px" height="200px">
                                                        <div style="height: 10px"></div>
                                                        <p> Filesize  :<span id ="filesize">
                                                                {{number_format(File::size
                                                        (public_path('foto_periode/'.$val->foto)) /1024 ,2)}} KB</span></p>
                                                    </div>
                                                </div>
                                                    <div class ="col-6" id ="image_baru">
                                                        <div class ="form-group">
                                                            <label>Foto Baru Pengurus</label>
                                                            <div id ="foto"  class ="mb-1"></div>
                                                            <p> Filesize  :<br><span id ="filesize_baru"></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type ="button" id ="inputimage" class ="btn btn-primary"><i class ="fa fa-upload"></i> Upload</button>
                                                <input type ="file" id ="idInputImage" name ="foto" onchange="foto_pengurus(event)"  hidden>

                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" onclick="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <script>
        function submit(){
            let filesize = document.getElementById("filesize_baru");
            filesize.innerHTML = 0;

        }
        image_baru();

        let image_barus = document.getElementById("image_baru");
        let filesize = document.getElementById("filesize_baru"),
            button = document.getElementById("inputimage"),
            image = document.getElementById("idInputImage"),
            box = document.getElementById("box");
        button.onclick = ()=>{
            let image_barus = document.getElementById("image_baru");
            image.click();

            image_barus.style.visibility="visible";

        }
        function image_baru (){
            let image_barus = document.getElementById("image_baru");
            image_barus.style.visibility="hidden";
        }
        function  updatesize(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];

                }
                filesize.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize());

        function foto_pengurus(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
    </script>
@endsection
