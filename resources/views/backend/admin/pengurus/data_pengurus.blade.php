@extends('backend.admin.layout.header')
@section('title','Data Pengurus')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data pengurus
                        </button>
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data pengurus</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('tambah_pengurus')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="row">
                                                <div class ="col-6">
                                                    <div class ="form-group">
                                                        <label>No KTP</label>
                                                        <input type ="text" name ="no_ktp" class ="form-control @error('no_ktp') is-invalid @enderror" placeholder="No Ktp">
                                                        @error('no_ktp')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>NIK</label>
                                                        <input type ="text" name ="nik" class ="form-control  @error('nik') is-invalid @enderror" placeholder="NIK">
                                                        @error('nik')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Nama</label>
                                                        <input type ="text" name ="nama" class ="form-control @error('nama') is-invalid @enderror" placeholder="Nama">
                                                        @error('nama')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Tempat Lahir</label>
                                                        <input type ="text" name ="tempat_lahir" class ="form-control @error('tempat_lahir') is-invalid @enderror" placeholder="Tempat Lahir">
                                                        @error('tempat_lahir')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Tanggal Lahir</label>
                                                        <input type ="date" name ="tanggal_lahir" class ="form-control @error('tanggal_lahir') is-invalid @enderror" placeholder="tanggal_lahir">
                                                        @error('tanggal_lahir')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Jenis Kelamin</label>
                                                        <select class ="form-control" name ="jenis_kelamin">
                                                            <option value ="">---Pilih Jenis Kelamin---</option>
                                                            <option value ="laki-laki">Laki-laki</option>
                                                            <option value ="perempuan">Perempuan</option>
                                                        </select>
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Password</label>
                                                       <input type ="password" name ="password" class ="form-control" placeholder="Password">
                                                    </div>
                                                </div>
                                                <div class ="col-6">
                                                    <div class ="form-group">
                                                        <label>Agama</label>
                                                        <select name ="agama_id" class ="form-control ">
                                                            <option value ="">--pilih Agama--</option>
                                                            @foreach($agama as $val)
                                                                <option value ="{{$val->id_agama}}">{{$val->agama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>No Hp 1</label>
                                                        <input type ="text" name ="no_hp_1" class ="form-control @error('no_hp_1') is-invalid @enderror" placeholder="No HP 1">
                                                        @error('no_hp_1')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>No Hp 2</label>
                                                        <input type ="text" name ="no_hp_2" class ="form-control @error('no_hp_2') is-invalid @enderror" placeholder="No HP 2">
                                                        @error('no_hp_2')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Jabatan</label>
                                                        <select name ="jabatan_id" class="form-control @error('jabatan') is-invalid @enderror">
                                                            <option value ="">--Pilih Jabatan--</option>
                                                            @foreach($jabatan as $val)
                                                                <option value ="{{$val->id_jabatan}}">{{$val->jabatan}}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('jabatan')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <label>Email</label>
                                                        <input type ="text" name ="email" class ="form-control @error('email') is-invalid @enderror" placeholder="Email">
                                                        @error('email')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class ="form-group">
                                                        <button type ="button" id="inputimage" class ="btn btn-primary"><i class ="fa fa-upload"></i> Upload</button>
                                                        <div id ="image_baru">
                                                            <label>File</label><br>
                                                            <div id ="foto"></div>
                                                            <div style="height: 10px"></div>
                                                            <p>Filesize : <span id ="filesize_baru">0</span></p>
                                                        </div>


                                                        <input type ="file" name ="file" id="idInputImage" onchange="image_foto_pengurus(event)" hidden>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class ="form-group">
                                                <label>Alamat</label>
                                                <textarea class ="form-control @error('alamat') is-invalid @enderror" name ="alamat"></textarea>
                                                @error('alamat')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Data Pengurus</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>No Ktp</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th width="20%">Aksi</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pengurus_agama as $key=>$val)

                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$val->no_ktp}}</td>
                                <td>{{$val->nik}}</td>
                                <td>{{$val->nama}}</td>
                                <th>
                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_pengurus}}'><i class="fa fa-edit"></i></a>
                                    <div class="modal fade" id="m_edit-{{$val->id_pengurus}}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content ">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Data Pengurus</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_pengurus',$val->id_pengurus)}}"enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class ="row">
                                                            <div class ="col-6">
                                                                <div class ="form-group">
                                                                    <label>No KTP</label>
                                                                    <input type ="text" name ="no_ktp" value ="{{$val->no_ktp}}" class ="form-control" placeholder="No Ktp">
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label>NIK</label>
                                                                    <input type ="text" name ="nik" value ="{{$val->nik}}" class ="form-control" placeholder="NIK">
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label>Nama</label>
                                                                    <input type ="text" name ="nama" value ="{{$val->nama}}" class ="form-control" placeholder="Nama">
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label>Tempat Lahir</label>
                                                                    <input type ="text" name ="tempat_lahir" value ="{{$val->tempat_lahir}}" class ="form-control" placeholder="Tempat Lahir">
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label>Tanggal Lahir</label>
                                                                    <input type ="date" name ="tanggal_lahir" value ="{{$val->tanggal_lahir}}" class ="form-control" placeholder="tanggal_lahir">
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label>Jenis Kelamin</label>
                                                                    <select class ="form-control" name ="jenis_kelamin">
                                                                        <option>{{$val->jenis_kelamin}}</option>
                                                                        <option>Laki-laki</option>
                                                                        <option>Perempuan</option>
                                                                    </select>
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label>Password</label>
                                                                    <input type ="password" value ="{{$val->password}}" name ="password" class ="form-control" placeholder="Password">
                                                                </div>
                                                            </div>
                                                            <div class ="col-6">
                                                                <div class ="form-group">
                                                                    <label>Agama</label>
                                                                    <select name ="agama_id" class ="form-control">
                                                                        @foreach($agama as $vaz)
                                                                            @if($val->agama_id == $vaz->id_agama)
                                                                                <option value ="{{$vaz->id_agama}}">
                                                                                {{$vaz->agama}}
                                                                                </option>
                                                                             @endif

                                                                            <option value ="{{$vaz->id_agama}}">{{$vaz->agama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label>No Hp 1</label>
                                                                    <input type ="text" value ="{{$val->no_hp_1}}" name ="no_hp_1" class ="form-control" placeholder="No HP 1">
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label>No Hp 2</label>
                                                                    <input type ="text" name ="no_hp_2" value ="{{$val->no_hp_2}}" class ="form-control" placeholder="No HP 2">
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label>Jabatan</label>
                                                                    <select name ="jabatan_id" class="form-control">
                                                                        @foreach($jabatan as $vaj)
                                                                            @if($val->jabatan_id == $vaj->id_jabatan)
                                                                                <option value ="{{$vaj->id_jabatan}}">
                                                                                {{$vaj->jabatan}}
                                                                                </option>
                                                                            @endif
                                                                            <option value ="{{$vaj->id_jabatan}}">{{$vaj->jabatan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class ="form-group">
                                                                    <label>Email</label>
                                                                    <input type ="text" value ="{{$val->email}}" name ="email" class ="form-control" placeholder="Email">
                                                                </div>
                                                                <div class ="form-group">
                                                                    <div class ="row">
                                                                        <div class ="col-6">
                                                                            <label>Foto Pengurus</label>
                                                                            <img src ="{{asset('foto_pengurus/'.$val->foto)}}" width="150px" height="150px">
                                                                            <p> Filesize :<span>{{number_format(File::size(public_path('foto_pengurus/'.$val->foto))/1024,2)}} KB</span></p>
                                                                        </div>
                                                                        <div class ="col-6">
                                                                            <label>Foto Baru Pengurus</label>
                                                                            <div id ="foto_update"  class ="mb-1"></div>
                                                                            <p>Filesize :<span id ="filesize_baru_update">0</span></p>
                                                                        </div>
                                                                    </div>
                                                                    <button type ="button" id ="inputimage_update" class ="btn btn-primary"><i class ="fa fa-upload"></i> Upload</button>
                                                                    <input type ="file" onchange="image_foto_pengurus_update(event)" id ="idInputImage_update" name ="file" hidden>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Alamat</label>
                                                            <textarea class ="form-control" name ="alamat">{{$val->alamat}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" onclick="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_pengurus}}">
                                        <i class = "fa fa-trash"></i>
                                    </button>
                                    <div class="modal fade" id="del-{{$val->id_pengurus}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data {{$val->nama}} </h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action="{{route('delete_pengurus',$val->id_pengurus)}}" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                        Apa Anda Yakin ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class = "btn btn-danger"><i class = "fa fa-trash"></i> Hapus</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="btn btn-secondary btn-sm" data-toggle="modal" href='#m_detail-{{$val->id_pengurus}}'><i class="fa fa-eye"></i></a>
                                    <div class="modal fade" id="m_detail-{{$val->id_pengurus}}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content ">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Melihat Data Pengurus</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                               <div class ="modal-body">
                                                   <div class ="row">
                                                       <div class ="col-4">
                                                            <img src = "{{asset('foto_pengurus/'.$val->foto)}}" width="200px" height="200px">
                                                       </div>
                                                       <div class ="col-4">
                                                           <div class ="form-group">
                                                               <label>No KTP</label>
                                                               <p>{{$val->no_ktp}}</p>
                                                           </div>
                                                           <div class ="form-group">
                                                               <label>NIK</label>
                                                               <p>{{$val->nik}}</p>
                                                           </div>
                                                           <div class ="form-group">
                                                               <label>Nama</label>
                                                               <p>{{$val->nama}}</p>
                                                           </div>
                                                           <div class ="form-group">
                                                               <label>tempat lahir</label>
                                                               <p>{{$val->tempat_lahir}}</p>
                                                           </div>
                                                           <div class ="form-group">
                                                               <label>Tanggal Lahir</label>
                                                               <p>{{$val->tanggal_lahir}}</p>
                                                           </div>
                                                           <div class ="form-group">
                                                               <label>Jenis Kelamin</label>
                                                               <p>{{$val->jenis_kelamin}}</p>
                                                           </div>

                                                       </div>
                                                       <div class ="col-4">
                                                           <div class ="form-group">
                                                               <label>Agama</label>
                                                               @foreach($agama as $vag)
                                                                   @if($val->agama_id == $vag->id_agama)
                                                                       <p>{{$vag->agama}}</p>
                                                                   @endif
                                                               @endforeach
                                                           </div>
                                                           <div class ="form-group">
                                                               <label>Jabatan</label>
                                                               @foreach($jabatan as $vaj)
                                                                   @if($val->jabatan_id == $vaj->id_jabatan)
                                                                       <p>{{$vaj->jabatan}}</p>
                                                                    @endif
                                                                @endforeach
                                                           </div>
                                                           <div class ="form-group">
                                                               <label>Email</label>
                                                               <p>{{$val->email}}</p>
                                                           </div>
                                                           <div class ="form-group">
                                                               <label>Alamat</label>
                                                               <p>{{$val->alamat}}</p>
                                                           </div>
                                                           <div class ="form-group">
                                                               <label>No Hp 1</label>
                                                               <p>{{$val->no_hp_1}}</p>
                                                           </div>
                                                           <div class ="form-group">
                                                               <label>No Hp 2</label>
                                                               <p>{{$val->no_hp_2}}</p>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script>

        let image_barus = document.getElementById("image_baru");
        let filesize = document.getElementById("filesize_baru"),
            button = document.getElementById("inputimage"),
            image = document.getElementById("idInputImage"),
            box = document.getElementById("box"),
            image_update = document.getElementById("idInputImage_update"),
            button_update =document.getElementById("inputimage_update"),
            filesize_update = document.getElementById("filesize_baru_update");
        button.onclick = ()=>{
            let image_barus = document.getElementById("image_baru");
            image.click();
            image_barus.style.visibility="visible";
        }
        button_update.onclick = ()=>{
            image_update.click();
        }
        image_baru();
        function submit(){
            let filesize = document.getElementById("filesize_baru");
            filesize.innerHTML = 0;

        }
        function image_baru_update(){

        }
        function image_baru (){
            let image_barus = document.getElementById("image_baru");
            image_barus.style.visibility="hidden";
        }
        function image_foto_pengurus(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 200;
            newimg.height = 200;
            imagediv.appendChild(newimg);
        }
        function image_foto_pengurus_update(event) {
            var image = URL.createObjectURL(event.target.files[0]);
            var imagediv = document.getElementById('foto_update');
            var newimg = document.createElement('img');
            newimg.src = image;
            newimg.width = 150;
            newimg.height = 150;
            imagediv.appendChild(newimg);
        }
        function  updatesize(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];

                }
                filesize.innerHTML = output;
            },1000)
        }
        image.addEventListener("change",updatesize());
        function  updatesize_update(){
            setInterval(() =>{
                let nBytes = 0;
                let Files = image_update.files;
                for(let i = 0;i <Files.length;i++){
                    nBytes += Files[i].size;
                }
                let output = nBytes +"bytes" ;
                let multiple = ["KB","MB","GB","GB","TB","PB","EB","ZB","YB"];
                for(j =0,nApprox = nBytes/1024;nApprox>1;nApprox/=1024,j++){
                    output = nApprox.toFixed(3)+" "+multiple[j];

                }
                filesize_update.innerHTML = output;
            },1000)
        }
        image_update.addEventListener("change",updatesize_update());
    </script>
@endsection
