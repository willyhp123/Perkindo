@extends('backend.admin.layout.header')
@section('title','Rekening Pembayaran')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default_1">
                            <i class="fa fa-plus"></i> Tambah Data Rekening
                        </button>

                        <div class="modal fade" id="modal-default_1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Rekening Pembayaran</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('tambah_rekening_pembayaran')}}">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Nama BANK</label>
                                                <input type ="text" name ="nama_bank" class ="form-control @error('nama_bank') is-invalid @enderror" placeholder="Nama Bank">
                                                @error('nama_bank')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>No Rek</label>
                                                <input type ="text" name ="no_rek" class ="form-control @error('no_rek') is-invalid @enderror" placeholder="No Rekening">
                                                @error('no_rek')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Atas Nama</label>
                                                <input type ="text" name ="atas_nama" class ="form-control @error('atas_nama') is-invalid @enderror" placeholder="Nama Pemilik Rekening">
                                                @error('atas_nama')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Rekening Pembayaran</li>
                            </ol>
                        </div>

                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Nama Bank</th>
                            <th>No Rek</th>
                            <th>Atas Nama</th>
                            <th width="20%">Aksi</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rekening_pembayaran as $key=>$val)
                            <tr>
                                <Td>{{$key+1}}</Td>
                                <td>{{$val->nama_bank}}</td>
                                <td>{{$val->no_rek}}</td>
                                <td>{{$val->atas_nama}}</td>
                                <td>
                                    <!-- Button trigger modal -->
                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_rekening_pembayaran}}'><i class="fa fa-edit"></i>Edit</a>
                                    <div class="modal fade" id="m_edit-{{$val->id_rekening_pembayaran}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Data Rekening Pembayaran</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_rekening_pembayaran',$val->id_rekening_pembayaran)}}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class ="form-group">
                                                            <label>Nama Bank</label>
                                                            <input type ="text" name = "nama_bank" class ="form-control"
                                                                   value ="{{$val->nama_bank}}" placeholder="Nama Bank">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>No Rek</label>
                                                            <input type ="text" name = "no_rek" class ="form-control"
                                                                   value ="{{$val->no_rek}}" placeholder="No Rek">
                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Atas Nama</label>
                                                            <input type ="text" name = "atas_nama" class ="form-control"
                                                                   value ="{{$val->atas_nama}}" placeholder="Atas Nama">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_rekening_pembayaran}}">
                                        <i class = "fa fa-trash"></i> Hapus
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="del-{{$val->id_rekening_pembayaran}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data Rekening Pembayaran </h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action="{{route('delete_rekening_pembayaran',$val->id_rekening_pembayaran)}}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class ="form-group">
                                                            <label>Nama Bank</label>
                                                            <p>{{$val->nama_bank}}</p>
                                                            <label>No rek</label>
                                                            <p>{{$val->no_rek}}</p>
                                                            <label>Atas Nama</label>
                                                            <p>{{$val->atas_nama}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="submit" class = "btn btn-danger"><i class = "fa fa-trash"></i> Hapus</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
