@extends('backend.admin.layout.header')
@section('title','Agama')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                    <div class ="row">
                        <div class ="col-6">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default_tambah">
                                <i class="fa fa-plus"></i> Tambah Data Agama
                            </button>

                            <div class="modal fade" id="modal-default_tambah">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Tambah Data Agama</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="post" action="{{route('tambah_agama')}}">
                                            @csrf
                                            <div class="modal-body">
                                                <div class ="form-group">
                                                    <label>Agama</label>
                                                    <input type ="text" name ="agama" class ="form-control @error('agama') is-invalid @enderror" placeholder="Nama Agama">
                                                    @error('agama')
                                                        <span class ="invalid-feedback">{{$message}}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                        <div class ="col-6">
                            <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Agama</li>
                            </ol>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Agama</th>
                            <th width="20%">Aksi</th>

                        </tr>

                        <tbody>

                            @foreach($agama as $key=>$val)
                                <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$val->agama}}</td>
                                <td>
                                    <!-- Button trigger modal -->
                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_agama}}'><i class="fa fa-edit"></i>Edit</a>
                                    <div class="modal fade" id="m_edit-{{$val->id_agama}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Data Agama</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_agama',$val->id_agama)}}">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class ="form-group">
                                                        <label>Agama</label>
                                                        <input type ="text" name = "agama"  class ="form-control"
                                                               value ="{{$val->agama}}" placeholder="Agama">

                                                    </div>
                                                </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_agama}}">
                                        <i class = "fa fa-trash"></i> Hapus
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="del-{{$val->id_agama}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data {{$val->agama}} </h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action="{{route('delete_agama',$val->id_agama)}}">
                                                    @csrf
                                                <div class="modal-body">
                                                    Apa Anda Yakin ?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class = "btn btn-danger"><i class = "fa fa-trash"></i> Hapus</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                                </tr>
                            @endforeach



                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
