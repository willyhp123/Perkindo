@extends('backend.admin.layout.header')
@section('title','kota Kabupaten')
<!-- Content Wrapper. Contains page content -->
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class ="row">
                    <div class ="col-6">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                            <i class="fa fa-plus"></i> Tambah Data kota/Kabupaten
                        </button>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data Kota/Kabupaten</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="{{route('tambah_kota_kabupaten')}}">
                                        @csrf
                                        <div class="modal-body">
                                            <div class ="form-group">
                                                <label>Provinsi</label>
                                                <select name ="provinsi_id" class ="form-control @error('provinsi_id') is-invalid @enderror ">
                                                    <option value ="">--Pilih Provinsi--</option>
                                                    @foreach($provinsi as $val)
                                                        <option value ="{{$val->id_provinsi}}" >{{$val->provinsi}}</option>
                                                    @endforeach
                                                </select>
                                                @error('provinsi_id')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class ="form-group">
                                                <label>Kota/Kabupaten</label>
                                                <input type ="text" name ="kota_kabupaten"
                                                       class ="form-control @error('kota_kabupaten_id') is-invalid @enderror" placeholder="Kota/Kabupaten">
                                                @error('kota_kabupaten')
                                                <span class ="invalid-feedback">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"><i class ="fa fa-plus"></i> Tambah</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <div class ="col-6">
                        <div class="card-tools">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item active">Kota Kabupaten</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Provinsi</th>
                            <th>Kota/Kabupaten</th>
                            <th width="20%">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kota_kabupaten as $key=>$val)
                            <tr>
                                <Td>{{$key+1}}</Td>
                                @foreach($provinsi as $vaz)
                                    @if($val->provinsi_id == $vaz->id_provinsi)
                                <td>{{$vaz->provinsi}}</td>
                                    @endif
                                @endforeach
                                <Td>{{$val->kota_kabupaten}}</Td>
                                <Td>

                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#m_edit-{{$val->id_kota_kabupaten}}'><i class="fa fa-edit"></i> Edit</a>
                                    <div class="modal fade" id="m_edit-{{$val->id_kota_kabupaten}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Data Provinsi</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <form method="post" action="{{route('update_kota_kabupaten',$val->id_kota_kabupaten)}}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class ="form-group">
                                                            <label>Provinsi</label>
                                                            <select name ="provinsi_id" class ="form-control">
                                                                @foreach($provinsi as $vaz)
                                                                    @if($val->provinsi_id == $vaz->id_provinsi)
                                                                        <option value="{{$vaz->id_provinsi}}" >---{{$vaz->provinsi}}---</option>
                                                                        @endif
                                                                    <option value ="{{$vaz->id_provinsi}}">{{$vaz->provinsi}}</option>
                                                                 @endforeach
                                                            </select>

                                                        </div>
                                                        <div class ="form-group">
                                                            <label>Kota Kabupaten</label>
                                                            <input type ="text" name ="kota_kabupaten" class ="form-control"
                                                                   placeholder="Kota Kabupaten" value ="{{$val->kota_kabupaten}}">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-warning"><i class ="fa fa-edit"></i> Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#del-{{$val->id_kota_kabupaten}}">
                                        <i class = "fa fa-trash"></i> Hapus
                                    </button>

                                    <div class="modal fade" id="del-{{$val->id_kota_kabupaten}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3>Hapus Data {{$val->kota_kabupaten}}</h3>
                                                </div>
                                                <div class = "border"></div>
                                                <form method="post" action ="{{route('delete_kota_kabupaten',$val->id_kota_kabupaten)}}">
                                                    @csrf
                                                    <div class="modal-body">
                                                        Apa Anda Yakin ?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type ="submit" class = "btn btn-danger"><i class = "ft ft-trash"></i>Hapus</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </Td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
